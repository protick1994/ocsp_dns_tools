import json
from datetime import datetime, timedelta
import pyasn
'''
Lum apache send koro !!

'''

asndb = pyasn.pyasn("/home/protick/proxyrack/asn_org_tools/data/ipsan_db.dat")


# main_file_base_directory = "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/new_ttl/results_new_exp_1/"

# bind_file_directory = "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/new_ttl/dns_ttl_new/bind"

main_file_base_directory = "/home/protick/node_code/results_new_exp_xxx/"

bind_file_directory = "/net/data/dns-ttl/dns_ttl_new_xxx/bind"

apache_files_p1_directory = ["/net/data/dns-ttl/dns_ttl_new_xxx/apache_server_p_1",
                          "/net/data/dns-ttl/dns_ttl_new_xxx/apache_server_p_2"]

apache_files_p2_directory = ["/net/data/dns-ttl/dns_ttl_new_xxx/apache_server_phase2"]

allowed_ttl = [1, 5, 15, 30, 60]

'''
'''

def get_leaf_files(path):
    import os
    list_of_files = []

    for root, dirs, files in os.walk(path):
        for file in files:
            list_of_files.append(os.path.join(root, file))
    return list_of_files


dump_directory = "middle_box_analysis/"


ip_to_index = {
    "3.223.194.233": 7,
    "34.226.99.56": 5,
    "52.44.221.99": 8,
    "52.71.44.50": 6,
    "18.207.47.246": 2,
    "3.208.196.0": 3,
    "44.195.175.12": 4,
    "50.16.6.90": 1,
}

from collections import defaultdict
dishonor_lst = []
honor_lst = []
req_dict = {}
response_to_ip = {}
req_uid_to_phase_1_resolver_tuple = {}
req_uid_to_phase_2_resolver_lst = defaultdict(lambda : list())
req_uid_to_asn = {}
req_uid_to_hash = {}

all_resolver_set = set()
all_exitnode_set = set()
all_asn_set = set()
all_lum_resolver_set = set()

found_set = set()
not_found_set = set()


def reset():
    global dishonor_lst
    global honor_lst
    global req_dict
    global response_to_ip
    global req_uid_to_phase_1_resolver_tuple
    global req_uid_to_phase_2_resolver_lst
    global req_uid_to_asn
    global req_uid_to_hash
    global all_resolver_set
    global all_exitnode_set
    global all_asn_set
    global all_lum_resolver_set
    global found_set
    global not_found_set

    dishonor_lst = []
    honor_lst = []
    req_dict = {}
    response_to_ip = {}
    req_uid_to_phase_1_resolver_tuple = {}
    req_uid_to_phase_2_resolver_lst = defaultdict(lambda: list())
    req_uid_to_asn = {}
    req_uid_to_hash = {}

    all_resolver_set = set()
    all_exitnode_set = set()
    all_asn_set = set()
    all_lum_resolver_set = set()

    found_set = set()
    not_found_set = set()


uid_ttl_to_bind_info = defaultdict(lambda : dict())

def analyze_bind(ttl):
    for key in ip_to_index:
        response_to_ip["phase{}".format(ip_to_index[key])] = key

    leaf_files_unfiltered = get_leaf_files(bind_file_directory.replace("xxx", ttl))
    # leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    print(len(leaf_files_unfiltered))
    for file in leaf_files_unfiltered:
        with open(file) as FileObj:
            for line in FileObj:
                try:
                    non_decidings_ips = ['52.4.120.223']

                    if "good" not in line or "zeus_reload" not in line:
                        continue
                    # good 198.82.247.98 1660802862.1054115 50.16.6.90 f08ed8ae-dfb6-4289-930a-b2bbe75362561660802861993.zeus_reload_1_1_1.60.1.1.securekey.app. A

                    segments = line.strip().split()

                    resolver_ip, timestamp, allotted_ip, req_url = segments[1], segments[2], segments[3], segments[4]
                    if resolver_ip == '3.220.52.113':
                        continue
                    req_uid = req_url.split(".")[0]

                    uid_ttl_to_bind_info[ttl][req_uid] = (resolver_ip, timestamp, allotted_ip)


                except Exception as e:
                    # TODO severe inspect
                    if "KeyError" in str(type(e)):
                        not_found_set.add(req_uid)


def analyze_apache_files():
    leaf_files_unfiltered = []

    for ttl in allowed_ttl:
        for file in apache_files_p1_directory:
            leaf_files_unfiltered = leaf_files_unfiltered + get_leaf_files(file.replace("xxx", str(ttl)))

    all_logs = []

    for file in leaf_files_unfiltered:
        with open(file) as FileObj:
            for line in FileObj:
                try:
                    non_decidings_ips = ['52.4.120.223']

                    if "zeus_reload" not in line:
                        continue

                    # 170.84.77.29 172.31.13.110 - - /index.html [18/Aug/2022:17:45:20 +0000] "GET / HTTP/1.1" 200 262 "-" "axios/0.17.1" 73fe66ed-dbe4-463a-9218-06f68f33d7781660844719424.zeus_reload_1_161_1.1.264999.1.securekey.app
                    # good 198.82.247.98 1660802862.1054115 50.16.6.90 f08ed8ae-dfb6-4289-930a-b2bbe75362561660802861993.zeus_reload_1_1_1.60.1.1.securekey.app. A

                    segments = line.strip().split()

                    server_ip, local_ip, timestr, user_agent, req_url = segments[0], segments[1], segments[5], segments[-2], segments[-1]
                    req_uid = req_url.split(".")[0]
                    timestr = timestr[1:]
                    datetime_object = datetime.strptime(timestr, '%d/%b/%Y:%H:%M:%S')

                    all_logs.append((server_ip, local_ip, timestr, user_agent, req_url))

                except Exception as e:
                    # TODO severe inspect
                    pass

    return all_logs


uid_ttl_to_meta_info = defaultdict(lambda : dict())

def analyze_main_files(ttl):
    leaf_files_unfiltered = get_leaf_files(main_file_base_directory.replace("xxx", ttl))
    leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    leaf_files_filtered = [e for e in leaf_files_filtered if ".json" in e]

    exp_id_list = []
    pp = []
    for element in leaf_files_filtered:
        exp_id_list.append(element[: - len("-out.json")])

    err_set = set()

    print(len(exp_id_list))

    for exp_id in exp_id_list:
        level = exp_id.split("_")[-2]
        full_file = "{}{}/{}-out.json".format(main_file_base_directory.replace("xxx", ttl), level, exp_id)
        f = open(full_file)
        d = json.load(f)
        a = 1
        # d['dict_of_phases']['12973']['1-response'].strip()[4: -5]
        weird = 0
        for num in d['dict_of_phases']:

            try:
                response_1 = d['dict_of_phases'][num]['1-response'].strip()[4: -5]

                req_url_uid = d['dict_of_phases'][num]['req_url'][7:].split(".")[0]

                save_tuple = (response_1, d['dict_of_phases'][num]['asn'],
                              d['dict_of_phases'][num]['ip_hash'], d['dict_of_phases'][num]['1-time'])

                uid_ttl_to_meta_info[ttl][req_url_uid] = save_tuple

            except Exception as e:
                err_set.add(str(e))

    return err_set



## try finding different headers

##
def init(ttl):
    ttl = str(ttl)
    reset()
    analyze_main_files(ttl=ttl)
    analyze_bind(ttl=ttl)






# uid_to_apache_req_list = defaultdict(lambda : list())
# f = open(dump_directory + "apache_logs.json")
# d = json.load(f)
#
#
# for e in d:
#     req_url = e[-1]
#     uid = req_url.split(".")[0]
#     e[2] = datetime_object = datetime.strptime(e[2], '%d/%b/%Y:%H:%M:%S')
#     uid_to_apache_req_list[uid].append(e)
#
# for uid in uid_to_apache_req_list:
#     uid_to_apache_req_list[uid].sort(key=lambda x: x[2])


def make_files():
    apache_logs = analyze_apache_files()

    # (server_ip, local_ip, timestr, user_agent, req_url)
    with open(dump_directory + "apache_logs.json", "w") as ouf:
        json.dump(apache_logs, fp=ouf)

    for ttl in allowed_ttl:
        init(ttl=ttl)

    with open(dump_directory + "ttl_uid_to_meta.json", "w") as ouf:
        json.dump(uid_ttl_to_meta_info, fp=ouf)

    with open(dump_directory + "uid_ttl_to_bind_info.json", "w") as ouf:
        json.dump(uid_ttl_to_bind_info, fp=ouf)

    uid_to_apache_req = {}
    uid_to_apache_req_list = defaultdict(lambda : list())
    f = open(dump_directory + "apache_logs.json")
    d = json.load(f)

    for e in d:
        req_url = e[-1]
        uid = req_url.split(".")[0]
        e[2] = datetime.strptime(e[2], '%d/%b/%Y:%H:%M:%S')
        uid_to_apache_req_list[uid].append(e)

    for uid in uid_to_apache_req_list:
        uid_to_apache_req_list[uid].sort(key=lambda x: x[2])

    # (server_ip, local_ip, timestr, user_agent, req_url)
    for uid in uid_to_apache_req_list:
        tup = uid_to_apache_req_list[uid][0]

        uid_to_apache_req[uid] = (tup[0], tup[1], tup[3], tup[4])
    with open(dump_directory + "uid_to_apache_req.json", "w") as ouf:
        json.dump(uid_to_apache_req, fp=ouf)


def get_response_count():
    f = open(dump_directory + "ttl_uid_to_meta.json")
    d = json.load(f)
    response_to_count = defaultdict(lambda : 0)
    for ttl1 in allowed_ttl:
        ttl = str(ttl1)
        for uuid in d[ttl]:
            response_to_count[d[ttl][uuid][0]] += 1

    response_list = []
    for response in response_to_count:
        response_list.append((response_to_count[response], response))

    response_list.sort(reverse=True)
    with open(dump_directory + "response_list.json", "w") as ouf:
        json.dump(response_list, fp=ouf, indent=2)



def is_blocked(content):
    keys = ["Block", "violate", "należności", "payment", "pago", "threats", "Blocage", "Blokada", "trifle",
            "Restricted", "деньги", "denied", "pagamento", "заблокирован", "rachunek", "средств"]

    for key in keys:
        if key.lower() in content.lower():
            return 1

    if "phase" in content:
        return 3

    return 2



def get_uid_to_resolver(uid_ttl_to_bind_info_file):
    uid_to_resolver = {}
    for ttl1 in allowed_ttl:
        ttl = str(ttl1)
        for uid in uid_ttl_to_bind_info_file[ttl]:
            uid_to_resolver[uid] = uid_ttl_to_bind_info_file[ttl][uid][0]
    return uid_to_resolver


def get_uid_to_apache(uid_to_apache_req_file):
    uid_to_apache = {}

    for uid in uid_to_apache_req_file:
        uid_to_apache[uid] = uid_to_apache_req_file[uid][0]
    return uid_to_apache


def content_mod():
    # (asn, resolver, clientapache__ip, client_hash, category)
    master_list = []

    #(server_ip, local_ip, timestr, user_agent, req_url)
    f = open(dump_directory + "uid_to_apache_req.json")
    uid_to_apache_req_file = json.load(f)

    # (resolver_ip, timestamp, allotted_ip)
    f = open(dump_directory + "uid_ttl_to_bind_info.json")
    uid_ttl_to_bind_info_file = json.load(f)

    uid_to_apache = get_uid_to_apache(uid_to_apache_req_file)
    uid_to_resolver = get_uid_to_resolver(uid_ttl_to_bind_info_file)

    # save_tuple = (response_1, d['dict_of_phases'][num]['asn'],
    #                               d['dict_of_phases'][num]['ip_hash'], d['dict_of_phases'][num]['1-time'])
    f = open(dump_directory + "ttl_uid_to_meta.json")
    ttl_uid_to_meta_file = json.load(f)


    # (asn, resolver, clientapache__ip, client_hash, category)

    for ttl1 in allowed_ttl:
        ttl = str(ttl1)
        for uuid in ttl_uid_to_meta_file[ttl]:
            resp = ttl_uid_to_meta_file[ttl][uuid][0]
            asn = ttl_uid_to_meta_file[ttl][uuid][1]
            ip_hash = ttl_uid_to_meta_file[ttl][uuid][2]
            cat = is_blocked(resp)

            if uuid in uid_to_apache:
                client_ip = uid_to_apache[uuid]
            else:
                client_ip = -1

            if uuid in uid_to_resolver:
                resolver_ip = uid_to_resolver[uuid]
            else:
                resolver_ip = -1

            master_list.append((asn, resolver_ip, client_ip, ip_hash, cat))

    with open(dump_directory + "master_list.json", "w") as ouf:
        json.dump(master_list, fp=ouf, indent=2)


ip_to_asn = {}
def get_asn(ip):
    global ip_to_asn
    if ip in ip_to_asn:
        return ip_to_asn[ip]
    ip_to_asn[ip] = asndb.lookup(ip)[0]
    return ip_to_asn[ip]

def not_same_asn(ip, asn):

    if ip == -1:
        return False

    ip_asn = get_asn(ip)
    if int(ip_asn) != int(asn):
        return True

    return False


def analyze_master_list():
    f = open(dump_directory + "master_list.json")
    # asn, resolver, client_apache_ip, client_hash, category)
    master_list = json.load(f)

    # asn -> 3 tar count
    # category_to -> (asn, client_hash)

    # bind ashei na, onno page !!!

    '''
    new cat: 1 -> block, 2 -> change (client_apache_ip ache), 3 -> change (client_apache_ip nai)
    4 -> normal, 5 -> bind ashe nai/content change, 6 -> thik ache, but diff ip
    '''

    asn_to_count = defaultdict(lambda: defaultdict(lambda: 0))
    cat_list = defaultdict(lambda : list())

    vis = defaultdict(lambda: defaultdict(lambda: 0))

    # blocker first
    for element in master_list:
        try:
            asn, resolver, client_ip, client_hash, category = element
            if vis[asn][client_hash] == 1:
                continue
            vis[asn][client_hash] = 1
            # blocker

            new_cat = -1
            if category == 1:
                new_cat = 1
            elif category == 2 and client_ip != -1:
                new_cat = 2
            elif category == 2 and client_ip == -1:
                new_cat = 3
            elif category == 3:
                new_cat = 4
            elif category == 2 and resolver == -1:
                new_cat = 5
            elif not_same_asn(client_ip, asn):
                new_cat = 6

            asn_to_count[asn][new_cat] += 1
            cat_list[new_cat].append((asn, client_hash))

        except Exception as e:
            print(e)

    with open(dump_directory + "asn_to_count.json", "w") as ouf:
        json.dump(asn_to_count, fp=ouf)
    with open(dump_directory + "cat_list.json", "w") as ouf:
        json.dump(cat_list, fp=ouf)


def final_verdict():
    f = open(dump_directory + "asn_to_count.json")
    asn_to_count = json.load(f)

    f = open(dump_directory + "cat_list.json")
    # (asn, client_hash)
    cat_list = json.load(f)

    asn_to_tot = defaultdict(lambda : 0)
    for asn in asn_to_count:
        for i in range(1, 7):
            try:
                asn_to_tot[asn] += asn_to_count[asn][str(i)]
            except:
                pass

    marked_asn_to_cat = defaultdict(lambda : dict())

    for i in range(1, 7):
        if i == 4:
            continue
        for asn in asn_to_count:
            tot = asn_to_tot[asn]
            if tot <= 3:
                continue
            try:
                div = asn_to_count[asn][str(i)] / tot
            except:
                continue
            if div > .1:
                marked_asn_to_cat[asn][str(i)] = 1
                print("Cat ASN {} {}".format(i, asn))

    for i in range(1, 7):
        if i == 4:
            continue

        dis_asn = set()
        dis_exitnode = set()
        if str(i) not in cat_list:
            continue
        print("Trottering cat {}".format(i))
        for e in cat_list[str(i)]:
            # (asn, client hash)
            asn, client_hash = e
            if str(i) not in marked_asn_to_cat[asn]:
                dis_asn.add(asn)
                dis_exitnode.add(client_hash)

        print("ASNs {}, exitnodes {}".format(len(dis_asn), len(dis_exitnode)))




# get_response_count()

# make_files()
# content_mod()

# analyze_master_list()
final_verdict()

'''
content blocker er karon temon nai.....

full list of advertisements ...

language barrier ...


same ISP majority:

same ISP er majority na:  application level


Malware often modifies content to steal user credentials or 
inject advertisements; middleboxes are deployed by ISPs for a variety
 of reasons, including security (e.g., firewalls, anti-virus systems), 
 content policies (e.g., content blockers), and performance optimization 
 (e.g., proxies, DNS interception boxes, transcoders).

'''

