import json
import pyasn, json

asndb = pyasn.pyasn('ipsan_db.dat')

f = open("misc_jsons/url_to_ips.json")
url_dict = json.load(f)

for url in url_dict:
    ip_list = url_dict[url]
    l = []
    for e in ip_list:
        l.append((e, asndb.lookup(e)[0]))
    url_dict[url] = l

with open("misc_jsons/url_to_ips.json", "w") as ouf:
    json.dump(url_dict, fp=ouf, indent=2)

