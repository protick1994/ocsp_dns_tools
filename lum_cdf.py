import json

f = open("/home/protick/ocsp_dns_tools/ttl_new_results/mom.json")
mom = json.load(f)

l = open("/home/protick/proxyrack/data/ip_to_asn_dict.json")
ip_to_asn_dict = json.load(l)
lum_resolvers_asn = [15169, 20473, 36692, 14061, 30607, 24940, 27725]

req_uid_to_phase_1_resolvers_all = mom['req_uid_to_phase_1_resolvers_all']
req_uid_to_phase_1_resolvers_without_lum = mom['req_uid_to_phase_1_resolvers_without_lum']
req_uid_to_response = mom['req_uid_to_response']
req_uid_to_response_time = mom['req_uid_to_response_time']
dump_directory = "ttl_new_results/"

ans = []
ans_max = []
for req_uid in req_uid_to_phase_1_resolvers_without_lum:
    if 'lum' not in req_uid_to_response[req_uid]:
        ans.append(len(req_uid_to_phase_1_resolvers_without_lum[req_uid]))


for req_uid in req_uid_to_phase_1_resolvers_all:
    try:

        response_ts = float(req_uid_to_response_time[req_uid])
        response_ts = response_ts / 1000


        if 'lum' in req_uid_to_response[req_uid]:
            pass
        else:
            all_resolvers = req_uid_to_phase_1_resolvers_all[req_uid]
            temp = set()
            for resolver_tuple in all_resolvers:
                resolver, timestamp = resolver_tuple
                timestamp = float(timestamp)
                if timestamp > response_ts:
                    continue
                asn = ip_to_asn_dict[resolver]
                if asn not in lum_resolvers_asn:
                    temp.add(resolver)
                else:
                    print("Got it")
            if len(temp) > 0:
                ans_max.append(len(temp))
            # ans_max.append(len(req_uid_to_phase_1_resolvers_without_lum[req_uid]))
    except:
        pass


with open(dump_directory + "hit_cdf.json", "w") as ouf:
    json.dump(ans, fp=ouf)

with open(dump_directory + "hit_cdf_mx.json", "w") as ouf:
    json.dump(ans_max, fp=ouf)
