import pyasn, json

# f = open("cert_subjects.json")
# ocsp_url_dict = json.load(f)
#
asndb = pyasn.pyasn('ipsan_db.dat')
#
#
# for element in ocsp_url_dict:
#     for serial in ocsp_url_dict[element]:
#         a_Record = ocsp_url_dict[element][serial]['a_record']
#         asn = asndb.lookup(a_Record)
#         ocsp_url_dict[element][serial]['asn'] = asn[0]
#
# with open("cert_subjects.json", "w") as ouf:
#     json.dump(ocsp_url_dict, fp=ouf, indent=2)


import requests
asn_list = ['6799', '3462', '577', '12793', '701', '812', '133385']


url_set = ['http://checkip.dyndns.org/', 'http://216.146.43.71/']

for url in url_set:
    print("Trying: {}".format(url))
    for asn in asn_list:
        proxies = {
            'http': 'http://lum-customer-c_9c799542-zone-residential-asn-{}:xm4jk9845cgb@zproxy.lum-superproxy.io:22225'.format(
                asn)
        }

        s = requests.Session()
        s.proxies = proxies
        r = s.get(url).content.decode("utf-8")
        r = r[r.find("Current IP Address: ") + 20:]
        ip = r[:r.find("</body>")]
        actual_asn = asndb.lookup(ip)[0]

        print("Ip: :", ip)
        print("Actual ASN: ", actual_asn)
        print("Asn selected: ", asn)

    for i in range(2):
        print("******************************************")




