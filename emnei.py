import json

import ujson

f = open("correlation_resolvers.json")
d = ujson.load(f)

def is_same_net(e1, e2):
    e1_pre = e1.split(".")[0] + e1.split(".")[1]
    e2_pre = e2.split(".")[0] + e2.split(".")[1]
    return e1_pre == e2_pre


l = []
for e1 in d:
    for e2 in d[e1]:
        if e1 != e2 and is_same_net(e1, e2):
            l.append((d[e1][e2], e1, e2))

l.sort(key=lambda x: x[0], reverse=True)
a = 1
with open("over_lap.json", "w") as ouf:
    json.dump(l, fp=ouf,indent=2)


