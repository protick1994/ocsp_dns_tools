from collections import defaultdict
from os import listdir
from os.path import isfile, join


# apache_dict = {}
# apache_files = ['apache_logs/' + f for f in listdir('apache_logs') if isfile(join('apache_logs/', f))]
# for apache_file in apache_files:
#     if 'access' not in apache_file:
#         continue
#     if '.gz' in apache_file:
#         continue
#     file = open(apache_file, 'r')
#
#     Lines = file.readlines()
#     for line in Lines:
#         try:
#             if "luminati.netsecurelab.org" not in line:
#                 continue
#             l = line.strip()
#
#
#             segments = l.split(" ")
#
#             url = segments[len(segments) - 1][1:-1]
#             url = url[: url.rfind("/")]
#             if url.count("-") != 2:
#                 continue
#
#             url_identifier = url.split("-")[0]
#             asn = url.split("-")[1]
#             if url_identifier not in apache_dict:
#                 apache_dict[url_identifier] = list()
#             source_ips.add(segments[0])
#             source_asns.add(asn)
#             apache_dict[url_identifier].append({'apache_source_ip': segments[0], 'asn': asn, 'query_time': segments[3]})
#         except Exception as e:
#             a = 1
#             pass
#
#
# bind_dict = {}
#
# bind_files = ['bind/' + f for f in listdir('bind') if isfile(join('bind/', f))]
# for bind_file in bind_files:
#     file = open(bind_file, 'r')
#     Lines = file.readlines()
#     for line in Lines:
#         if "luminati.netsecurelab.org" not in line:
#             continue
#         if line.startswith("client"):
#             continue
#         l = line.strip()
#         segments = l.split(" ")
#         time = segments[0] + "-" + segments[1]
#         resolver_ip = segments[5]
#         resolver_ip = resolver_ip[: resolver_ip.rfind("#")]
#         query_type = segments[10]
#         url = segments[8]
#         url_identifier = segments[8].split("-")[0]
#         if url_identifier not in bind_dict:
#             bind_dict[url_identifier] = list()
#         bind_dict[url_identifier].append({'resolver_ip': resolver_ip, 'query_type': query_type, 'query_time': time})
#
from datetime import datetime

divider = datetime.strptime('15-Feb-2022-11:26:00', '%d-%b-%Y-%H:%M:%S')

prev_dict = set()

dummy_dict = set()

after_dict = set()

all_set = set()


def check_ttl_logs():
    #times = []
    bind_files = ['bind_ttl/' + f for f in listdir('bind_ttl') if isfile(join('bind_ttl/', f))]
    for bind_file in bind_files:
        file = open(bind_file, 'r')
        Lines = file.readlines()
        for line in Lines:
            if "ttlexp.luminati.netsecurelab.org" not in line:
                continue
            if 'run1' not in line:
                continue

            if line.startswith("client"):
                continue

            l = line.strip()
            segments = l.split(" ")
            time = segments[0] + "-" + segments[1]
            resolver_ip = segments[5]
            resolver_ip = resolver_ip[: resolver_ip.rfind("#")]
            url = segments[8]
            time = time[: time.rfind(".")]
            datetime_object = datetime.strptime(time, '%d-%b-%Y-%H:%M:%S')

            all_set.add(resolver_ip)
            if url.startswith("run1.ttlexp"):
                # normal case
                if datetime_object > divider:
                    after_dict.add(resolver_ip)
                else:
                    prev_dict.add(resolver_ip)
            else:
                dummy_dict.add(resolver_ip)


check_ttl_logs()
#times.sort()

mx = 0

malfunctioning_resolvers = set()
properly_func_resolvers = set()

for entry in dummy_dict:
    if entry in prev_dict and entry not in after_dict:
        malfunctioning_resolvers.add(entry)

# for entry in dummy_dict:
#     if entry in prev_dict and entry in after_dict:
#         properly_func_resolvers.add(entry)

print("{} malfunctioning resolvers out of {} resolvers".format(len(malfunctioning_resolvers), len(all_set)))


# mother_info = dict()
# apache_keys = set(apache_dict)
# bind_keys = set(bind_dict)
# for common_key in apache_keys.intersection(bind_keys):
#     mother_info[common_key] = {}
#     mother_info[common_key]['bind'] = bind_dict[common_key]
#     mother_info[common_key]['apache'] = apache_dict[common_key]
#
# import json
# a = 1
# print("total requests", len(mother_info.keys()))
# print("total ips", len(source_ips))
# print("total asns", len(source_asns))
#
# with open("resolver_to_ip_results.json", "w") as ouf:
#     json.dump(mother_info, fp=ouf)

