import numpy as np
import matplotlib.pyplot as plt
import json
import random


def alright(chosen_keys, m_dict):

    vals = []



    for key in chosen_keys:
        if m_dict[key]['all_allowed'][0] > m_dict[key]['all_allowed'][1]:
            return False

        if m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][0] > m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][1]:
            return False

        vals.append(m_dict[key]['all_allowed'][1])
        vals.append(m_dict[key]['all_allowed'][0])
        vals.append(m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][1])
        vals.append(m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][0])

    for e in vals:
        if e == 0 or int(e) == 0:
            return False

    return True


f = open('dns_ssl.json')
m_dict = json.load(f)

xtra_space = 0.05

keys = list(m_dict.keys())
N = 5
while(True):
    chosen_keys = random.sample(keys, N)

    if not alright(chosen_keys, m_dict):
        continue

    ind = np.arange(N) + .15
    width = 0.35
    fig, ax = plt.subplots(figsize=(10, 20))
    dns_plus_ssl_1 = []
    ssl_1 = []

    dns_plus_ssl_2 = []
    ssl_2 = []
    x_labels = ['x']

    for key in chosen_keys:
        x_labels.append(key[:-5])
        dns_plus_ssl_1.append(m_dict[key]['all_allowed'][1] + m_dict[key]['all_allowed'][0])
        ssl_1.append(m_dict[key]['all_allowed'][1])

        dns_plus_ssl_2.append(m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][1] + m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][0])
        ssl_2.append(m_dict[key]['both_ocsp_and_ocsp_stapling_disabled'][1])

    ax.bar(ind, dns_plus_ssl_1, width, color='g')
    ax.bar(ind, ssl_1, width, color='r')

    # ax.bar(ind, dns_plus_ssl_1, width, color='g')
    # ax.bar(ind, ssl_1, width, color='r')

    ax.bar(ind + width + xtra_space, dns_plus_ssl_2, width, color='g')
    ax.bar(ind + width + xtra_space, ssl_2, width, color='r')

    ax.set_ylabel('Latency, millisecond')
    ax.set_title('SSL time vs DNS lookup')

    plt.legend(["DNS lookup time", "SSL connection time"])
    ax.set_xticklabels(x_labels, rotation='vertical')
    plt.show()
    a = 1






