import json
from collections import defaultdict
import pyasn
from caida_as_org import AS2ISP
from dns import resolver, rdata
from dns.rdatatype import CNAME, A, NS
import matplotlib.pyplot as plt
as2isp = AS2ISP()
asndb = pyasn.pyasn("/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ipsan_db.dat")

def get_cname_chain(url):
    try:
        dns_records = []
        if url.startswith("http://"):
            url = url[7:]
        if "/" in url:
            url = url[0: url.find("/")]
        if url.endswith("."):
            url = url[: -1]
        for rdata in resolver.resolve(url, CNAME, raise_on_no_answer=False):
            dns_records.append(str(rdata))
        if len(dns_records) == 0:
            return []
        else:
            return [dns_records[0]] + get_cname_chain(dns_records[0])

    except Exception as e:
        return []

# def ocsp_crawler_find_stat():
#     ## Classify top 30
#     pass
#
#     f = open("OCSP_DNS_DJANGO/stat_finder/top_30_ocsp_responders.json")
#     top_responders = json.load(f)
#
#     # for element in top


def get_asn_and_org(ip):
    asn = asndb.lookup(ip)[0]

    org_name, org_country = as2isp.getISP("20221212", str(asn))
    return {"asn": asn, "org_name": org_name, "org_country": org_country}


def process_class():
    orglst = ["Highwinds", "Verizon", "Akamai", "Cloudflare", "Alibaba"]
    d = defaultdict(lambda : list())
    # TODO check cname by dig
    org_to_ulr_dict = defaultdict(lambda : list())
    f = open("top_30_with_category.json")
    dns_records = json.load(f)
    '''
    {'total_certs_crawled': 191156, 'is_delegated': False, 'type': 2, 'ip_to_org_info': {'151.139.128.14': {'asn': 20446, 'org_name': 'Highwinds Network Group, Inc.', 'org_country': 'US'}}}
    '''
    for key in dns_records:
        key_d = key[7:]
        ip_dict = dns_records[key]['ip_to_org_info']
        key = list(ip_dict.keys())[0]
        org = ip_dict[key]['org_name']
        org_match = False
        for o in orglst:
            if o in org:
                org_match = True
        if org_match:
            org_to_ulr_dict[org].append(key_d)
        # d[dns_records[key]['type']].append(key_d)
        #
    # labels = []
    # sizes = []
    # for key in d:
    #     print("{}: {}".format(key, len(d[key])))
    #     labels.append(key)
    #     sizes.append(len(d[key]))
    #
    #
    # explode = []  # only "explode" the 2nd slice (i.e. 'Hogs')
    # for size in sizes:
    #     if size >= 4:
    #         explode.append(.1)
    #     else:
    #         explode.append(0)
    #
    # fig1, ax1 = plt.subplots()
    # ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
    #         shadow=True, startangle=90)
    # ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    #
    # plt.show()


    for key in org_to_ulr_dict:
        print(key)
        print(":::::")
        for e in org_to_ulr_dict[key]:
            print(e)
        print("*******")


def process_ip_to_asn_org():
    # TODO check cname by dig

    f = open("dns.json")
    dns_records = json.load(f)

    f = open("top_30_ocsp_responders.json")
    top_responders = json.load(f)
    top_responders = [element['url'] for element in top_responders]

    has_cname = {}
    dns_record_dict = defaultdict(lambda : set())
    ip_to_asn_org_dict = defaultdict(lambda : {})
    # url_to_a_record = defaultdict(lambda : list())
    for dns_record in dns_records:
        url, record_type, record = dns_record['url'], dns_record['type'], dns_record['record']
        if url not in top_responders:
            continue
        if record_type == "CNAME":
            has_cname[url] = get_cname_chain(url)
            # print("{} : {}".format(url, get_cname_chain(url)))
        else:
            dns_record_dict[url].add(record)
            if url in ip_to_asn_org_dict[record]:
                continue
            ip_to_asn_org_dict[url][record] = get_asn_and_org(ip=record)

    f = open("ocsp_top_30.json")
    top_responders = json.load(f)['top_30_ocsp_responders']

    for top_responder in top_responders:
        if 'cname' in top_responders[top_responder]:
            top_responders[top_responder]['cname'] = has_cname[top_responder]
        top_responders[top_responder]['ip_to_org_info'] = {}
        for ip in dns_record_dict[top_responder]:
            top_responders[top_responder]['ip_to_org_info'][ip] = ip_to_asn_org_dict[top_responder][ip]

    with open("top_30_with_org.json", "w") as ouf:
        json.dump(top_responders, fp=ouf, indent=2)


process_class()
