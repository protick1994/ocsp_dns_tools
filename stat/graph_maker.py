import matplotlib.pyplot as plt
import numpy as np
import json
from os import listdir
from os.path import isfile, join
import seaborn as sns
from collections import defaultdict
from tabulate import tabulate
from pathlib import Path
import matplotlib.cm as cm
from matplotlib.colors import Normalize

from collections import defaultdict

import os
def bar_plot():
    retset = set()
    files = []
    for f in os.listdir("pii"):
        if f.endswith('.json') and 'count' not in f:
            retset.add('pii' + "/" + f)  # Need to track path as well

    key_to_color = dict()
    key_set = set()
    for file in retset:
        f = open(file)
        d = json.load(f)
        for key in d:
            key_set.add(key)
    import random
    for key in key_set:
        r = random.random()
        b = random.random()
        g = random.random()
        color = (r, g, b)
        key_to_color[key] = color

    for file in retset:
        #print(file)
        name_to_save = file[: -5]
        name_to_save = name_to_save[len('pii/final_ans_'): ]
        #print(name_to_save)

        f = open(file)
        d = json.load(f)
        keys = list(d.keys())
        keys.sort()
        x = []
        y = []
        cols = []
        import matplotlib.patches as mpatches
        patches = []
        #patches.append(mpatches.Patch(label='{}: {}'.format(0, "ASN 397455 - The Hancock Telephone Company")))



        for key in keys:
            x.append(key)
            y.append(d[key])
            cols.append(key_to_color[key])
            patches.append(mpatches.Patch(label='{}:  {} apks'.format(key, d[key])))



        x_pos = [i for i, _ in enumerate(x)]

        fig = plt.figure(figsize=(10, 10))
        plt.rcParams["font.weight"] = "bold"
        plt.rcParams["axes.labelweight"] = "bold"
        plt.style.use('ggplot')
        plt.bar(x_pos, y, color=cols)
        plt.xlabel("PII type")
        plt.ylabel("APK count")
        plt.legend(handles=patches, handlelength=0, handletextpad=0, loc='upper right', bbox_to_anchor=(1, 1),
                   ncol=1, fancybox=True, shadow=True, fontsize=11)
        plt.title(name_to_save)

        plt.xticks(x_pos, x, rotation='60')


        plt.savefig('pii_graphs/{}.png'.format(name_to_save), bbox_inches="tight")
        plt.clf()

def pie_chart():
    retset = set()
    files = []
    for f in os.listdir("pii"):
        if f.endswith('.json') and 'count' in f:
            retset.add('pii' + "/" + f)  # Need to track path as well

    key_to_color = dict()
    key_set = set()
    for file in retset:
        f = open(file)
        d = json.load(f)
        for key in d:
            key_set.add(key)
    import random
    for key in key_set:
        r = random.random()
        b = random.random()
        g = random.random()
        color = (r, g, b)
        key_to_color[key] = color

    for file in retset:

        name_to_save = file[: -5]
        name_to_save = name_to_save[len('final_ans_count_'): ]

        f = open(file)
        d = json.load(f)
        keys = list(d.keys())
        keys.sort()

        x = []
        labels = []

        cols = []
        for key in keys:
            if key == 'phone':
                continue
            labels.append(key)
            x.append(d[key])
            cols.append(key_to_color[key])

        fig, ax = plt.subplots(figsize=(6, 6))

        patches, texts, pcts = ax.pie(
            x, labels=labels, autopct='%.1f%%',
            wedgeprops={'linewidth': 3.0, 'edgecolor': 'white'},
            textprops={'size': 'x-large'},
            startangle=90)
        # For each wedge, set the corresponding text label color to the wedge's
        # face color.
        for i, patch in enumerate(patches):
            texts[i].set_color(patch.get_facecolor())
        plt.setp(pcts, color='white')
        plt.setp(texts, fontweight=600)
        ax.set_title('PII distribution for {}'.format(name_to_save), fontsize=18)
        plt.tight_layout()
        plt.savefig('pii_graphs/piechart-{}.png'.format(name_to_save), bbox_inches="tight")
        plt.clf()





def process_browser_json():
    result_dir = 'browser_final'
    run_result_dir = [result_dir + "/" + f for f in listdir(result_dir)]
    m_dict = defaultdict(lambda: defaultdict(lambda: list()))

    count = 0

    for dir in run_result_dir:
        count = count + 1
        if count == 3:
            break
        run = int(dir[dir.rfind("-") + 1:])
        for sub_dir in [dir + "/" + f for f in listdir(dir)]:
            mode = sub_dir[sub_dir.rfind("/") + 1:]
            ac_sub_dir = sub_dir + "/time_data"
            for leaf_json in [ac_sub_dir + "/" + f for f in listdir(ac_sub_dir)]:
                web_name = leaf_json[leaf_json.rfind("/") + 1:]
                f = open(leaf_json)
                d = json.load(f)

                semi_dict = {}
                semi_dict['secureConnectionStart'] = d['navigation_timing'][0]['secureConnectionStart']
                semi_dict['connectEnd'] = d['navigation_timing'][0]['connectEnd']
                semi_dict['requestStart'] = d['navigation_timing'][0]['requestStart']

                semi_dict['domainLookupEnd'] = d['navigation_timing'][0]['domainLookupEnd']
                semi_dict['domainLookupStart'] = d['navigation_timing'][0]['domainLookupStart']
                semi_dict['dns_time'] = semi_dict['domainLookupEnd'] - semi_dict['domainLookupStart']

                semi_dict['ssl_connection_time_con_end'] = semi_dict['connectEnd'] - semi_dict['secureConnectionStart']
                semi_dict['ssl_connection_time_app_data'] = semi_dict['requestStart'] - semi_dict['secureConnectionStart']

                child_dict = {}
                if 'resource_timing' in d and isinstance(d["resource_timing"], list):
                    for element in d["resource_timing"]:
                        e_dict = {}
                        e_dict['secureConnectionStart'] = element['secureConnectionStart']
                        e_dict['connectEnd'] = element['connectEnd']
                        e_dict['requestStart'] = element['requestStart']

                        e_dict['domainLookupEnd'] = element['domainLookupEnd']
                        e_dict['domainLookupStart'] = element['domainLookupStart']
                        e_dict['dns_time'] = e_dict['domainLookupEnd'] - e_dict['domainLookupStart']

                        e_dict['ssl_connection_time_con_end'] = element['connectEnd'] - element['secureConnectionStart']
                        e_dict['ssl_connection_time_app_data'] = element['requestStart'] - element[
                            'secureConnectionStart']

                        child_dict[element['name']] = e_dict

                semi_dict['resources'] = child_dict

                m_dict[web_name][mode].append((run, semi_dict))

    with open("web_browser.json", "w") as ouf:
        json.dump(m_dict, fp=ouf)


def browser_graph_multiple_runs():
    result_dir = 'results_V2'
    run_result_dir = [result_dir + "/" + f for f in listdir(result_dir)]
    m_dict = defaultdict(lambda: defaultdict(lambda: list()))
    for dir in run_result_dir:
        run = int(dir[dir.rfind("-") + 1:])
        for sub_dir in [dir + "/" + f for f in listdir(dir)]:
            mode = sub_dir[sub_dir.rfind("/") + 1:]
            ac_sub_dir = sub_dir + "/time_data"
            for leaf_json in [ac_sub_dir + "/" + f for f in listdir(ac_sub_dir)]:
                web_name = leaf_json[leaf_json.rfind("/") + 1:]
                f = open(leaf_json)
                d = json.load(f)
                ssl_connection_time_con_end = d['navigation_timing'][0]['connectEnd'] - d['navigation_timing'][0]['secureConnectionStart']
                ssl_connection_time_app_data = d['navigation_timing'][0]['requestStart'] - d['navigation_timing'][0]['secureConnectionStart']
                m_dict[web_name][mode].append((run, ssl_connection_time_con_end, ssl_connection_time_app_data, d))

    data_dict_tuple = (defaultdict(lambda: dict()), defaultdict(lambda: dict()))
    import statistics
    for web in m_dict:
        for mode in m_dict[web]:
            ssl_connection_time_con_end_median = statistics.median([e[1] for e in m_dict[web][mode]])
            ssl_connection_time_con_app_data = statistics.median([e[2] for e in m_dict[web][mode]])

            data_dict_tuple[0][mode][web] = ssl_connection_time_con_end_median
            data_dict_tuple[1][mode][web] = ssl_connection_time_con_app_data

    a = 1

    final_dict = defaultdict(lambda: dict())

    y_dict = defaultdict(lambda: list())
    x_labels = []
    for mode in data_dict_tuple[0]:
        for web in data_dict_tuple[0][mode]:
            final_dict[web][mode] = data_dict_tuple[0][mode][web]

    a = 1

    for web in final_dict:
        is_redirected = False

        for mode in final_dict[web]:
            if final_dict[web][mode] == 0:
                is_redirected = True
                break
        total_modes = len(final_dict[web].keys())
        if not is_redirected and total_modes == 4:
            x_labels.append(web[: -5])
            for mode in final_dict[web]:
                y_dict[mode].append(final_dict[web][mode])

    with open("y_dict.json", "w") as ouf:
        json.dump(y_dict, fp=ouf)

    with open("x_labels.json", "w") as ouf:
        json.dump(x_labels, fp=ouf)

    f = open('y_dict.json')
    y_dict = json.load(f)

    f = open('x_labels.json')
    x_labels = json.load(f)

    mode_lst = list(y_dict.keys())
    for mode_1_index in range(len(mode_lst)):
        for mode_2_index in range(mode_1_index + 1, len(mode_lst)):
            mode_1 = mode_lst[mode_1_index]
            mode_2 = mode_lst[mode_2_index]
            if mode_1 == mode_2:
                continue
            fig, axs = plt.subplots(1, 1, figsize=(50, 10))

            x = [i for i in range(0, len(x_labels))]

            axs.set_title("Results")
            #for mode in y_dict:
            sample_size = 200
            axs.plot(x[0:sample_size], y_dict[mode_1][0:sample_size], marker='.')
            axs.plot(x[0:sample_size], y_dict[mode_2][0:sample_size], marker='.')

            plt.ylabel("Latency in millisecond")
            plt.xticks(x[0:sample_size], x_labels[0:sample_size], rotation='vertical')
            axs.legend([mode_1, mode_2])

            Path('browser_graphs_v4/').mkdir(parents=True, exist_ok=True)
            # plt.suptitle("{} \n served by: {}".format(url, cdn), fontsize=20)

            plt.savefig('browser_graphs_v4/{}-{}.png'.format(mode_1, mode_2), bbox_inches="tight")
            plt.clf()


def make_browser_graph():
    f = open('browser_result.json')
    d = json.load(f)
    from collections import defaultdict

    count_dilemma = [0, 0]
    g_d = {}
    for e in d:
        if d[e]['base_diff'] < 0:
            count_dilemma[1] = 1 + count_dilemma[1]
        elif d[e]['base_diff'] > 0:
            count_dilemma[0] = 1 + count_dilemma[0]
            if d[e]['base_diff'] > 10:
                g_d[e] = (d[e]['base_diff'], d[e]['pre'], d[e]['post'], len(d[e].keys()))

    #print(len(list(g_d.items())))
    g_d = dict(sorted(list(g_d.items()), key=lambda item: item[1][0]))
    g_d = dict(list(g_d.items())[0: 50])

    fig, axs = plt.subplots(1, 1, figsize=(30, 10))

    x = [i for i in range(0, len(g_d.keys()))]

    x_trunc = []
    x_labels = []
    index = 0
    items = list(g_d.items())
    while index < len(items):
        x_trunc.append(index)
        x_labels.append(items[index][0][: -5])
        index += 1


    y1 = [(e[1][1] + 50) for e in g_d.items()]
    y2 = [(e[1][2] + 50) for e in g_d.items()]

    axs.set_title("Results")
    axs.plot(x, y1, marker='.')
    axs.plot(x, y2, marker='.')
    plt.ylabel("Latency in millisecond")
    plt.xticks(x, x_labels, rotation='vertical')
    axs.legend(["Latency without OCSP", "Latency with OCSP"])



    Path('browser_graphs/').mkdir(parents=True, exist_ok=True)
    #plt.suptitle("{} \n served by: {}".format(url, cdn), fontsize=20)

    plt.savefig('browser_graphs/graph.png', bbox_inches="tight")
    plt.clf()


def latency_table_maker():
    d = defaultdict(lambda: list())
    cache_result_jsons = ["../cache_exp/" + f for f in listdir("../cache_exp") if isfile(join("../cache_exp", f))]
    d = {}
    for cache_result_json in cache_result_jsons:

        f = open(cache_result_json)
        cache_dict = json.load(f)
        for label in cache_dict:
            if label == 'old':
                continue
            for serial in cache_dict[label]:
                latency_tuple_list = []
                for key in cache_dict[label][serial]:
                    latency_tuple_list.append((int(key), cache_dict[label][serial][key]['elapsed_time']))
                latency_tuple_list.sort()
                d[serial] = latency_tuple_list

                table = [['Iteration', 'Latency']]

                for e in latency_tuple_list:
                    l = [e[0], e[1]]
                    table.append(l)

                with open('tables/table_{}.txt'.format(serial), 'w') as f:
                    f.write(tabulate(table, headers='firstrow', tablefmt='grid'))


def get_root_domain(url):
    if url.startswith("http://"):
        url = url[7:]
    if "/" in url:
        url = url[0: url.find("/")]
    return url


def cache_exp_graph_maker():
    cache_result_jsons = ["jsons_v11/" + f for f in listdir("jsons_v11") if isfile(join("jsons_v11", f))]
    d = {}
    #cc = 0
    for cache_result_json in cache_result_jsons:
        try:
            # cc = cc + 1
            # if cc == 2:
            # break
            # if 'akamai2' in cache_result_json:
            #     continue
            cdn = None
            index = cache_result_json.find("-cache_exp")
            cdn = cache_result_json[: index]
            cdn = cdn[cdn.find("/") + 1:]

            f = open(cache_result_json)
            cache_dict = json.load(f)
            url = None
            # fig = plt.figure(figsize=(15, 5))
            fig, axs = plt.subplots(7, 1, figsize=(30, 30))

            index = 0
            url = cache_dict['url'][7:]
            url = url.replace("/", "\\")
            for label in cache_dict:
                if label == 'old':
                    continue
                if label == 'url':
                    url = cache_dict[label][7:]
                    url = url.replace("/", "\\")
                    continue
                if label == 'new':
                    verbose_label = cdn + " (Existing certificate)"
                if label == 'random':
                    verbose_label = cdn + " (Random certificate)"
                if label == 'random_dynamic':
                    verbose_label = cdn + " (Random (Different Certificates))"

                s_cnt = 0
                for serial in cache_dict[label]:
                    s_cnt += 1
                    if label == 'new' and s_cnt == 3:
                        break


                    x1 = []  # with_nonce
                    x2 = []  # without_nonce
                    y1 = []  # with_nonce
                    y2 = []  # without_nonce
                    y3 = []  # connection time wn
                    y4 = []  # connection time won
                    for mode in ['with_nonce', 'without_nonce']:
                        try:
                            verbose_label_ = verbose_label
                            latency_tuple_list = []
                            for key in cache_dict[label][serial][mode]:
                                try:
                                    latency_tuple_list.append((int(key),
                                                               cache_dict[label][serial][mode][key]['elapsed_time'],
                                                               cache_dict[label][serial][mode][key]['connection_time'],
                                                               cache_dict[label][serial][mode][key]['response_time']))
                                except Exception as e:
                                    pass
                            latency_tuple_list.sort()

                            # y1 = [e[2] for e in latency_tuple_list]
                            if mode == 'with_nonce':
                                x1 = [e[0] for e in latency_tuple_list]
                                y1 = [e[3] for e in latency_tuple_list]
                                y3 = [e[2] for e in latency_tuple_list]
                            else:
                                x2 = [e[0] for e in latency_tuple_list]
                                y2 = [e[3] for e in latency_tuple_list]
                                y4 = [e[2] for e in latency_tuple_list]
                            # y2 = [e[3] for e in latency_tuple_list]
                        except Exception as e:
                            p = 1

                    row = index
                    col = 0
                    print(verbose_label_, cdn)

                    axs[row].set_title(verbose_label_[len(cdn):])
                    axs[row].plot(x1, y1, marker='.')
                    axs[row].plot(x2, y2, marker='.')
                    axs[row].plot(x1, y3, marker='.')
                    axs[row].plot(x2, y4, marker='.')
                    axs[row].legend(["Total response time with nonce", "Total response time without nonce",
                                     "Connection time with nonce", "Connection time without nonce"], loc='upper right')

                    index += 1

            Path('exp_res_v8/{}/'.format(cdn)).mkdir(parents=True, exist_ok=True)
            plt.suptitle("{} \n served by: {}".format(url, cdn), fontsize=20)

            plt.savefig('exp_res_v8/{}/{}.png'.format(cdn, url), bbox_inches="tight")
            plt.clf()

        except Exception as e:
            pass


def cache_exp_pdf_maker():
    cache_result_jsons = ["../cache_exp/" + f for f in listdir("../cache_exp") if isfile(join("../cache_exp", f))]
    d = {}
    for cache_result_json in cache_result_jsons:
        if 'akamai2' in cache_result_json:
            continue
        cdn = None
        if 'akamai' in cache_result_json:
            cdn = 'akamai'
        if 'cloudflare' in cache_result_json:
            cdn = 'cloudflare'
        if cdn in d:
            continue
        d[cdn] = 1

        f = open(cache_result_json)
        cache_dict = json.load(f)
        for label in cache_dict:
            if label == 'old':
                continue
            if label == 'new':
                verbose_label = cdn + " (Existing certificate)"
            if label == 'random':
                verbose_label = cdn + " (Random certificate)"
            for serial in cache_dict[label]:
                latency_tuple_list = []
                for key in cache_dict[label][serial]:
                    latency_tuple_list.append((int(key), cache_dict[label][serial][key]['elapsed_time']))
                latency_tuple_list.sort()
                #x = [e[0] for e in latency_tuple_list]
                y = [e[1] for e in latency_tuple_list]
                fig = plt.figure(figsize=(10, 10))
                fig.suptitle(verbose_label, fontsize=20)
                ax = fig.add_subplot(111)
                sns.distplot(y, hist=False, kde=True,
                             kde_kws={'shade': True, 'linewidth': 3},
                             label='yo')
                ax.set_ylabel('density', fontsize=20)
                ax.set_xlabel('latency', fontsize=20)
                #plt.show()
                plt.savefig('cache_Exp/{}/{}/{}.png'.format(cdn, label, serial+"_pdf"))
                plt.clf()


def distribution_maker():
    f = open("ocsp.json")
    top_responders = json.load(f)
    print(len(top_responders))
    top_30_responders = top_responders[0: 30]
    bottom_30_responders = top_responders[-30:]
    responders_to_be_plotted = bottom_30_responders
    plt_title = 'Bottom 30 OCSP responders'

    responders = [element['url'][7:] for element in responders_to_be_plotted]
    responders.reverse()
    certificate_count = [int(element['c']) for element in responders_to_be_plotted]
    certificate_count.reverse()
    plt.figure(figsize=(12, 16))
    plt.barh(responders, certificate_count, height=.5, alpha=1)

    for index, value in enumerate(certificate_count):
        plt.text(value + .5, index - .2,
                 str(value))


    plt.title(plt_title)
    # plt.ylabel('Product')
    plt.xlabel('Certificate count')
    plt.tight_layout()
    # fig = plt.figure(figsize=(8, 36))
    # plt.yticks(np.arange(min(y_pos), max(y_pos), 5.0)
    plt.show()


def latency_graph_maker():
    f = open("latency_results.json")
    url_to_latency_dict = json.load(f)

    d = {}

    for url in url_to_latency_dict:
        d[url] = url_to_latency_dict[url]['average']

    d_tuples = sorted(d.items(), key=lambda x: x[1])

    min_ten = d_tuples[0: 20]
    max_ten = d_tuples[len(d_tuples) - 20:]

    urls = [e[0][7:] for e in min_ten]
    latency = [round(e[1], 2) for e in min_ten]


    plt.figure(figsize=(8, 8))
    plt.barh(urls, latency, height=.5, alpha=1)

    for index, value in enumerate(latency):
        plt.text(value + .5, index - .2,
                 str(value))


    plt.title('Least Avergae Latency')

    plt.tight_layout()

    plt.show()

def fix_json():
    f = open("latency_results.json")
    url_to_latency_dict = json.load(f)

    with open("latency_v2.json", "w") as ouf:
        json.dump(url_to_latency_dict, fp=ouf, indent=2)


from collections import defaultdict
# 233 dicts
def latency_by_asn():
    f = open("latency_results.json")
    url_to_latency_dict = json.load(f)

    min_candidate = defaultdict(lambda: 0)
    max_candidate = defaultdict(lambda: 0)

    for url in url_to_latency_dict:
        max_candidate[url_to_latency_dict[url]['max_latency'][1]] += 1
        min_candidate[url_to_latency_dict[url]['min_latency'][1]] += 1

    d = {}

    max_candidate = dict(max_candidate)
    min_candidate = dict(min_candidate)


    max_candidate = sorted(max_candidate.items(), key=lambda x: -x[1])[0: 5]
    min_candidate = sorted(min_candidate.items(), key=lambda x: -x[1])[0: 5]

    a = 1

    asns = [e[0] for e in min_candidate]
    count = [e[1] for e in min_candidate]

    plt.figure(figsize=(8, 8))
    plt.barh(asns, count, height=.5, alpha=1)

    for index, value in enumerate(count):
        plt.text(value + .5, index - .2,
                 str(value))

    plt.title('ASNs that showed minimum latency')

    plt.tight_layout()

    plt.show()


bar_plot()
