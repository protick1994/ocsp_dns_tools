from collections import defaultdict
import json

source_dir = "../ttl_result/"

def dict_union(dict_list):
    d = {}
    for dict_ in dict_list:
        for key in dict_:
            d[key] = dict_[key]
    return d


def get_org_dict():
    org_dict_list = []
    for ttl in [1, 5, 15, 30, 60]:
        f = open(source_dir + "{}/resolver_to_org_country.json".format(ttl))
        d = json.load(f)
        org_dict_list.append(d)
    org_dict = dict_union(org_dict_list)
    return org_dict


def parse_bind_apache_logs(base_file, child_base):
    org_dict = get_org_dict()
    d = {}
    identifier_to_ip = {}
    ip_set = set()
    # with open(base_file) as FileObj:
    #     for line in FileObj:
    #         try:
    #             real_line = line.strip()
    #             segments = real_line.split()
    #             a = 1
    #             ip = segments[2]
    #
    #
    #
    #             url = segments[3]
    #             type = segments[4]
    #             if type == 'A':
    #                 a = 1
    #                 identifier = url.split(".")[0]
    #                 if len(identifier) > 20:
    #                     ip_set.add(ip)
    #                     d[identifier] = 1
    #                     identifier_to_ip[identifier] = ip
    #         except Exception as e:
    #             print('parse bind apache logs ', e)

    identifier_to_count = defaultdict(lambda: 0)
    has_touched = {}

    with open(child_base) as FileObj:
        for line in FileObj:
            try:
                real_line = line.strip()
                segments = real_line.split()
                a = 1
                ip = segments[2]

                url = segments[3]
                type = segments[4]
                if type in ['A', 'AAAA']:
                    a = 1
                    identifier = url.split(".")[0]
                    if len(identifier) > 20:
                        if ip in org_dict:
                            if "Cisco" in org_dict[ip][0]:
                                a = 1
                        pre_identifier, bucket = identifier[: identifier.rfind("-")], identifier.split("-")[-1]
                        hash_ = identifier + type
                        if hash_ not in has_touched:
                            has_touched[hash_] = 1
                            identifier_to_count[pre_identifier] += 1
            except Exception as e:
                print('Parse bind apache logs ', e)

    corrupt_resolver_set = set()
    for identifier in identifier_to_count:
        if identifier_to_count[identifier] >= 74:
            if identifier in identifier_to_ip:
                corrupt_resolver_set.add(identifier_to_ip[identifier])



    whole_org_to_count = defaultdict(lambda : 0)
    corrupt_org_to_count = defaultdict(lambda: 0)
    for ip in ip_set:
        if ip in org_dict:
            whole_org_to_count[org_dict[ip][0]] += 1
    for ip in corrupt_resolver_set:
        if ip in org_dict:
            if "Cisco" in org_dict[ip][0]:
                a = 1
            corrupt_org_to_count[org_dict[ip][0]] += 1

    d = {
        "org_to_count_global": whole_org_to_count,
        "org_to_count_victim": corrupt_org_to_count,
        "all_resolvers": list(ip_set),
        "corrupt_resolvers": list(corrupt_resolver_set)
    }

    with open("sec_lab.json", "w") as ouf:
        json.dump(d, fp=ouf)
    # d, identifier_to_count


parse_bind_apache_logs("/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/base_log",
                       "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/log_child")


