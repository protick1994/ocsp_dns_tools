import json
from collections import defaultdict
from local import LOCAL
from pathlib import Path

f = open("/home/protick/ocsp_dns_django/apache_master/apache_1/apache_all.json")
req_id_to_meta = None


def load_tank():
    global req_id_to_meta
    req_id_to_meta = json.load(f)


info_paper = {}

lum_resolvers_asn = [15169, 20473, 36692, 14061, 30607, 24940, 27725]


class Inline(object):
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


def get_asn_count(correct_req_id_set, incorrect_req_id_set):
    # req_id_to_meta[identifier] = (meta["client_ip"], datetime.timestamp(meta["date"]), asn)
    asn_set = set()
    for req_id in incorrect_req_id_set:
        if req_id in req_id_to_meta:
            asn_set.add(req_id_to_meta[req_id][2])
    return len(asn_set)


master_mozambiq = []
meta_data_dict = Inline()

meta_data_dict.exitnode_set = set()
meta_data_dict.ttl_to_exitnode_set = defaultdict(lambda: set())

meta_data_dict.resolver_set = set()
meta_data_dict.ttl_to_resolver_set = defaultdict(lambda: set())

meta_data_dict.asn_set = set()
meta_data_dict.ttl_to_asn_set = defaultdict(lambda: set())

meta_data_dict.local_resolver_set = set()
meta_data_dict.ttl_to_local_resolver_set = defaultdict(lambda: set())

meta_data_dict.local_resolver_set_5 = set()
meta_data_dict.ttl_to_local_resolver_set_5 = defaultdict(lambda: set())

meta_data_dict.public_resolver_set = set()
meta_data_dict.ttl_to_public_resolver_set = defaultdict(lambda: set())

meta_data_dict.public_resolver_set_5 = set()
meta_data_dict.ttl_to_public_resolver_set_5 = defaultdict(lambda: set())

meta_data_dict.global_public_local_dict = {}

meta_data_dict.excluded_lum_resolver_set = set()


def fix_sets():
    meta_data_dict.exitnode_set = list(meta_data_dict.exitnode_set)
    meta_data_dict.resolver_set = list(meta_data_dict.resolver_set)
    meta_data_dict.asn_set = list(meta_data_dict.asn_set)
    meta_data_dict.local_resolver_set = list(meta_data_dict.local_resolver_set)
    meta_data_dict.local_resolver_set_5 = list(meta_data_dict.local_resolver_set_5)
    meta_data_dict.public_resolver_set = list(meta_data_dict.public_resolver_set)
    meta_data_dict.public_resolver_set_5 = list(meta_data_dict.public_resolver_set_5)
    meta_data_dict.excluded_lum_resolver_set = list(meta_data_dict.excluded_lum_resolver_set)

    for ttl in meta_data_dict.ttl_to_exitnode_set:
        meta_data_dict.ttl_to_exitnode_set[ttl] = list(meta_data_dict.ttl_to_exitnode_set[ttl])
    for ttl in meta_data_dict.ttl_to_resolver_set:
        meta_data_dict.ttl_to_resolver_set[ttl] = list(meta_data_dict.ttl_to_resolver_set[ttl])
    for ttl in meta_data_dict.ttl_to_asn_set:
        meta_data_dict.ttl_to_asn_set[ttl] = list(meta_data_dict.ttl_to_asn_set[ttl])
    for ttl in meta_data_dict.ttl_to_local_resolver_set:
        meta_data_dict.ttl_to_local_resolver_set[ttl] = list(meta_data_dict.ttl_to_local_resolver_set[ttl])
    for ttl in meta_data_dict.ttl_to_local_resolver_set_5:
        meta_data_dict.ttl_to_local_resolver_set_5[ttl] = list(meta_data_dict.ttl_to_local_resolver_set_5[ttl])
    for ttl in meta_data_dict.ttl_to_public_resolver_set:
        meta_data_dict.ttl_to_public_resolver_set[ttl] = list(meta_data_dict.ttl_to_public_resolver_set[ttl])
    for ttl in meta_data_dict.ttl_to_public_resolver_set_5:
        meta_data_dict.ttl_to_public_resolver_set_5[ttl] = list(meta_data_dict.ttl_to_public_resolver_set_5[ttl])


if LOCAL:
    source_dir = "../ttl_result/"
else:
    source_dir = "/home/protick/ocsp_dns_django/ttl_result_v2/"

allowed_ttls = [1, 5, 15, 30, 60]

master_dict = {}
resultant_dump = {}


def dict_union(dict_list):
    d = {}
    for dict_ in dict_list:
        for key in dict_:
            d[key] = dict_[key]
    return d


def master_dict_sanity():
    for key in master_dict:
        print("TTL {}".format(key))
        for k in master_dict[key]:
            print("CLD {}".format(k))


def get_resolver_to_asn_dict():
    # master_dict_sanity()
    dict_list = []
    for ttl in allowed_ttls:
        nested_dict = master_dict[ttl]["final_resolver_to_asn"]["resolver_to_asn_own"]
        meta_data_dict.ttl_to_resolver_set[ttl] = set(list(nested_dict.keys()))
        dict_list.append(nested_dict)

    resolver_to_asn_global = dict_union(dict_list)
    meta_data_dict.resolver_set = set(list(resolver_to_asn_global.keys()))
    master_dict["resolver_to_asn_global"] = resolver_to_asn_global


def get_all_data():
    for ttl in allowed_ttls:
        master_dict[ttl] = {}

    file_list = ["final_resolver_to_asn", "resolver_to_org_country", "resolver_public_local_dict", "final_data"]

    for ttl in allowed_ttls:
        for f in file_list:
            fl = open("{}{}/{}.json".format(source_dir, ttl, f))
            d = json.load(fl)
            master_dict[ttl][f] = d

    # return master_dict


def get_all_data_only_org():
    for ttl in allowed_ttls:
        master_dict[ttl] = {}

    file_list = ["resolver_to_org_country"]

    for ttl in allowed_ttls:
        for f in file_list:
            fl = open("{}{}/{}.json".format(source_dir, ttl, f))
            d = json.load(fl)
            master_dict[ttl][f] = d

    # return master_dict


def get_unified_field(file_name):
    dict_list = []
    for ttl in allowed_ttls:
        dict_list.append(master_dict[ttl][file_name])

    unified_dict = dict_union(dict_list)
    return unified_dict


def resolver_distribution():
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in global_resolver_to_org_country:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    resultant_dump["geographic_resolver_distribution"] = country_code_to_count_map


def cdf_data():
    resolver_to_asn_global = master_dict['resolver_to_asn_global']
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = {}

    # meta_data_dict = Inline()

    pub_loc_dict_list = []

    total_10_exitnode_resolvers = set()
    total_10_exitnode_exitnodes = set()
    total_10_exitnode_wrong_exitnodes = set()
    bad_exitnodes_set_by_ttl = defaultdict(lambda: set())

    middle_resolver_set = list()
    middle_resolver_with_org_list = list()

    for ttl in allowed_ttls:
        ratio_cdf_dict[ttl] = {}
        ratio_cdf_dict[ttl]["local"] = {}
        ratio_cdf_dict[ttl]["public"] = {}
        ratio_cdf_dict[ttl]["all"] = {}
        for k in ratio_cdf_dict[ttl]:
            for t_h in [10]:
                ratio_cdf_dict[ttl][k][t_h] = {}
                ratio_cdf_dict[ttl][k][t_h]["ans"] = []
                ratio_cdf_dict[ttl][k][t_h]["inc_res_set"] = []
                ratio_cdf_dict[ttl][k][t_h]["cor_res_set"] = []
                ratio_cdf_dict[ttl][k][t_h]["all_set"] = []

        is_public = master_dict[ttl]["resolver_public_local_dict"]
        pub_loc_dict_list.append(is_public)
        d = master_dict[ttl]["final_data"]
        final_dict = d["data_elaborate"]
        ex_node_set = set()
        excluded_lum_resolver = []

        for key in final_dict:
            try:
                if int(resolver_to_asn_global[key]) in lum_resolvers_asn:
                    excluded_lum_resolver.append(key)
                    meta_data_dict.excluded_lum_resolver_set.add(key)
                    continue
            except:
                pass

            # append((req_id, ip_hash))
            correct_set = set()
            incorrect_set = set()
            correct_req_id_set = set()
            incorrect_req_id_set = set()
            for e in final_dict[key]["ic"]:
                incorrect_set.add(e[1])
                incorrect_req_id_set.add(e[0])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            for e in final_dict[key]["c"]:
                correct_set.add(e[1])
                correct_req_id_set.add(e[0])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            subtotal = correct_set.union(incorrect_set)
            total = len(subtotal)

            ratio = len(incorrect_set) / total

            threshold_to_check = [10]

            is_pub_resol = -1
            if key in is_public:
                if is_public[key]:
                    is_pub_resol = True
                    meta_data_dict.ttl_to_public_resolver_set[ttl].add(key)
                    meta_data_dict.public_resolver_set.add(key)
                else:
                    is_pub_resol = False
                    meta_data_dict.ttl_to_local_resolver_set[ttl].add(key)
                    meta_data_dict.local_resolver_set.add(key)

            for loc_glo_key in ratio_cdf_dict[ttl]:
                if loc_glo_key != "all":
                    if key not in is_public:
                        continue
                    if loc_glo_key == "local":
                        if is_public[key]:
                            continue
                    if loc_glo_key == "public":
                        if not is_public[key]:
                            continue

                for threshold in threshold_to_check:
                    if total >= threshold:

                        total_10_exitnode_resolvers.add(key)
                        total_10_exitnode_exitnodes.update(subtotal)
                        total_10_exitnode_wrong_exitnodes.update(incorrect_set)

                        if threshold == 10:
                            if is_pub_resol != -1:
                                if is_pub_resol:
                                    meta_data_dict.public_resolver_set_5.add(key)
                                    meta_data_dict.ttl_to_public_resolver_set_5[ttl].add(key)
                                else:
                                    meta_data_dict.local_resolver_set_5.add(key)
                                    meta_data_dict.ttl_to_local_resolver_set_5[ttl].add(key)

                        # if loc_glo_key == "public" and ratio >= .1 and ratio <= .9:
                        #     asn_count = get_asn_count(correct_req_id_set, incorrect_req_id_set)
                        #     master_mozambiq.append((ratio, asn_count))

                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["ans"].append(ratio)

                        # if loc_glo_key == "public":
                        #     if 0 < ratio < 1:
                        #         middle_resolver_set.append((key, ratio))

                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["all_set"].append(key)
                        if ratio >= .9:
                            bad_exitnodes_set_by_ttl[ttl].update(incorrect_set)

                            ratio_cdf_dict[ttl][loc_glo_key][threshold]["inc_res_set"].append(key)
                        elif ratio <= .1:
                            ratio_cdf_dict[ttl][loc_glo_key][threshold]["cor_res_set"].append(key)

    meta_data_dict.global_public_local_dict = dict_union(pub_loc_dict_list)
    master_dict["ratio_cdf_dict"] = ratio_cdf_dict
    resultant_dump["ratio_cdf_dict"] = ratio_cdf_dict

    info_paper["total_10_exitnode_resolvers"] = len(total_10_exitnode_resolvers)
    info_paper["total_10_exitnode_exitnodes"] = len(total_10_exitnode_exitnodes)
    info_paper["total_10_exitnode_wrong_exitnodes"] = len(total_10_exitnode_wrong_exitnodes)
    for ttl in bad_exitnodes_set_by_ttl:
        str = "bad_exitnodes_set_by_{}".format(ttl)
        info_paper[str] = len(bad_exitnodes_set_by_ttl[ttl])
    resultant_dump["info_paper"] = info_paper
    print(info_paper)


def geographic_correct_incorrect_distribution():
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = master_dict["ratio_cdf_dict"]

    geo_distro = {}

    for ttl in allowed_ttls:
        geo_distro[ttl] = {}

        country_code_to_count_map = defaultdict(lambda: 0)
        for resolver in ratio_cdf_dict[ttl]["all"][5]["inc_res_set"]:
            country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
        geo_distro[ttl]["incorrect"] = country_code_to_count_map

        country_code_to_count_map = defaultdict(lambda: 0)
        for resolver in ratio_cdf_dict[ttl]["all"][5]["cor_res_set"]:
            country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
        geo_distro[ttl]["correct"] = country_code_to_count_map

    resultant_dump["geographic_corr_incorr_distro"] = geo_distro


def geographic_correct_incorrect_distribution_all_over():
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = master_dict["ratio_cdf_dict"]

    inc_set = set()
    cor_set = set()
    all_set = set()

    for ttl in allowed_ttls:
        for resolver in ratio_cdf_dict[ttl]["all"][5]["inc_res_set"]:
            inc_set.add(resolver)

        for resolver in ratio_cdf_dict[ttl]["all"][5]["cor_res_set"]:
            cor_set.add(resolver)

        for resolver in ratio_cdf_dict[ttl]["all"][5]["all_set"]:
            all_set.add(resolver)

    geo_distro = {}

    country_code_to_count_map = defaultdict(lambda: 0)

    for resolver in inc_set:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    geo_distro["incorrect"] = country_code_to_count_map

    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in cor_set:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    geo_distro["correct"] = country_code_to_count_map

    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in all_set:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    geo_distro["all"] = country_code_to_count_map

    resultant_dump["geographic_corr_incorr_distro_global"] = geo_distro


def venn_diagram_info():
    ratio_cdf_dict = master_dict["ratio_cdf_dict"]

    mother_dict = {}

    for ttl in allowed_ttls:
        mother_dict[ttl] = {}

        inc_set = set()
        for resolver in ratio_cdf_dict[ttl]["all"][5]["inc_res_set"]:
            inc_set.add(resolver)
        mother_dict[ttl]["ttl_dis_honor"] = list(inc_set)

        cor_set = set()
        for resolver in ratio_cdf_dict[ttl]["all"][5]["cor_res_set"]:
            cor_set.add(resolver)
        mother_dict[ttl]["ttl_honor"] = list(cor_set)

    resultant_dump["venn_info"] = mother_dict


def is_normal_http_response(str):
    if "phase1" in str or "phase2" in str:
        return True
    return False


def http_response_class():
    wrong_asn_set = set()
    correct_asn_set = set()

    for ttl in allowed_ttls:
        f = open("{}{}/http_response_to_asn_list.json".format(source_dir, ttl))
        d = json.load(f)
        for key in d:
            if not is_normal_http_response(key):
                for asn in d[key]:
                    wrong_asn_set.add(asn)
            if is_normal_http_response(key):
                for asn in d[key]:
                    correct_asn_set.add(asn)

    resultant_dump["http_response_info"] = {
        "i_c_s": list(wrong_asn_set),
        "c_s": list(correct_asn_set)
    }


def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    return json.JSONEncoder.default(obj)


def proc_meta_info():
    meta_info = []
    meta_info.append("Total exit-nodes: {}".format(len(meta_data_dict.exitnode_set)))
    for ttl in meta_data_dict.ttl_to_exitnode_set:
        meta_info.append("Total exit-nodes for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_exitnode_set[ttl])))

    # meta_info.append("Total exit-nodes: {}".format(len(meta_data_dict.exitnode_set)))
    # for ttl in meta_data_dict.ttl_to_exitnode_set:
    #     meta_info.append("Total exit-nodes for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_exitnode_set[ttl])))

    meta_info.append("Total resolvers: {}".format(len(meta_data_dict.resolver_set)))
    for ttl in meta_data_dict.ttl_to_resolver_set:
        meta_info.append("Total resolvers for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_resolver_set[ttl])))

    meta_info.append("Total local resolvers: {}".format(len(meta_data_dict.local_resolver_set)))
    for ttl in meta_data_dict.ttl_to_local_resolver_set:
        meta_info.append(
            "Total local resolvers for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_local_resolver_set[ttl])))

    meta_info.append("Total public resolvers: {}".format(len(meta_data_dict.public_resolver_set)))
    for ttl in meta_data_dict.ttl_to_public_resolver_set:
        meta_info.append(
            "Total public resolvers for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_public_resolver_set[ttl])))

    meta_info.append(
        "Total local resolvers with atleast 5 exitnodes: {}".format(len(meta_data_dict.local_resolver_set_5)))
    for ttl in meta_data_dict.ttl_to_local_resolver_set_5:
        meta_info.append(
            "Total local resolvers with atleast 5 exitnodes for ttl {}: {}".format(ttl, len(
                meta_data_dict.ttl_to_local_resolver_set_5[ttl])))

    meta_info.append(
        "Total public resolvers with atleast 5 exitnodes: {}".format(len(meta_data_dict.public_resolver_set_5)))
    for ttl in meta_data_dict.ttl_to_public_resolver_set_5:
        meta_info.append(
            "Total public resolvers with atleast 5 exitnodes for ttl {}: {}".format(ttl, len(
                meta_data_dict.ttl_to_public_resolver_set_5[ttl])))

    return meta_info


def get_middle_req_org_rank():
    f = open("/home/protick/ocsp_dns_django/preprocessed_middle_req_log/bind/middle_req.json")
    d = json.load(f)
    resolver_to_count = defaultdict(lambda: 0)
    total_resolvers = 0
    total_identifiers = 0
    for ttl in d:
        if not str(ttl) in ["60"]:
            continue
        data = d[ttl]
        total_resolvers = len(list(data.keys()))
        for resolver in list(data.keys()):
            tot_identifiers = len(list(data[resolver].keys()))
            resolver_to_count[resolver] = tot_identifiers
            total_identifiers += tot_identifiers

    org_to_count = defaultdict(lambda: 0)
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")
    for resolver in resolver_to_count:
        try:
            org = global_resolver_to_org_country[resolver][0]
            org_to_count[org] += resolver_to_count[resolver]
        except:
            pass

    d_ = {
        "total_resolvers": total_resolvers,
        "total_identifers": total_identifiers,
        "org_count": org_to_count,
        "resolver_to_count": resolver_to_count
    }

    resultant_dump["middle_req_rank_info"] = d_


def table_maker_preprocess():
    global_resolver_to_asn = master_dict["resolver_to_asn_global"]
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")
    is_public = get_unified_field("resolver_public_local_dict")

    org_to_cn = {}
    org_Set = set()
    org_to_asn_marker = defaultdict(lambda: dict())
    org_to_asn_count = defaultdict(lambda: 0)
    org_to_resolver_marker = defaultdict(lambda: dict())
    org_to_resolver_count = defaultdict(lambda: 0)

    org_to_incorrect_resolvers = defaultdict(lambda: set())
    org_to_correct_resolvers = defaultdict(lambda: set())

    org_to_local_resolvers = defaultdict(lambda: set())
    org_to_public_resolvers = defaultdict(lambda: set())

    org_to_exitnode_set = defaultdict(lambda: set())
    org_to_incorrect_exitnode_set = defaultdict(lambda: set())

    for ttl in allowed_ttls:
        d = master_dict[ttl]["final_data"]
        final_dict = d["data_elaborate"]

        for key in final_dict:
            asn = "undefined"
            if key in global_resolver_to_asn:
                asn = global_resolver_to_asn[key]

            org = "undefined"
            cn = "undefined"
            if key in global_resolver_to_org_country:
                org, cn = global_resolver_to_org_country[key]

            if asn not in org_to_asn_marker[org]:
                org_to_asn_marker[org][asn] = 1
                org_to_asn_count[org] += 1

            org_Set.add(org)
            org_to_cn[org] = cn

            if key not in org_to_resolver_marker[org]:
                org_to_resolver_marker[org][key] = 1
                org_to_resolver_count[org] += 1

            correct_set = set()
            incorrect_set = set()

            for e in final_dict[key]["ic"]:
                incorrect_set.add(e[1])
            for e in final_dict[key]["c"]:
                correct_set.add(e[1])

            total_set = correct_set.union(incorrect_set)
            total = len(total_set)

            if total >= 10:
                org_to_exitnode_set[org].update(total_set)
                org_to_incorrect_exitnode_set[org].update(incorrect_set)
                ratio = len(incorrect_set) / total
                if ratio >= .90:
                    org_to_incorrect_resolvers[org].add(key)
                if ratio <= .1:
                    org_to_correct_resolvers[org].add(key)
                if key in is_public:
                    if is_public[key]:
                        org_to_public_resolvers[org].add(key)
                    else:
                        org_to_local_resolvers[org].add(key)


    summary = []
    for org in org_Set:
        tot_exitnodes = len(org_to_exitnode_set[org])
        inc_exitnodes = len(org_to_incorrect_exitnode_set[org])
        perc = 0
        if tot_exitnodes > 0:
            perc = inc_exitnodes / tot_exitnodes
        element = Inline()
        element.org = org
        element.cn = org_to_cn[org]
        element.tot_exitnodes = tot_exitnodes
        element.inc_exitnodes = inc_exitnodes
        element.inc_exit_perc = perc
        element.total_asns = org_to_asn_count[org]
        element.total_resolvers = org_to_resolver_count[org]
        element.total_incorrect_resolvers = len(org_to_incorrect_resolvers[org])
        element.total_correct_resolvers = len(org_to_correct_resolvers[org])
        element.total_public_resolvers = len(org_to_public_resolvers[org])
        element.total_local_resolvers = len(org_to_local_resolvers[org])
        summary.append(element)

    resultant_dump["table_data_global"] = summary


def init():
    # load_tank()
    get_all_data()
    # TODO change
    # get_all_data_only_org()
    # print("get_all_data")
    # master_dict_sanity()
    # print("Separator")
    get_resolver_to_asn_dict()
    print("get_resolver_to_asn_dict")

    # resolver_distribution()
    # print("resolver_distribution")

    # cdf_data()
    # print("cdf_data")

    # geographic_correct_incorrect_distribution()
    # print("geographic_correct_incorrect_distribution")
    # geographic_correct_incorrect_distribution_all_over()
    # print("geographic_correct_incorrect_distribution_all_over")
    # venn_diagram_info()
    # print("venn_diagram_info")
    # http_response_class()
    # print("http_response_class")
    # get_middle_req_org_rank()
    # print("get_middle_req_org_rank")

    try:
        table_maker_preprocess()
        print("table maker pre process")
    except:
        pass

    dump_directory = "graph_data_pre_v_22/"
    Path(dump_directory).mkdir(parents=True, exist_ok=True)

    # for key in resultant_dump:
    #     with open(dump_directory + "{}.json".format(key), "w") as ouf:
    #         json.dump(resultant_dump[key], fp=ouf)
    # with open(dump_directory + "{}.json".format("all_fk"), "w") as ouf:
    #     json.dump(resultant_dump, fp=ouf)

    with open(dump_directory + "table_final.json", "w") as ouf:
        json.dump(resultant_dump["table_data_global"], fp=ouf, default=lambda o: o.__dict__)
