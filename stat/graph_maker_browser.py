import json
from collections import defaultdict
from os import listdir


def proc_url(url):
    if url.startswith("https://"):
        url = url[8:]
    elif url.startswith("http://"):
        url = url[7:]
    url = url[: url.find("/")]
    return url


def find_urls():
    url_set = set()
    result_dir = 'results_V2'
    run_result_dir = [result_dir + "/" + f for f in listdir(result_dir)]

    for dir in run_result_dir:
        if 'run' not in dir:
            continue
        for sub_dir in [dir + "/" + f for f in listdir(dir)]:
            ac_sub_dir = sub_dir + "/time_data"
            for leaf_json in [ac_sub_dir + "/" + f for f in listdir(ac_sub_dir)]:
                f = open(leaf_json)
                d = json.load(f)
                url_set.add(proc_url(d['navigation_timing'][0]['name']))
                if 'resource_timing' in d and isinstance(d["resource_timing"], list):
                    for element in d["resource_timing"]:
                        url_set.add(proc_url(element['name']))

    with open("all_urls.json", "w") as ouf:
        json.dump(list(url_set), fp=ouf)


def process_browser_json():
    result_dir = 'results'
    run_result_dir = [result_dir + "/" + f for f in listdir(result_dir)]
    m_dict = defaultdict(lambda: defaultdict(lambda: list()))

    for dir in run_result_dir:
        run = int(dir[dir.rfind("-") + 1:])
        for sub_dir in [dir + "/" + f for f in listdir(dir)]:
            mode = sub_dir[sub_dir.rfind("/") + 1:]
            ac_sub_dir = sub_dir + "/time_data"
            for leaf_json in [ac_sub_dir + "/" + f for f in listdir(ac_sub_dir)]:
                web_name = leaf_json[leaf_json.rfind("/") + 1:]
                f = open(leaf_json)
                d = json.load(f)

                semi_dict = {}
                semi_dict['secureConnectionStart'] = d['navigation_timing'][0]['secureConnectionStart']
                semi_dict['connectEnd'] = d['navigation_timing'][0]['connectEnd']
                semi_dict['requestStart'] = d['navigation_timing'][0]['requestStart']

                semi_dict['domainLookupEnd'] = d['navigation_timing'][0]['domainLookupEnd']
                semi_dict['domainLookupStart'] = d['navigation_timing'][0]['domainLookupStart']
                semi_dict['dns_time'] = semi_dict['domainLookupEnd'] - semi_dict['domainLookupStart']

                semi_dict['ssl_connection_time_con_end'] = semi_dict['connectEnd'] - semi_dict['secureConnectionStart']
                semi_dict['ssl_connection_time_app_data'] = semi_dict['requestStart'] - semi_dict['secureConnectionStart']

                child_dict = {}
                if 'resource_timing' in d and isinstance(d["resource_timing"], list):
                    for element in d["resource_timing"]:
                        e_dict = {}
                        e_dict['secureConnectionStart'] = element['secureConnectionStart']
                        e_dict['connectEnd'] = element['connectEnd']
                        e_dict['requestStart'] = element['requestStart']

                        e_dict['domainLookupEnd'] = element['domainLookupEnd']
                        e_dict['domainLookupStart'] = element['domainLookupStart']
                        e_dict['dns_time'] = e_dict['domainLookupEnd'] - e_dict['domainLookupStart']

                        e_dict['ssl_connection_time_con_end'] = element['connectEnd'] - element['secureConnectionStart']
                        e_dict['ssl_connection_time_app_data'] = element['requestStart'] - element[
                            'secureConnectionStart']

                        child_dict[element['name']] = e_dict

                semi_dict['resources'] = child_dict

                m_dict[web_name][mode].append((run, semi_dict))

    with open("web_browser.json", "w") as ouf:
        json.dump(m_dict, fp=ouf)


def waterfall_graph_maker():
    f = open('web_browser.json')
    m_dict = json.load(f)

    data_dict_tuple = defaultdict(lambda: defaultdict(lambda: dict()))

    import statistics
    for web in m_dict:
        for mode in m_dict[web]:

            nested_child_dict = defaultdict(lambda: list())
            nested_child_f = dict()

            med_secure_connection_start = statistics.median([e[1]['secureConnectionStart'] for e in m_dict[web][mode]])
            med_secure_connection_end = statistics.median([e[1]['connectEnd'] for e in m_dict[web][mode]])

            data_dict_tuple[web][mode]['base'] = (med_secure_connection_start, med_secure_connection_end)

            for e in m_dict[web][mode]:
                for r in e[1]['resources']:
                    d_ = e[1]['resources'][r]
                    if d_['secureConnectionStart'] == 0:
                        continue
                    nested_child_dict[d_['name']].append((d_['secureConnectionStart'], d_['connectEnd']))

            for resource in nested_child_dict:
                nested_child_f[resource] = (statistics.median([e[0] for e in nested_child_dict[resource]]),
                                            statistics.median([e[1] for e in nested_child_dict[resource]]))
                data_dict_tuple[web][mode][resource] = nested_child_f[resource]

    with open("waterfall.json", "w") as ouf:
        json.dump(data_dict_tuple, fp=ouf)


def waterfall_graph_maker_single_run():
    f = open('web_browser.json')
    m_dict = json.load(f)

    data_dict_tuple = defaultdict(lambda: defaultdict(lambda: dict()))

    import statistics
    for web in m_dict:

        for mode in m_dict[web]:

            nested_child_dict = defaultdict(lambda: list())
            nested_child_f = dict()

            med_secure_connection_start = m_dict[web][mode][0][1]['secureConnectionStart']
            med_secure_connection_end = m_dict[web][mode][0][1]['connectEnd']

            data_dict_tuple[web][mode]['base'] = (med_secure_connection_start, med_secure_connection_end)

            e = m_dict[web][mode][0]
            for r in e[1]['resources']:
                d_ = e[1]['resources'][r]
                if d_['secureConnectionStart'] == 0:
                    continue
                nested_child_dict[d_['name']].append((d_['secureConnectionStart'], d_['connectEnd']))

            for resource in nested_child_dict:
                nested_child_f[resource] = (statistics.median([e[0] for e in nested_child_dict[resource]]),
                                            statistics.median([e[1] for e in nested_child_dict[resource]]))
                data_dict_tuple[web][mode][resource] = nested_child_f[resource]

    with open("waterfall_2.json", "w") as ouf:
        json.dump(data_dict_tuple, fp=ouf)


def dns_ssl_comp():
    f = open('web_browser.json')
    m_dict = json.load(f)

    data_dict_tuple = defaultdict(lambda: dict())
    data_dict_tuple_v2 = defaultdict(lambda: dict())

    import statistics
    for web in m_dict:
        for mode in m_dict[web]:

            dns_med = statistics.median([e[1]['dns_time'] for e in m_dict[web][mode]])
            ssl_med = statistics.median([e[1]['ssl_connection_time_con_end'] for e in m_dict[web][mode]])

            data_dict_tuple[web][mode] = (dns_med, ssl_med)
            data_dict_tuple_v2[web][mode] = (m_dict[web][mode][0][1]['dns_time'], m_dict[web][mode][0][1]['ssl_connection_time_con_end'])


    with open("dns_ssl.json", "w") as ouf:
        json.dump(data_dict_tuple, fp=ouf)
    with open("dns_ssl_2.json", "w") as ouf:
        json.dump(data_dict_tuple_v2, fp=ouf)


find_urls()



#process_browser_json()