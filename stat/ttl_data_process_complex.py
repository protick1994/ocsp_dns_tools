import json
from collections import defaultdict
from local import LOCAL
from pathlib import Path


lum_resolvers_asn = [15169, 20473, 36692, 14061, 30607, 24940, 27725]

class Inline(object):
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


meta_data_dict = Inline()

server_dns_http_time_list = []

# TOTAL exit nodes
meta_data_dict.exitnode_set = set()
meta_data_dict.ttl_to_exitnode_set = defaultdict(lambda: set())


meta_data_dict.resolver_set = set()
meta_data_dict.ttl_to_resolver_set = defaultdict(lambda: set())


meta_data_dict.candidate_resolver_set = set()
meta_data_dict.ttl_to_candidate_resolver_set = defaultdict(lambda: set())

meta_data_dict.candidate_resolver_set_5 = set()
meta_data_dict.ttl_to_candidate_resolver_set_5 = defaultdict(lambda: set())

meta_data_dict.candidate_resolver_set_10 = set()
meta_data_dict.ttl_to_candidate_resolver_set_10 = defaultdict(lambda: set())


meta_data_dict.asn_set = set()
meta_data_dict.ttl_to_asn_set = defaultdict(lambda: set())

meta_data_dict.local_resolver_set = set()
meta_data_dict.ttl_to_local_resolver_set = defaultdict(lambda: set())

meta_data_dict.local_resolver_set_5 = set()
meta_data_dict.ttl_to_local_resolver_set_5 = defaultdict(lambda: set())

meta_data_dict.public_resolver_set = set()
meta_data_dict.ttl_to_public_resolver_set = defaultdict(lambda: set())

meta_data_dict.public_resolver_set_5 = set()
meta_data_dict.ttl_to_public_resolver_set_5 = defaultdict(lambda: set())

meta_data_dict.public_resolver_set_10 = set()
meta_data_dict.ttl_to_public_resolver_set_10 = defaultdict(lambda: set())

meta_data_dict.local_resolver_set_10 = set()
meta_data_dict.ttl_to_local_resolver_set_10 = defaultdict(lambda: set())

meta_data_dict.global_public_local_dict = {}
meta_data_dict.excluded_lum_resolver_set = set()


def fix_sets():
    meta_data_dict.exitnode_set = list(meta_data_dict.exitnode_set)
    meta_data_dict.resolver_set = list(meta_data_dict.resolver_set)
    meta_data_dict.asn_set = list(meta_data_dict.asn_set)
    meta_data_dict.local_resolver_set = list(meta_data_dict.local_resolver_set)
    meta_data_dict.local_resolver_set_5 = list(meta_data_dict.local_resolver_set_5)
    meta_data_dict.public_resolver_set_5 = list(meta_data_dict.public_resolver_set_5)
    meta_data_dict.local_resolver_set_10 = list(meta_data_dict.local_resolver_set_10)
    meta_data_dict.public_resolver_set_10 = list(meta_data_dict.public_resolver_set_10)
    meta_data_dict.public_resolver_set = list(meta_data_dict.public_resolver_set)

    meta_data_dict.excluded_lum_resolver_set = list(meta_data_dict.excluded_lum_resolver_set)
    meta_data_dict.candidate_resolver_set = list(meta_data_dict.candidate_resolver_set)
    meta_data_dict.candidate_resolver_set_5 = list(meta_data_dict.candidate_resolver_set_5)
    meta_data_dict.candidate_resolver_set_10 = list(meta_data_dict.candidate_resolver_set_10)

    for ttl in meta_data_dict.ttl_to_candidate_resolver_set:
        meta_data_dict.ttl_to_candidate_resolver_set[ttl] = list(meta_data_dict.ttl_to_candidate_resolver_set)
    for ttl in meta_data_dict.ttl_to_candidate_resolver_set_5:
        meta_data_dict.ttl_to_candidate_resolver_set_5[ttl] = list(meta_data_dict.ttl_to_candidate_resolver_set_5)
    for ttl in meta_data_dict.ttl_to_candidate_resolver_set_10:
        meta_data_dict.ttl_to_candidate_resolver_set_10[ttl] = list(meta_data_dict.ttl_to_candidate_resolver_set_10)
    for ttl in meta_data_dict.ttl_to_exitnode_set:
        meta_data_dict.ttl_to_exitnode_set[ttl] = list(meta_data_dict.ttl_to_exitnode_set[ttl])
    for ttl in meta_data_dict.ttl_to_resolver_set:
        meta_data_dict.ttl_to_resolver_set[ttl] = list(meta_data_dict.ttl_to_resolver_set[ttl])
    for ttl in meta_data_dict.ttl_to_asn_set:
        meta_data_dict.ttl_to_asn_set[ttl] = list(meta_data_dict.ttl_to_asn_set[ttl])
    for ttl in meta_data_dict.ttl_to_local_resolver_set:
        meta_data_dict.ttl_to_local_resolver_set[ttl] = list(meta_data_dict.ttl_to_local_resolver_set[ttl])
    for ttl in meta_data_dict.ttl_to_local_resolver_set_5:
        meta_data_dict.ttl_to_local_resolver_set_5[ttl] = list(meta_data_dict.ttl_to_local_resolver_set_5[ttl])
    for ttl in meta_data_dict.ttl_to_public_resolver_set:
        meta_data_dict.ttl_to_public_resolver_set[ttl] = list(meta_data_dict.ttl_to_public_resolver_set[ttl])
    for ttl in meta_data_dict.ttl_to_public_resolver_set_5:
        meta_data_dict.ttl_to_public_resolver_set_5[ttl] = list(meta_data_dict.ttl_to_public_resolver_set_5[ttl])


    for ttl in meta_data_dict.ttl_to_public_resolver_set_10:
        meta_data_dict.ttl_to_public_resolver_set_10[ttl] = list(meta_data_dict.ttl_to_public_resolver_set_10[ttl])
    for ttl in meta_data_dict.ttl_to_local_resolver_set_10:
        meta_data_dict.ttl_to_local_resolver_set_10[ttl] = list(meta_data_dict.ttl_to_local_resolver_set_10[ttl])


if LOCAL:
    source_dir = "../ttl_result/results_proactive_complex_v2/"
else:
    source_dir = "/home/protick/ocsp_dns_django/results_proactive_complex_v8/"

allowed_thresholds = [43, 49, 55, 58]

master_dict = {}
resultant_dump = {}


def dict_union(dict_list):
    d = {}
    for dict_ in dict_list:
        for key in dict_:
            d[key] = dict_[key]
    return d

def master_dict_sanity():
    for key in master_dict:
        print("TTL {}".format(key))
        for k in master_dict[key]:
            print("CLD {}".format(k))

def get_resolver_to_asn_dict():
    #master_dict_sanity()
    dict_list = []
    for ttl in allowed_thresholds:
        nested_dict = master_dict[ttl]["final_resolver_to_asn"]["resolver_to_asn_own"]
        # meta_data_dict.ttl_to_resolver_set[ttl] = set(list(nested_dict.keys()))
        dict_list.append(nested_dict)

    resolver_to_asn_global = dict_union(dict_list)
    # meta_data_dict.resolver_set = set(list(resolver_to_asn_global.keys()))
    master_dict["resolver_to_asn_global"] = resolver_to_asn_global


def get_all_data():

    for ttl in allowed_thresholds:
        master_dict[ttl] = {}

    file_list = ["final_resolver_to_asn", "resolver_to_org_country", "resolver_public_local_dict", "final_data"]

    for ttl in allowed_thresholds:
        for f in file_list:
            try:
                fl = open("{}{}/{}.json".format(source_dir, ttl, f))
                d = json.load(fl)
                master_dict[ttl][f] = d
            except:
                print("error loading {}".format("{}{}/{}.json".format(source_dir, ttl, f)))
                master_dict[ttl][f] = {}

    # return master_dict


def get_all_data_only_org():

    for ttl in allowed_ttls:
        master_dict[ttl] = {}

    file_list = ["resolver_to_org_country"]

    for ttl in allowed_ttls:
        for f in file_list:
            fl = open("{}{}/{}.json".format(source_dir, ttl, f))
            d = json.load(fl)
            master_dict[ttl][f] = d

    # return master_dict


def get_unified_field(file_name):
    dict_list = []
    for ttl in allowed_thresholds:
        dict_list.append(master_dict[ttl][file_name])

    unified_dict = dict_union(dict_list)
    return unified_dict


def resolver_distribution():
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in global_resolver_to_org_country:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1

    resultant_dump["geographic_resolver_distribution"] = country_code_to_count_map




def cdf_data():
    resolver_to_asn_global = master_dict['resolver_to_asn_global']
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = {}

    # meta_data_dict = Inline()

    pub_loc_dict_list = []

    for ttl in allowed_thresholds:
        ratio_cdf_dict[ttl] = {}
        ratio_cdf_dict[ttl]["local"] = {}
        ratio_cdf_dict[ttl]["public"] = {}
        ratio_cdf_dict[ttl]["all"] = {}
        for k in ratio_cdf_dict[ttl]:
            for t_h in [3, 5, 10]:
                ratio_cdf_dict[ttl][k][t_h] = {}

                ratio_cdf_dict[ttl][k][t_h]["ans_proactive"] = []
                ratio_cdf_dict[ttl][k][t_h]["ans_normal"] = []
                ratio_cdf_dict[ttl][k][t_h]["ans_reduce"] = []

                ratio_cdf_dict[ttl][k][t_h]["pro_res_set"] = []
                ratio_cdf_dict[ttl][k][t_h]["other_res_set"] = []
                ratio_cdf_dict[ttl][k][t_h]["all_set"] = []

        # TODO ***
        is_public = master_dict[ttl]["resolver_public_local_dict"]
        pub_loc_dict_list.append(is_public)

        d = master_dict[ttl]["final_data"]
        final_dict = d["data_elaborate"]
        ex_node_set = set()
        excluded_lum_resolver = []

        for key in final_dict:
            try:
                if int(resolver_to_asn_global[key]) in lum_resolvers_asn:
                    excluded_lum_resolver.append(key)
                    meta_data_dict.excluded_lum_resolver_set.add(key)
                    continue
            except:
                pass

            # append((req_id, ip_hash))
            normal_set = set()
            pro_set = set()
            reduce_set = set()
            for e in final_dict[key]["reduce"]:
                reduce_set.add(e[1])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            for e in final_dict[key]["pro"]:
                pro_set.add(e[1])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            for e in final_dict[key]["normal"]:
                normal_set.add(e[1])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            subtotal = normal_set.union(pro_set).union(reduce_set)
            total = len(subtotal)
            if total == 0:
                continue

            pro_ratio = len(pro_set) / total
            reduce_ratio = len(reduce_set) / total
            normal_set_ratio = len(normal_set) / total

            threshold_to_check = [5, 10]

            is_pub_resol = -1
            if key in is_public:
                if is_public[key]:
                    is_pub_resol = True
                    meta_data_dict.ttl_to_public_resolver_set[ttl].add(key)
                    meta_data_dict.public_resolver_set.add(key)
                else:
                    is_pub_resol = False
                    meta_data_dict.ttl_to_local_resolver_set[ttl].add(key)
                    meta_data_dict.local_resolver_set.add(key)

            for loc_glo_key in ratio_cdf_dict[ttl]:

                if loc_glo_key != "all":
                    if key not in is_public:
                        continue
                    if loc_glo_key == "local":
                        if is_public[key]:
                            continue
                    if loc_glo_key == "public":
                        if not is_public[key]:
                            continue

                for threshold in threshold_to_check:
                    if total >= threshold:
                        if threshold == 5:
                            if is_pub_resol != -1:
                                if is_pub_resol:
                                    meta_data_dict.public_resolver_set_5.add(key)
                                    meta_data_dict.ttl_to_public_resolver_set_5[ttl].add(key)
                                else:
                                    meta_data_dict.local_resolver_set_5.add(key)
                                    meta_data_dict.ttl_to_local_resolver_set_5[ttl].add(key)

                        # ratio_cdf_dict[ttl][k][t_h]["ans_proactive"] = []
                        # ratio_cdf_dict[ttl][k][t_h]["ans_normal"] = []
                        # ratio_cdf_dict[ttl][k][t_h]["ans_reduce"] = []

                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["ans_proactive"].append(pro_ratio)
                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["ans_normal"].append(normal_set_ratio)
                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["ans_reduce"].append(reduce_ratio)

                        # if loc_glo_key == "public":
                        #     if 0 < ratio < 1:
                        #         middle_resolver_set.append((key, ratio))

                        # ratio_cdf_dict[ttl][k][t_h]["pro_res_set"] = []
                        # ratio_cdf_dict[ttl][k][t_h]["other_res_set"] = []
                        # ratio_cdf_dict[ttl][k][t_h]["all_set"] = []

                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["all_set"].append(key)
                        if pro_ratio >= 1:
                            ratio_cdf_dict[ttl][loc_glo_key][threshold]["pro_res_set"].append(key)
                        elif pro_ratio <= 0:
                            ratio_cdf_dict[ttl][loc_glo_key][threshold]["other_res_set"].append(key)


    # for resolver, ratio in middle_resolver_set:
    #     try:
    #         org = global_resolver_to_org_country[resolver][0]
    #         middle_resolver_with_org_list.append((resolver, org, ratio))
    #     except:
    #         pass

    meta_data_dict.global_public_local_dict = dict_union(pub_loc_dict_list)

    master_dict["ratio_cdf_dict"] = ratio_cdf_dict
    resultant_dump["ratio_cdf_dict"] = ratio_cdf_dict
    #resultant_dump["middle_resolver_with_org_list"] = middle_resolver_with_org_list


def cdf_data_v2():
    resolver_to_asn_global = master_dict['resolver_to_asn_global']
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = {}

    # meta_data_dict = Inline()

    pub_loc_dict_list = []

    for ttl in allowed_thresholds:
        ratio_cdf_dict[ttl] = {}
        ratio_cdf_dict[ttl]["local"] = {}
        ratio_cdf_dict[ttl]["public"] = {}
        ratio_cdf_dict[ttl]["all"] = {}
        for k in ratio_cdf_dict[ttl]:
            for t_h in [3, 5, 10]:
                ratio_cdf_dict[ttl][k][t_h] = {}
                ratio_cdf_dict[ttl][k][t_h]["ans_proactive"] = []
                ratio_cdf_dict[ttl][k][t_h]["ans_normal"] = []
                ratio_cdf_dict[ttl][k][t_h]["ans_reduce"] = []

                ratio_cdf_dict[ttl][k][t_h]["pro_res_set"] = []
                ratio_cdf_dict[ttl][k][t_h]["other_res_set"] = []
                ratio_cdf_dict[ttl][k][t_h]["all_set"] = []

        # TODO ***
        is_public = master_dict[ttl]["resolver_public_local_dict"]
        pub_loc_dict_list.append(is_public)

        d = master_dict[ttl]["final_data"]
        final_dict = d["data_elaborate"]
        ex_node_set = set()
        excluded_lum_resolver = []

        for key in final_dict:
            try:
                if int(resolver_to_asn_global[key]) in lum_resolvers_asn:
                    excluded_lum_resolver.append(key)
                    meta_data_dict.excluded_lum_resolver_set.add(key)
                    continue
            except:
                pass

            meta_data_dict.resolver_set.add(key)
            meta_data_dict.ttl_to_resolver_set[ttl].add(key)

            # append((req_id, ip_hash))
            normal_set = set()
            pro_set = set()
            reduce_set = set()
            for e in final_dict[key]["reduce"]:
                reduce_set.add(e[1])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            for e in final_dict[key]["pro"]:
                pro_set.add(e[1])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])
                server_dns_http_time_list.append((e[3], e[5], e[7]))
            for e in final_dict[key]["normal"]:
                normal_set.add(e[1])
                ex_node_set.add(e[1])
                meta_data_dict.exitnode_set.add(e[1])
                meta_data_dict.ttl_to_exitnode_set[ttl].add(e[1])

            subtotal = pro_set.union(reduce_set)
            total = len(subtotal)
            if total <= 0:
                continue

            meta_data_dict.candidate_resolver_set.add(key)
            meta_data_dict.ttl_to_candidate_resolver_set[ttl].add(key)

            pro_ratio = len(pro_set) / total
            reduce_ratio = len(reduce_set) / total

            threshold_to_check = [10]

            is_pub_resol = -1
            if key in is_public:
                if is_public[key]:
                    is_pub_resol = True
                    meta_data_dict.ttl_to_public_resolver_set[ttl].add(key)
                    meta_data_dict.public_resolver_set.add(key)
                else:
                    is_pub_resol = False
                    meta_data_dict.ttl_to_local_resolver_set[ttl].add(key)
                    meta_data_dict.local_resolver_set.add(key)

            for loc_glo_key in ratio_cdf_dict[ttl]:

                if loc_glo_key != "all":
                    if key not in is_public:
                        continue
                    if loc_glo_key == "local":
                        if is_public[key]:
                            continue
                    if loc_glo_key == "public":
                        if not is_public[key]:
                            continue

                for threshold in threshold_to_check:
                    if total >= threshold:
                        if threshold == 5:

                            meta_data_dict.candidate_resolver_set_5.add(key)
                            meta_data_dict.ttl_to_candidate_resolver_set_5[ttl].add(key)


                            if is_pub_resol != -1:
                                if is_pub_resol:
                                    meta_data_dict.public_resolver_set_5.add(key)
                                    meta_data_dict.ttl_to_public_resolver_set_5[ttl].add(key)
                                else:
                                    meta_data_dict.local_resolver_set_5.add(key)
                                    meta_data_dict.ttl_to_local_resolver_set_5[ttl].add(key)

                        if threshold == 10:

                            meta_data_dict.candidate_resolver_set_10.add(key)
                            meta_data_dict.ttl_to_candidate_resolver_set_10[ttl].add(key)

                            if is_pub_resol != -1:
                                if is_pub_resol:
                                    meta_data_dict.public_resolver_set_10.add(key)
                                    meta_data_dict.ttl_to_public_resolver_set_10[ttl].add(key)
                                else:
                                    meta_data_dict.local_resolver_set_10.add(key)
                                    meta_data_dict.ttl_to_local_resolver_set_10[ttl].add(key)

                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["ans_proactive"].append(pro_ratio)
                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["ans_reduce"].append(reduce_ratio)

                        ratio_cdf_dict[ttl][loc_glo_key][threshold]["all_set"].append(key)
                        if pro_ratio >= 1:
                            ratio_cdf_dict[ttl][loc_glo_key][threshold]["pro_res_set"].append(key)
                        elif pro_ratio <= 0:
                            ratio_cdf_dict[ttl][loc_glo_key][threshold]["other_res_set"].append(key)

    meta_data_dict.global_public_local_dict = dict_union(pub_loc_dict_list)

    master_dict["ratio_cdf_dict_2"] = ratio_cdf_dict
    resultant_dump["ratio_cdf_dict_2"] = ratio_cdf_dict


def geographic_correct_incorrect_distribution():
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = master_dict["ratio_cdf_dict"]

    geo_distro = {}

    for ttl in allowed_ttls:
        geo_distro[ttl] = {}

        country_code_to_count_map = defaultdict(lambda: 0)
        for resolver in ratio_cdf_dict[ttl]["all"][5]["inc_res_set"]:
            country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
        geo_distro[ttl]["incorrect"] = country_code_to_count_map

        country_code_to_count_map = defaultdict(lambda: 0)
        for resolver in ratio_cdf_dict[ttl]["all"][5]["cor_res_set"]:
            country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
        geo_distro[ttl]["correct"] = country_code_to_count_map

    resultant_dump["geographic_corr_incorr_distro"] = geo_distro


def geographic_correct_incorrect_distribution_all_over():
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")

    ratio_cdf_dict = master_dict["ratio_cdf_dict"]

    inc_set = set()
    cor_set = set()
    all_set = set()

    for ttl in allowed_ttls:
        for resolver in ratio_cdf_dict[ttl]["all"][5]["inc_res_set"]:
            inc_set.add(resolver)

        for resolver in ratio_cdf_dict[ttl]["all"][5]["cor_res_set"]:
            cor_set.add(resolver)

        for resolver in ratio_cdf_dict[ttl]["all"][5]["all_set"]:
            all_set.add(resolver)

    geo_distro = {}

    country_code_to_count_map = defaultdict(lambda: 0)

    for resolver in inc_set:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    geo_distro["incorrect"] = country_code_to_count_map

    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in cor_set:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    geo_distro["correct"] = country_code_to_count_map

    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in all_set:
        country_code_to_count_map[global_resolver_to_org_country[resolver][1]] += 1
    geo_distro["all"] = country_code_to_count_map

    resultant_dump["geographic_corr_incorr_distro_global"] = geo_distro


def venn_diagram_info():

    ratio_cdf_dict = master_dict["ratio_cdf_dict"]

    mother_dict = {}

    for ttl in allowed_ttls:
        mother_dict[ttl] = {}

        inc_set = set()
        for resolver in ratio_cdf_dict[ttl]["all"][5]["inc_res_set"]:
            inc_set.add(resolver)
        mother_dict[ttl]["ttl_dis_honor"] = list(inc_set)

        cor_set = set()
        for resolver in ratio_cdf_dict[ttl]["all"][5]["cor_res_set"]:
            cor_set.add(resolver)
        mother_dict[ttl]["ttl_honor"] = list(cor_set)

    resultant_dump["venn_info"] = mother_dict


def is_normal_http_response(str):
    if "phase1" in str or "phase2" in str:
        return True
    return False


def http_response_class():
    wrong_asn_set = set()
    correct_asn_set = set()

    for ttl in allowed_ttls:
        f = open("{}{}/http_response_to_asn_list.json".format(source_dir, ttl))
        d = json.load(f)
        for key in d:
            if not is_normal_http_response(key):
                for asn in d[key]:
                    wrong_asn_set.add(asn)
            if is_normal_http_response(key):
                for asn in d[key]:
                    correct_asn_set.add(asn)

    resultant_dump["http_response_info"] = {
        "i_c_s": list(wrong_asn_set),
        "c_s": list(correct_asn_set)
    }

def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    return json.JSONEncoder.default(obj)


def proc_meta_info():
    meta_info = []
    meta_info.append("Total exit-nodes: {}".format(len(meta_data_dict.exitnode_set)))
    for ttl in meta_data_dict.ttl_to_exitnode_set:
        meta_info.append("Total exit-nodes for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_exitnode_set[ttl])))

    # meta_info.append("Total exit-nodes: {}".format(len(meta_data_dict.exitnode_set)))
    # for ttl in meta_data_dict.ttl_to_exitnode_set:
    #     meta_info.append("Total exit-nodes for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_exitnode_set[ttl])))

    meta_info.append("Total resolvers: {}".format(len(meta_data_dict.resolver_set)))
    for ttl in meta_data_dict.ttl_to_resolver_set:
        meta_info.append("Total resolvers for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_resolver_set[ttl])))

    meta_info.append("Total candidate resolvers: {}".format(len(meta_data_dict.candidate_resolver_set)))
    for ttl in meta_data_dict.ttl_to_candidate_resolver_set:
        meta_info.append("Total candidate resolvers for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_candidate_resolver_set[ttl])))

    meta_info.append("Total candidate resolvers with atleast 5 exitnodes: {}".format(len(meta_data_dict.candidate_resolver_set_5)))
    for ttl in meta_data_dict.ttl_to_candidate_resolver_set_5:
        meta_info.append("Total candidate resolvers 5 exitnodes for ttl {}: {}".format(ttl, len(
            meta_data_dict.ttl_to_candidate_resolver_set_5[ttl])))

    meta_info.append(
        "Total candidate resolvers with atleast 10 exitnodes: {}".format(len(meta_data_dict.candidate_resolver_set_10)))
    for ttl in meta_data_dict.ttl_to_candidate_resolver_set_10:
        meta_info.append("Total candidate resolvers 10 exitnodes for ttl {}: {}".format(ttl, len(
            meta_data_dict.ttl_to_candidate_resolver_set_10[ttl])))


    meta_info.append("Total local resolvers in the candidates: {}".format(len(meta_data_dict.local_resolver_set)))
    for ttl in meta_data_dict.ttl_to_local_resolver_set:
        meta_info.append("Total local resolvers in the candidates for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_local_resolver_set[ttl])))

    meta_info.append("Total public resolvers in the candidates : {}".format(len(meta_data_dict.public_resolver_set)))
    for ttl in meta_data_dict.ttl_to_public_resolver_set:
        meta_info.append(
            "Total public resolvers in the candidates  for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_public_resolver_set[ttl])))

    meta_info.append("Total local resolvers with atleast 5 exitnodes  in the candidates : {}".format(len(meta_data_dict.local_resolver_set_5)))
    for ttl in meta_data_dict.ttl_to_local_resolver_set_5:
        meta_info.append(
            "Total local resolvers with atleast 5 exitnodes in the candidates  for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_local_resolver_set_5[ttl])))

    meta_info.append("Total public resolvers with atleast 5 exitnodes: {}".format(len(meta_data_dict.public_resolver_set_5)))
    for ttl in meta_data_dict.ttl_to_public_resolver_set_5:
        meta_info.append(
            "Total public resolvers with atleast 5 exitnodes for ttl {}: {}".format(ttl, len(meta_data_dict.ttl_to_public_resolver_set_5[ttl])))


    meta_info.append("Total local resolvers with atleast 10 exitnodes  in the candidates : {}".format(
        len(meta_data_dict.local_resolver_set_10)))
    for ttl in meta_data_dict.ttl_to_local_resolver_set_10:
        meta_info.append(
            "Total local resolvers with atleast 10 exitnodes in the candidates  for ttl {}: {}".format(ttl, len(
                meta_data_dict.ttl_to_local_resolver_set_10[ttl])))

    meta_info.append(
        "Total public resolvers with atleast 10 exitnodes: {}".format(len(meta_data_dict.public_resolver_set_10)))
    for ttl in meta_data_dict.ttl_to_public_resolver_set_10:
        meta_info.append(
            "Total public resolvers with atleast 10 exitnodes for ttl {}: {}".format(ttl, len(
                meta_data_dict.ttl_to_public_resolver_set_10[ttl])))

    resultant_dump["meta_info"] = meta_info
    return meta_info


def get_middle_req_org_rank():
    f = open("/home/protick/ocsp_dns_django/preprocessed_middle_req_log/bind/middle_req.json")
    d = json.load(f)
    resolver_to_count = defaultdict(lambda: 0)
    total_resolvers = 0
    total_identifiers = 0
    for ttl in d:
        if not str(ttl) in ["60"]:
            continue
        data = d[ttl]
        total_resolvers = len(list(data.keys()))
        for resolver in list(data.keys()):
            tot_identifiers = len(list(data[resolver].keys()))
            resolver_to_count[resolver] = tot_identifiers
            total_identifiers += tot_identifiers

    org_to_count = defaultdict(lambda: 0)
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")
    for resolver in resolver_to_count:
        try:
            org = global_resolver_to_org_country[resolver][0]
            org_to_count[org] += resolver_to_count[resolver]
        except:
            pass

    d_ = {
        "total_resolvers": total_resolvers,
        "total_identifers": total_identifiers,
        "org_count": org_to_count,
        "resolver_to_count": resolver_to_count
    }

    resultant_dump["middle_req_rank_info"] = d_


def table_maker_preprocess():
    global_resolver_to_asn = master_dict["resolver_to_asn_global"]
    global_resolver_to_org_country = get_unified_field("resolver_to_org_country")
    org_to_cn = {}
    org_Set = set()
    org_to_asn_marker = defaultdict(lambda: dict())
    org_to_asn_count = defaultdict(lambda: 0)
    org_to_resolver_marker = defaultdict(lambda: dict())
    org_to_resolver_count = defaultdict(lambda: 0)
    org_to_incorrect_resolvers = defaultdict(lambda: set())
    org_to_correct_resolvers = defaultdict(lambda: set())
    org_to_exitnode_set = defaultdict(lambda: set())
    org_to_incorrect_exitnode_set = defaultdict(lambda: set())

    for ttl in allowed_ttls:
        d = master_dict[ttl]["final_data"]
        final_dict = d["data_elaborate"]

        for key in final_dict:
            asn = "undefined"
            if key in global_resolver_to_asn:
                asn = global_resolver_to_asn[key]

            org = "undefined"
            cn = "undefined"
            if key in global_resolver_to_org_country:
                org, cn = global_resolver_to_org_country[key]

            if asn not in org_to_asn_marker[org]:
                org_to_asn_marker[org][asn] = 1
                org_to_asn_count[org] += 1

            org_Set.add(org)
            org_to_cn[org] = cn

            if key not in org_to_resolver_marker[org]:
                org_to_resolver_marker[org][key] = 1
                org_to_resolver_count[org] += 1

            correct_set = set()
            incorrect_set = set()

            for e in final_dict[key]["ic"]:
                incorrect_set.add(e[1])
            for e in final_dict[key]["c"]:
                correct_set.add(e[1])

            total_set = correct_set.union(incorrect_set)
            total = len(total_set)

            org_to_exitnode_set[org].update(total_set)
            org_to_incorrect_exitnode_set[org].update(incorrect_set)

            if total >= 5:
                ratio = len(incorrect_set) / total
                if ratio > .95:
                    org_to_incorrect_resolvers[org].add(key)
                if ratio < .05:
                    org_to_correct_resolvers[org].add(key)

    summary = []
    for org in org_Set:
        tot_exitnodes = len(org_to_exitnode_set[org])
        inc_exitnodes = len(org_to_incorrect_exitnode_set[org])
        perc = 0
        if tot_exitnodes > 0:
            perc = inc_exitnodes / tot_exitnodes
        element = Inline()
        element.org = org
        element.cn = org_to_cn[org]
        element.tot_exitnodes = tot_exitnodes
        element.inc_exitnodes = inc_exitnodes
        element.inc_exit_perc = perc
        element.total_asns = org_to_asn_count[org]
        element.total_resolvers = org_to_resolver_count[org]
        element.total_incorrect_resolvers = len(org_to_incorrect_resolvers[org])
        element.total_correct_resolvers = len(org_to_correct_resolvers[org])
        summary.append(element)
    resultant_dump["table_data_global"] = summary


def init():
    get_all_data()

    print("get_all_data")

    get_resolver_to_asn_dict()
    print("get_resolver_to_asn_dict")

    # cdf_data()
    # print("cdf")

    cdf_data_v2()
    print("cdf v2")

    proc_meta_info()
    print("proc_meta_info")

    resultant_dump["server_dns_http_time_list"] = server_dns_http_time_list

    dump_directory = "complex_data_pre_v18/"
    Path(dump_directory).mkdir(parents=True, exist_ok=True)

    for key in resultant_dump:
        with open(dump_directory + "{}.json".format(key), "w") as ouf:
            json.dump(resultant_dump[key], fp=ouf)
    with open(dump_directory + "{}.json".format("all_fk"), "w") as ouf:
        json.dump(resultant_dump, fp=ouf)

init()