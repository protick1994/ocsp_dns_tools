import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import json
from collections import defaultdict
from os import listdir
# from caida_as_org import AS2ISP
from tabulate import tabulate
from local import LOCAL


# as2isp = AS2ISP()
# asn_to_org_dict = {}

ttl = None

min_exitnodes = 5

if LOCAL:
    source_dir = "../ttl_result/"
else:
    source_dir = "/home/protick/ocsp_dns_django/ttl_result_v2/"



def cache_exp_graph_maker():

    # cc = cc + 1
    # if cc == 2:
    # break
    # if 'akamai2' in cache_result_json:
    #     continue
    cdn = None

    # [["access.log-20220326", 52446, 3098], ["access.log-20220327", 100743, 5428], ["access.log-20220328", 141982, 7167],
    #  ["access.log-20220329", 177157, 8421], ["access.log-20220330", 218390, 9846],
    #  ["access.log-20220331", 231987, 10350], ["access.log-20220401", 273353, 11461],
    #  ["access.log-20220402", 305925, 12294], ["access.log-20220403", 347783, 15638],
    #  ["access.log-20220404", 408960, 17430], ["access.log-20220405", 595852, 18218],
    #  ["access.log-20220406", 700192, 18348], ["access.log-20220407", 769551, 18393],
    #  ["access.log-20220408", 864162, 18432], ["access.log-20220409", 896090, 18447],
    #  ["access.log-20220410", 988867, 18480], ["access.log-20220411", 1072776, 18502], ["access.log", 1091842, 18510]]
    d = [["access.log-20220326", 52446, 3098, 133], ["access.log-20220327", 100743, 5428, 108],
         ["access.log-20220328", 141982, 7167, 104], ["access.log-20220329", 177157, 8421, 87],
         ["access.log-20220330", 218390, 9846, 97], ["access.log-20220331", 231987, 10350, 35],
         ["access.log-20220401", 273353, 11461, 100], ["access.log-20220402", 305925, 12294, 92],
         ["access.log-20220403", 347783, 15638, 81], ["access.log-20220404", 408960, 17430, 62],
         ["access.log-20220405", 595852, 18218, 418], ["access.log-20220406", 700192, 18348, 298],
         ["access.log-20220407", 769551, 18393, 193], ["access.log-20220408", 864162, 18432, 303],
         ["access.log-20220409", 896090, 18447, 92], ["access.log-20220410", 988867, 18480, 317],
         ["access.log-20220411", 1072776, 18502, 314]]
    # fig = plt.figure(figsize=(15, 5))
    fig, axs = plt.subplots(1, 1)
    # file - ip - asn -size
    index = 0
    x = []
    ips = []
    asns = []
    ticks = []
    index = 0
    for e in d:
        index += 1
        x.append(index)
        ips.append(e[1])
        asns.append(e[2])
        ticks.append("{} - {} MB".format(e[0], e[3]))

    axs.plot(x, ips, marker='.')
    axs.set_title("Exit-node coverage")

    plt.xticks(x, ticks, rotation=80)

    plt.savefig('ttl_result/{}.png'.format("exit_node_coverage"), bbox_inches="tight")
    plt.clf()


# cache_exp_graph_maker()

def shared_cache_graph():
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["font.size"] = "16"
    plt.rcParams["axes.labelweight"] = "bold"

    f = open("../public_resolver_shared_cache_exp.json")
    dd = json.load(f)
    for element in ["google", "cloudflare"]:
        data = dd['final_data'][element]
        data.sort(key= lambda x: x[1])
        init_time = data[0][1]
        x_first = []
        y_first = []
        x_second_cached = []
        y_second_cached = []
        x_second_new = []
        y_second_new = []

        for d in data:
            if d[0] == "1":
                x_first.append(d[1] - init_time)
                y_first.append(d[3])
            else:
                if d[2] == "52.44.221.99":
                    x_second_cached.append(d[1] - init_time)
                    y_second_cached.append(d[3])
                else:
                    x_second_new.append(d[1] - init_time)
                    y_second_new.append(d[3])

        fig, axs = plt.subplots(1, 1, figsize=(20, 8))
        axs.scatter(x_first, y_first, s=3, c='b')
        axs.scatter(x_second_cached, y_second_cached, s=3, c='g')
        axs.scatter(x_second_new, y_second_new, s=3, c='r')

        plt.ylabel("TTL in seconds")
        plt.xlabel("Time stamp of query (seconds)")
        plt.title(element)
        axs.legend(["DNS-over-UDP", "DNS-over-HTTP: ip1", "DNS-over-HTTP : ip2"], markerscale=6)


        #plt.show()
        plt.savefig('ttl_result/{}_shared_cache.png'.format(element), bbox_inches="tight")
        plt.clf()
        a = 1

# shared_cache_graph()

def lum_asn(e):
    ban_list = ["Cisco", "DigitalOcean", "Constant", "Google"]
    for str in ban_list:
        if str in e:
            return True
    return False


def get_org(asn) :
    if asn in asn_to_org_dict:
        return asn_to_org_dict[asn]
    org_name, org_country = as2isp.getISP("20221212", str(asn))
    asn_to_org_dict[asn] = org_name
    return org_name

def table_maker():

    f = open("../table_data.json")
    d = json.load(f)
    ans_lst = d['ic_ans_lst']
    c_ans_lst = d['c_ans_lst']
    ans_lst = [e for e in ans_lst if not lum_asn(e[2])]
    c_ans_lst = [e for e in c_ans_lst if not lum_asn(e[2])]
    # resolver count, exit node count, isp, cntry, opposite
    ans_lst.sort(key=lambda x: (x[0]/ (x[0] + x[4])), reverse=True)
    c_ans_lst.sort(key=lambda x: (x[0]/ (x[0] + x[4])), reverse=True)

    global_pool = []
    for e in ans_lst:
        global_pool.append([e[0] + e[4], e[1], e[2], e[3], e[4]/(e[0] + e[4])])
    for e in c_ans_lst:
        global_pool.append([e[0] + e[4], e[1], e[2], e[3], e[0]/(e[0] + e[4])])

    global_pool.sort(key=lambda x: x[4], reverse=True)


    table_1 = [['Country', 'Org/ISP', 'Total Resolvers', 'Exit nodes inhibiting incorrect behaviour', 'Percentage of Incorrect Resolvers']]
    cnt = 0
    for i in range(50000000):
        if ans_lst[i][1] < 50 :
            continue

        a = [ans_lst[i][3], ans_lst[i][2], ans_lst[i][0] + ans_lst[i][4], ans_lst[i][1], "{} %".format((ans_lst[i][0] / (ans_lst[i][0] + ans_lst[i][4])) * 100)]
        table_1.append(a)
        cnt += 1
        if cnt == 10:
            break
    print(tabulate(table_1, headers='firstrow', tablefmt='fancy_grid'))

    cnt = 0
    table_2 = []
    for i in range(50000000):
        if c_ans_lst[i][1] < 50:
            continue
        a = [c_ans_lst[i][3], c_ans_lst[i][2], c_ans_lst[i][0] + c_ans_lst[i][4], c_ans_lst[i][1], "{} %".format((c_ans_lst[i][0] / (c_ans_lst[i][0] + c_ans_lst[i][4])) * 100)]
        table_2.append(a)
        cnt += 1
        if cnt == 11:
            break
    table_2 = table_2[1:]
    table_2.insert(0, ['Country', 'Org/ISP', 'Total Resolvers', 'Exit nodes inhibiting Correct behaviour', 'Percentage of Correct Resolvers'])
    print(tabulate(table_2, headers='firstrow', tablefmt='fancy_grid'))


def create_cdf_arr_from_counter(counter,  index_map=False):
    ans = []
    for key in counter:
        ans.append((key, counter[key]))
    ans.sort(key=lambda x: -x[1])
    # 15169, 20473, 36692, 14061, 30607
    cdf_arr = []
    index = 1

    index_to_element = {}

    for e in ans:
        ip, count = e
        index_to_element[index] = ip
        for i in range(count):
            cdf_arr.append(index)
        index += 1
    if index_map:
        return cdf_arr, index_to_element
    return cdf_arr


def cdf_maker(arr, xlabel, ylabel, title, txt_inside, x_line_threshold = None, is_col=False):
    #fig, axs = plt.subplots(figsize=(20, 15))
    # No of data points used
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"
    
    fig, ax = plt.subplots(figsize=(20, 8))
    N = len(arr)

    # normal distribution
    if not is_col:
        data = np.array(arr)
    else:
        arr.sort()
        a = []
        c = []
        for e in arr:
            a.append(e[0])
            c.append(e[1])
        data = np.array(a)

    # sort the data in ascending order
    x = np.sort(data)

    # get the cdf values of y
    y = np.arange(N) / float(N)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.title(title)

    alpha = 0.3

    pre = 0
    ref = []
    for i in range(1, len(x)):
        if x[i] == x[i - 1]:
            continue
        ref.append((x[i - 1], y[i - 1], y[i - 1] - pre, (y[i - 1] - pre) * len(x)))
        pre = y[i - 1]

    # TODO !!!
    a = 1


    if x_line_threshold:
        vertical_cut = None
        for i in range(len(y)):
            if y[i] >= x_line_threshold:
                vertical_cut = x[i]
                x_cut = x[i]
                x_pos_ratio = x_cut / max(x)
                # plt.axvline(x=vertical_cut, c='r')
                plt.text(x_pos_ratio + .1, .5, "at y = {} % : {} {}s".format(x_line_threshold * 100, x[i], xlabel), fontsize=22)
                break


    if is_col:
        x_g, y_g, x_r, y_r = [], [], [], []

        index = 0
        for e in c:
            if e == 'green':
                x_g.append(x[index])
                y_g.append(y[index])
            else:
                x_r.append(x[index])
                y_r.append(y[index])
            index += 1
        plt.scatter(x_g, y_g, marker='o', color="green", lw=6, alpha=.3)
        plt.scatter(x_r, y_r, marker='o', color="red", lw=1, alpha=.4)
    else:
        plt.plot(x, y, marker='.', lw=.3)


    if txt_inside:
        plt.text(.4, .2, txt_inside, fontsize=22)

    #plt.show()
    plt.savefig('ttl_result/{}.png'.format(title), bbox_inches="tight")
    plt.clf()


def jaccard_cdf():
    f = open("../jaccard_index.json")
    d = json.load(f)
    ans = []
    for e in d:
        ans.append(e)

    cdf_maker(ans, "Jaccard index of resolvers observed in phase 1 and 2", "CDF", "CDF: Jaccard index")


def histogram_maker(counter, x_title, y_title, title):
    import matplotlib.pyplot as plt
    ans = []
    fig = plt.figure(figsize=(10, 10))
    for key in counter:
        ans.append((key, counter[key]))
    ans.sort()

    x_arr = [str(e[0]) for e in ans]
    y_arr = [e[1] for e in ans]

    plt.bar(x_arr, y_arr)
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    plt.savefig('ttl_result/histogram-{}.png'.format(title), bbox_inches="tight")
    plt.clf()


def reolver_hits_per_req():
    f = open("../req_id_to_bind_ips_phase_2.json")
    d = json.load(f)
    ans = []


    counter = defaultdict(lambda: 0)

    for e in d:

        ans.append(len(d[e]))
        if len(d[e]) > 20:
            continue
        counter[len(d[e])] += 1

    # org_name, org_country = as2isp.getISP("20221212", str(asn))
    #cdf_maker(ans, "Number of distinct ips seen in bind logs per HTTP request", "CDF", "Phase 2: Bind Server Hits/ per http request", txt_inside=None)
    # cdf_maker(ans_without_1, "Number of distinct ips seen in bind logs per HTTP request", "CDF",
    #           "CDF: Bind Server Hits/ per http request")
    histogram_maker(counter, "Number of distinct ips seen in bind logs per HTTP request", "Count", "Phase 2 : Bind Server Hits/ per http request")


# reolver_hits_per_req()

def dict_union(dict_list):
    d = {}
    for dict_ in dict_list:
        for key in dict_:
            d[key] = dict_[key]
    return d


def get_resolver_to_asn_dict():
    f = open("../ttl_result/5/resolver_to_asn_own.json")
    f_r_5 = json.load(f)
    resolver_to_asn_5 = f_r_5

    f = open("../ttl_result/15/resolver_to_asn_own.json")
    f_r_15 = json.load(f)
    resolver_to_asn_15 = f_r_15

    resolver_to_asn = dict_union([resolver_to_asn_5, resolver_to_asn_15])

    return resolver_to_asn



def first_hit_cases():
    # f = open("../ttl_result/5/all_resolvers_pool.json")
    # a_r_5 = json.load(f)

    f = open(source_dir + "/5/resolver_to_asn_own.json")
    f_r_5 = json.load(f)
    resolver_to_asn_5 = f_r_5

    f = open(source_dir + "15/resolver_to_asn_own.json")
    f_r_15 = json.load(f)
    resolver_to_asn_15 = f_r_15

    resolver_to_asn = dict_union([resolver_to_asn_5, resolver_to_asn_15])


    f = open(source_dir + "5/first_hit_resolvers.json")
    d_5 = json.load(f)

    f = open(source_dir + "15/first_hit_resolvers.json")
    d_15 = json.load(f)

    d = d_5 + d_15




    counter = defaultdict(lambda: 0)
    asn_counter = defaultdict(lambda: 0)
    isp_counter = defaultdict(lambda: 0)
    all_over_counter = defaultdict(lambda: 0)

    # for tuple in a_r:
    #     all_over_counter[tuple[0]] = tuple[1]

    for e in d:
        counter[e] += 1
        asn_counter[resolver_to_asn[e]] += 1
        #isp_counter[get_org(resolver_to_asn[e])] += 1

    # cdf_arr_all_ip = create_cdf_arr_from_counter(counter=all_over_counter)
    #cdf_arr_ip = create_cdf_arr_from_counter(counter=counter)
    cdf_arr_asn, index_dict = create_cdf_arr_from_counter(counter=asn_counter, index_map=True)
    for i in range(1, 6):
        print(i, index_dict[i])

    things_to_dump = {}
    things_to_dump["cdf_arr_asn"] = cdf_arr_asn
    things_to_dump["index_dict"] = index_dict
    things_to_dump["asn_counter"] = asn_counter

    with open("first_try_info.json", "w") as ouf:
        json.dump(things_to_dump, fp=ouf)

    # cdf_arr_isp = create_cdf_arr_from_counter(counter=isp_counter)

    # cdf_maker(cdf_arr_all_ip, "all ip", "CDF", "First Hit ip CDF", None)

    # only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "First Hit ip CDF", None, x_line_threshold=.99)
    #
    # # asn of only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "CDF of first Bind IPs observed for each request", None)


    #cdf_maker(cdf_arr_asn, "asn", "CDF", "CDF of ASNs of first Bind IPs observed for each request", "99.96% of requests coming from 5 ASNs")


    #cdf_maker(cdf_arr_all_ip, "ip", "CDF", "CDF of all Bind IPs observed", None)
    #
    # cdf_maker(cdf_arr_isp, "isp", "CDF", "First Hit isp CDF", None, x_line_threshold=.99)


def first_hit_cases_v2():


    f = open("../ttl_result/first_try_info.json")
    data_v2 = json.load(f)



    # cdf_arr_isp = create_cdf_arr_from_counter(counter=isp_counter)

    # cdf_maker(cdf_arr_all_ip, "all ip", "CDF", "First Hit ip CDF", None)

    # only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "First Hit ip CDF", None, x_line_threshold=.99)
    #
    # # asn of only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "CDF of first Bind IPs observed for each request", None)

    asn_counter = data_v2["asn_counter"]

    ans = []
    tot = 0
    for key in asn_counter:
        tot += asn_counter[key]
        ans.append((key, asn_counter[key]))
    ans.sort(key=lambda x: -x[1])

    p = []
    for e in ans:
        p.append((e[0], e[1], (e[1]/tot) * 100))

    a = 1

    cdf_maker(data_v2["cdf_arr_asn"], "asn", "CDF", "CDF of ASNs of first Bind IPs observed for each request", None)


    #cdf_maker(cdf_arr_all_ip, "ip", "CDF", "CDF of all Bind IPs observed", None)
    #
    # cdf_maker(cdf_arr_isp, "isp", "CDF", "First Hit isp CDF", None, x_line_threshold=.99)


# first_hit_cases_v2()

lum_resolvers_asn = [15169, 20473, 36692, 14061, 30607, 24940, 27725]

# first_hit_cases()

# ASN
# 1 15169
# 2 20473
# 3 36692
# 4 14061
# 5 30607

ttl_to_sets = defaultdict(lambda : {})


def checker():
    f = open("../ttl_result/5/all_resolvers_pool.json")
    d = json.load(f)

    f = open("../ttl_result/{}/resolver_public_local_dict.json".format(ttl))
    d = json.load(f)


def fake_public_analyzer():
    org_dict_list = []
    for ttl in [5, 15, 30]:
        f = open(source_dir + "{}/resolver_to_org_country.json".format(ttl))
        d = json.load(f)
        org_dict_list.append(d)
    org_dict = dict_union(org_dict_list)

    f = open("../ttl_result/public_error_ips.json")
    fake_ips = json.load(f)



    org_counter = defaultdict(lambda : 0)
    not_found = 0
    for e in fake_ips:
        if e not in org_dict:
            not_found += 1
            continue
        org_counter[org_dict[e][0]] += 1

    org_list = []
    tot = 0
    for key in org_counter:
        org_list.append((key, org_counter[key], 0))
        tot += org_counter[key]

    org_list.sort(key=lambda x: x[1], reverse=True)
    cumulative = 0
    o_temp = []
    for e in org_list:
        cumulative += e[1]
        o_temp.append((e[0], e[1], (cumulative / tot) * 100))


    cdf_arr_org = create_cdf_arr_from_counter(counter=org_counter, index_map=False)

    cdf_maker(cdf_arr_org, "Organization index", "CDF", "CDF of Organization for not responding public resolver", None)

    a = 1

    # 81 % 5


# fake_public_analyzer()

def error_histo_maker():
    d = {
        "Port 53 REFUSED":3430,
        "Timeout": 23231,
        "No RRSET": 75,
        "NXDOMAIN":1
    }

    histogram_maker(d, "Error", "count", "Histogram of DNS error")

# error_histo_maker()



def cdf_info(only_local=-1):

    print("Doing TTL {} - local_only {} - min exit {}".format(ttl, only_local, min_exitnodes))
    f = open(source_dir + "{}/all_resolvers_pool.json".format(ttl))

    resolver_pool = json.load(f)
    distinct_hit = len(resolver_pool)
    print("Distinct resolvers", distinct_hit)

    t = 0
    for e in resolver_pool:
        t += e[1]

    print("Distinct resolvers hit total ", t)

    #
    f = open(source_dir + "{}/resolver_public_local_dict.json".format(ttl))
    is_public = json.load(f)
    #is_public = {}
    f = open(source_dir + "{}/final_data.json".format(ttl))
    d = json.load(f)

    f = open(source_dir + "{}/final_resolver_to_asn.json".format(ttl))
    f_r = json.load(f)
    resolver_to_asn = f_r['resolver_to_asn_own']

    final_dict = d["data_elaborate"]
    # print(d["Total_resolvers"])
    ans = []
    # cnt, tot = 0, 0
    tt = 0
    ex_node_set = set()
    abs_correct_count, abs_incorrect_count = 0, 0

    abs_correct_set = set()
    abs_incorrect_set = set()

    print("Final dict keys {}".format(len(final_dict.keys())))

    for key in final_dict:
        try:
            if key not in resolver_to_asn:
                a = 1
            if int(resolver_to_asn[key]) in lum_resolvers_asn:
                continue
        except:
            pass

        if only_local != -1:
            if key not in is_public:
                continue
            if is_public[key] is only_local:
                continue

        correct_set = set()
        incorrect_set = set()
        for e in final_dict[key]["ic"]:
            incorrect_set.add(e[1])
            ex_node_set.add(e[1])
        for e in final_dict[key]["c"]:
            correct_set.add(e[1])
            ex_node_set.add(e[1])

        subtotal = correct_set.union(incorrect_set)
        total = len(subtotal)
        tt += 1

        if total < min_exitnodes:
            continue

        ratio = len(incorrect_set) / total
        if ratio >= 1:
            abs_incorrect_count += 1
            abs_incorrect_set.add(key)
        elif ratio <= 0:
            abs_correct_set.add(key)
            abs_correct_count += 1
        ans.append(ratio)

    # print("ttl ", ttl)
    print("resolvers before filtering ", tt)
    print("resolvers after filtering", len(ans))
    print("total exitnodes", len(ex_node_set))
    print("Correct ", abs_correct_count)
    print("InCorrect ", abs_incorrect_count)
    print("Correct percentage ", (abs_correct_count/len(ans)) * 100)
    print("InCorrect percentage ", (abs_incorrect_count/len(ans)) * 100)




def cdf_starter(only_local=-1):
    f = open(source_dir + "{}/all_resolvers_pool.json".format(ttl))

    resolver_pool = json.load(f)
    distinct_hit = len(resolver_pool)
    print("Distinct resolvers", distinct_hit)
    #
    f = open(source_dir + "{}/resolver_public_local_dict.json".format(ttl))
    is_public = json.load(f)
    #is_public = {}
    f = open(source_dir + "{}/final_data.json".format(ttl))
    d = json.load(f)

    f = open(source_dir + "{}/final_resolver_to_asn.json".format(ttl))
    f_r = json.load(f)
    resolver_to_asn = f_r['resolver_to_asn_own']

    final_dict = d["data_elaborate"]
    # print(d["Total_resolvers"])
    ans = []
    # cnt, tot = 0, 0
    tt = 0
    ex_node_set = set()
    abs_correct_count, abs_incorrect_count = 0, 0

    abs_correct_set = set()
    abs_incorrect_set = set()

    for key in final_dict:
        try:
            if key not in resolver_to_asn:
                a = 1
            if int(resolver_to_asn[key]) in lum_resolvers_asn:
                continue
        except:
            pass

        if only_local != -1:
            if key not in is_public:
                continue
            if is_public[key] is only_local:
                continue

        correct_set = set()
        incorrect_set = set()
        for e in final_dict[key]["ic"]:
            incorrect_set.add(e[1])
            ex_node_set.add(e[1])
        for e in final_dict[key]["c"]:
            correct_set.add(e[1])
            ex_node_set.add(e[1])

        subtotal = correct_set.union(incorrect_set)
        total = len(subtotal)
        tt += 1

        if total < min_exitnodes:
            continue

        ratio = len(incorrect_set) / total
        if ratio >= 1:
            abs_incorrect_count += 1
            abs_incorrect_set.add(key)
        elif ratio <= 0:
            abs_correct_set.add(key)
            abs_correct_count += 1
        ans.append(ratio)

    print("ttl ", ttl)
    print("resolvers", tt, len(ans))
    print("total exitnodes", len(ex_node_set))

    ttl_to_sets[ttl]["abs_correct_set"] = abs_correct_set
    ttl_to_sets[ttl]["abs_incorrect_set"] = abs_incorrect_set
    header = None
    txt_inside = None
    if only_local == -1:
        header = "CDF: TTL Dishonoring Resolvers - TTL {}".format(ttl)
        txt_inside = "{} Total Resolvers exhibiting atleast {} exitnodes \n" \
                     "{} TTL Honoring resolvers ({} %) \n" \
                     "{} TTL Dishonoring resolvers ({} %)".format(len(ans), min_exitnodes, abs_correct_count, (abs_correct_count/len(ans)) * 100, abs_incorrect_count, (abs_incorrect_count/len(ans)) * 100)
    elif only_local:
        header = "CDF: TTL Dishonoring Local Resolvers - TTL {} minutes".format(ttl)
        txt_inside = "{} Local Resolvers exhibiting atleast {} exitnodes".format(len(ans), min_exitnodes)
    else:
        header = "CDF: TTL Dishonoring Public Resolvers - TTL {}".format(ttl)
        txt_inside = "{} Public Resolvers exhibiting atleast {} exitnodes".format(len(ans), min_exitnodes)

    cdf_maker(ans, "Percentage of incorrect exit-nodes", "CDF", header, txt_inside)


def http_parser():
    try:
        f = open("../ttl_result/{}/http_response_dict.json".format(ttl))
        d = json.load(f)

        a = []
        total = 0
        for e in d:
            total += d[e]
            a.append((d[e], e))
        a.sort(reverse=True)
        a = 1
    except Exception as e:
        pass

# http_parser()
#jaccard_cdf()
# TODO histogram !!
# reolver_hits_per_req()

# ttl = 15
# http_parser()

def cmp():
    import json
    f = open("../ttl_result/global_asn_list.json")
    old = json.load(f)

    f = open("../ttl_result/global_asn_list_updated.json")
    new = json.load(f)

    print(len(old))
    print(len(new))
    a = 0

    for e in old:
        if e not in new:
            print(e)
            a += 1

    print(a)


def middle_requests(ttl):
    f = open("../ttl_result/{}/resolver_to_middle_req.json".format(ttl))
    d = json.load(f)

    f = open("../ttl_result/{}/resolver_to_last_req.json".format(ttl))
    last_req_json = json.load(f)

    print(len(d.keys()))
    final_candidates = {}

    for e in d:
        if len(list(d[e].keys())) >= 1:
            final_candidates[e] = d[e]
    print(len(final_candidates.keys()))

    max_dict = defaultdict(lambda: 0)
    median_dict = defaultdict(lambda: 0)


    diff_list = []

    resolver_to_instances = defaultdict(lambda: dict())

    resolver_to_req_to_seq = defaultdict(lambda: defaultdict(lambda : list()))

    for resolver in final_candidates:
        lens = []

        for req_id in final_candidates[resolver]:
            to_be_comapred = 1231231231649125509
            final_candidates[resolver][req_id].sort()
            if resolver in last_req_json and req_id in last_req_json[resolver]:
                to_be_comapred = last_req_json[resolver][req_id]
                diff_in_seconds = final_candidates[resolver][req_id][0] - last_req_json[resolver][req_id]
                if diff_in_seconds <= 60 * ttl + 120:
                    diff_list.append(diff_in_seconds)


            for index in range(len(final_candidates[resolver][req_id])):
                to_be_comapred = min(to_be_comapred, final_candidates[resolver][req_id][0])
                diff_in_seconds = final_candidates[resolver][req_id][index] - to_be_comapred
                if diff_in_seconds <= 10:
                    continue
                lens.append(len(final_candidates[resolver][req_id]) - index)
                resolver_to_instances[resolver][req_id] = final_candidates[resolver][req_id][index: ]


                pre = to_be_comapred
                if len(final_candidates[resolver][req_id]) - index > 1:
                    temp = []
                    for id in range(0, len(resolver_to_instances[resolver][req_id])):
                        temp.append(resolver_to_instances[resolver][req_id][id] - min(pre, resolver_to_instances[resolver][req_id][id]))
                        pre = resolver_to_instances[resolver][req_id][id]

                    resolver_to_req_to_seq[resolver][req_id].append(temp)
                break


        if len(lens) > 0:
            import statistics
            max_dict[resolver] = max(lens)
            median_dict[resolver] = statistics.median(lens)

    med_list, max_list = [], []
    med_list_counter = defaultdict(lambda: 0)
    max_list_counter = defaultdict(lambda: 0)

    for key in median_dict:
        med_list.append(median_dict[key])
        med_list_counter[median_dict[key]] += 1

    for key in max_dict:
        max_list.append(max_dict[key])
        max_list_counter[max_dict[key]] += 1




    # cdf_maker(med_list, "Median number of retries", "CDF", "CDF: Median number of retries, TTL {}".format(ttl), None)
    # cdf_maker(max_list, "Max number of retries", "CDF", "CDF: Max number of retries, TTL {}".format(ttl), None)
    cdf_maker(diff_list, "Time Gap for the first proactive request to come", "CDF", "CDF: Time Gap for the first proactive request to come, TTL {}".format(ttl), None)
    histogram_maker(max_list_counter, "max retries", "count", "Histogram of max retires")
    histogram_maker(med_list_counter, "median retries", "count", "Histogram of median retires")


    arr = []
    resolver_to_asn_dict = get_resolver_to_asn_dict()
    for resolver in resolver_to_instances:
        req_ids_seen = len(resolver_to_instances[resolver].keys())
        arr.append((resolver_to_asn_dict[resolver], get_org(resolver_to_asn_dict[resolver]), req_ids_seen))

    #arr.sort(key=lambda x:x[2], reverse=True)
    org_counter = defaultdict(lambda: 0)
    for e in arr:
        org_counter[e[1]] += e[2]
    arr = []
    for key in org_counter:
        arr.append((key, org_counter[key]))

    arr.sort(key=lambda x: x[1], reverse=True)


    p = {}
    p["org_set"] = arr
    p["recurring_set"] = resolver_to_req_to_seq

    with open("middle_info_extend-{}.json".format(ttl), "w") as ouf:
        json.dump(p, fp=ouf, indent=2)


def is_linear(lst):
    for index in range(1, len(lst)):
        if abs(lst[index] - lst[index - 1]) > 6:
            return False
    return True


def is_increasing(lst):
    for index in range(1, len(lst)):
        if lst[index] - lst[index - 1] < 12:
            return False
    return True


def is_decreasing(lst):
    for index in range(1, len(lst)):
        if lst[index] - lst[index - 1] > -12:
            return False
    return True


def define_temp_list(lst):
    if is_linear(lst):
        return "regular"
    if is_increasing(lst):
        return "increasing"
    if is_decreasing(lst):
        return "decreasing"
    else:
        return "random"


def recurring_set_plot(ttl):
    f = open("middle_info_extend-{}.json".format(ttl))
    recurring_set = json.load(f)["recurring_set"]

    # f = open("middle_info_extend-15.json")
    # recurring_set_15 = json.load(f)["recurring_set"]



    label_list = defaultdict(lambda : list())
    for resolver in recurring_set:
        for req_id in recurring_set[resolver]:
            for lo in recurring_set[resolver][req_id]:
                lst = lo
                temp_lst = []
                x_list = []

                index = 0
                for e in lst:
                    if e <= 5:
                        continue
                    index += 1
                    x_list.append(index)
                    temp_lst.append(e)
                label = define_temp_list(temp_lst)
                label_list[label].append(temp_lst)
                #axs.plot(x_list, temp_lst, marker='.')

    for label in label_list:
        if label == "regular":
            fig, axs = plt.subplots(1, 1, figsize=(10, 5))
        else:
            fig, axs = plt.subplots(1, 1, figsize=(10, 10))
        for lst in label_list[label]:
            length = len(lst)
            if length == 1:
                continue
            x = []
            index = 0
            for i in range(length):
                index += 1
                x.append(index)
            axs.plot(x, lst, marker='.')
        axs.set_title("DNS request intervals - {}".format(label))
        plt.savefig('ttl_result/{}-{}-{}.png'.format("dns_request_interval", ttl, label), bbox_inches="tight")
        plt.clf()





# recurring_set_plot(15)


# middle_requests(5)
# middle_requests(15)

# cmp()


for temp in [5, 15]:
    ttl = temp
    for e in [5, 10]:
        min_exitnodes = e
        for mode in [-1, True, False]:
            cdf_info(only_local=mode)

#
# for e in ttl_to_sets[5]["abs_incorrect_set"]:
#     if e in ttl_to_sets[15]["abs_correct_set"] or e in ttl_to_sets[15]["abs_incorrect_set"]:
#         print(e)
#
# for k in ttl_to_sets:
#     for e in ttl_to_sets[k]:
#         ttl_to_sets[k][e] = list(ttl_to_sets[k][e])
#
# with open("ttl_cmp.json", "w") as ouf:
#     json.dump(ttl_to_sets, fp=ouf)

# def calc_disjoint():
#     f = open("ttl_cmp.json")
#     ttl_to_sets = json.load(f)
#
#     th_cnt = 0
#     false_alarm = 0
#     for e in ttl_to_sets["15"]["abs_incorrect_set"]:
#         if e in ttl_to_sets["30"]["abs_correct_set"]:
#             th_cnt += 1
#
#
#     print(th_cnt)



# f = open("../ttl_result/5/first_hit_resolvers.json")
# keys_1 = set(json.load(f))
#
# f = open("../ttl_result/5/final_resolver_to_asn.json")
# final_resolvers = json.load(f)['resolver_to_asn_own']
# keys_2 = set(list(final_resolvers.keys()))
#
# print(len(keys_2), len(keys_1), len(keys_1.intersection(keys_2)))



#middle_requests(5)
# middle_requests(5)