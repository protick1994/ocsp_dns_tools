import matplotlib.pyplot as plt
import networkx as nx

asn = "ASN 271791 - CABLE NORTE CA."
asn_2 = "Qinghai Telecom"
asn_3 = "The Hancock Telephone Company"

# def get_random_number()

lst = [
    [
      "http://ocsp.incommon-igtf.org",
      1485.0
    ],
    [
      "http://ocsp.pki.goog/gts1c3",
      1014.5
    ],
    [
      "http://ocsp.securecore-ca.com",
      1802.5
    ],
    [
      "http://yandex.ocsp-responder.com",
      479
    ],
    [
      "http://statusf.digitalcertvalidation.com",
      997
    ],
    [
      "http://e1.o.lencr.org",
      462.0
    ],
    [
      "http://ocsp.netsolssl.com",
      1272.0
    ],
    [
      "http://unitedtrustov.ocsp-certum.com",
      813
    ],
    [
      "http://ocsp.pki.goog/gts1o1core",
      266
    ],
    [
      "http://alpiro.ocsp.sectigo.com",
      928
    ],
    [
      "http://certcloud.ocsp.trust-provider.com",
      1048.5
    ],
    [
      "http://ocsp03.sbca.telesec.de/ocspr",
      1397
    ],
    [
      "http://dvcasha2.ocsp-certum.com",
      1226
    ],
    [
      "http://ocsp2.globalsign.com/gsorganizationvalsha2g2",
      1196
    ],
    [
      "http://ocsp.digicert.com",
      1136.0
    ],
    [
      "http://ocsp.buypass.com",
      497.5
    ],
    [
      "http://status.rapidssl.com",
      2427
    ],
    [
      "http://ocsp.pki.goog/gts1d2",
      854
    ],
    [
      "http://ocsp.incommon-ecc.org",
      630.0
    ],
    [
      "http://ocsps.ssl.com",
      647
    ],
    [
      "http://zerossl.ocsp.sectigo.com",
      1689
    ],
    [
      "http://ocsp.comodoca4.com",
      842.0
    ],
    [
      "http://ocsp.starfieldtech.com/",
      1306.5
    ],
    [
      "http://ocsp06.actalis.it/VA/AUTHDV-G3",
      1443
    ],
    [
      "http://ocsp.quovadisglobal.com",
      1959
    ],
    [
      "http://ocsp.usertrust.com",
      1218
    ],
    [
      "http://ocsp.serverpass.telesec.de/ocspr",
      1302
    ],
    [
      "http://ocsp.swisssign.net/E7F1E7FD2E53AD11E5811A57A4738F127D98C8AE",
      1179.0
    ],
    [
      "http://oneocsp.microsoft.com/ocsp",
      450.0
    ],
    [
      "http://ocsp.globalsign.com/ca/gsatlasr3dvtlsca2020",
      411.0
    ],
    [
      "http://h.ocsp-certum.com",
      825.0
    ],
    [
      "http://r3.o.lencr.org",
      1386.0
    ],
    [
      "http://ocsp.entrust.net",
      461.5
    ],
    [
      "http://trustocean.ocsp.sectigo.com",
      588
    ],
    [
      "http://ocsp.trustcor.ca",
      772.5
    ],
    [
      "http://ocsp.trust.telia.com",
      1433
    ],
    [
      "http://rootnetworksdv.ocsp-certum.com",
      3684.5
    ],
    [
      "http://xinchachadv.ocsp-certum.com",
      461
    ],
    [
      "http://ocsp.globalsign.com/gsgccr3dvtlsca2020",
      1137.5
    ],
    [
      "http://statush.digitalcertvalidation.com",
      1035.5
    ],
    [
      "http://ocsp.globalsign.com/rnpicpeduovsslca2019",
      500
    ],
    [
      "http://ocsp.swisssign.net/DBBCBF821859DC69FAF8ABAA834D771D0BB08BD8",
      1030
    ],
    [
      "http://ocsp.digicert.cn",
      705.5
    ],
    [
      "http://ocsp.int-x3.letsencrypt.org",
      514.0
    ],
    [
      "http://status.geotrust.com",
      540
    ],
    [
      "http://ocsp.godaddy.com/",
      833.5
    ],
    [
      "http://ocsp.dcocsp.cn",
      1201
    ]

  ]

lst_3 = [
    [
      "http://certcloud.ocsp.trust-provider.com",
      99.0
    ],
    [
      "http://zerossl.ocsp.sectigo.com",
      99.0
    ],
    [
      "http://ocsp.globalsign.com/ca/attatlasr3ovtlsca2020",
      103.0
    ],
    [
      "http://ocsp.sca1b.amazontrust.com",
      135.0
    ],
    [
      "http://r3.o.lencr.org",
      116.0
    ],
    [
      "http://ocsp.sectigo.com",
      114.0
    ],
    [
      "http://ocsp2.globalsign.com/gsalphasha2g2",
      95.0
    ],
    [
      "http://ocsp.comodoca.com",
      146.0
    ]
  ]


lst_2= [
    [
      "http://dvcasha2.ocsp-certum.com",
      2075.0
    ],
    [
      "http://ocsp.digicert.com",
      1728.0
    ],
    [
      "http://ocspx.digicert.com",
      4769.0
    ],
    [
      "http://ocsp.sectigochina.com",
      2152.5
    ],
    [
      "http://ocsp.starfieldtech.com/",
      1306.5
    ],
    [
      "http://ocsp.usertrust.com",
      1643.5
    ],
    [
      "http://oneocsp.microsoft.com/ocsp",
      1027.0
    ],
    [
      "http://r3.o.lencr.org",
      1374.0
    ],
    [
      "http://ocsp.sectigo.com",
      1460.0
    ]
  ]

import random
lst_2 = lst_3
lst_2 = random.sample(lst_2, 8)

N = len(lst_2)
G = nx.star_graph(N + 1)

index_to_url = {}

for i in range(1, N + 1):
    G.remove_edge(0, i)
    G.add_edge(0, i, len=lst_2[i - 1][1])
    index_to_url[i] = (lst_2[i - 1][0], lst_2[i - 1][1])

G.remove_edge(0, N + 1)
G.add_edge(0, N + 1, len=138)
index_to_url[N + 1] = ("Google Public DNS server", 143)

# 1 .......... N
# N + 1


fig, ax = plt.subplots(figsize=(10, 15))


#pos = nx.spring_layout(G, weight='length')  # Seed layout for reproducibility
pos = nx.nx_agraph.graphviz_layout(G, prog="neato")
colors = range(N + 1)
# #e2a0aa
node_colors = []
node_colors.append("lime")
for i in range(N):
    node_colors.append("#A0CBE2")

node_colors.append("#e2a0aa")
options = {
    "node_color": node_colors,
    "edge_color": colors,
    "width": 2,
    "edge_cmap": plt.cm.Blues,
    "with_labels": True,
    "node_size": 600
}
nx.draw(G, pos=pos, **options)
#nx.draw_networkx()

import matplotlib.patches as mpatches

patches = []
patches.append(mpatches.Patch(label='{}: {}'.format(0, "ASN 397455 - The Hancock Telephone Company")))
for i in range(1, N+2):
    patches.append(mpatches.Patch(label='{}: {} ({} ms)'.format(i, index_to_url[i][0][7: ], index_to_url[i][1])))

plt.legend(handles=patches, handlelength=0, handletextpad=0, loc='upper left', bbox_to_anchor=(0.5, 1.05),
          ncol=1, fancybox=True, shadow=True)

plt.title('Latency from different different OCSP responders for ASN 397455 - The Hancock Telephone Company', y=-0.01)
#ax.set_title('Latency from different different OCSP responders for ASN 271791 - CABLE NORTE CA')
#fig.suptitle('Latency from different different OCSP responders for ASN 271791 - CABLE NORTE CA', fontsize=20)
plt.show()


