import numpy as np
import matplotlib.pyplot as plt
import json
import random
import pandas as pd
import seaborn as sns
import matplotlib.patches as mpatches


def make_sd_bars():

    f = open('luminati_stats/all_data.json')
    d = json.load(f)
    import statistics
    data = d
    m_dict = {}
    for key in data:
        if 'ocsp.comodoca.com' not in key:
            continue
        print("Hit")
        median_latency = []
        candidate = 0
        for asn in data[key]['responder_to_asn_data']:
            if not asn.isnumeric():
                continue
            if len(data[key]['responder_to_asn_data'][asn]) < 10:
                continue
            candidate += 1
            median_latency.append((asn, statistics.median(data[key]['responder_to_asn_data'][asn])))

        if candidate < 10:
            continue

        df = pd.DataFrame()
        asn_list, latency_list = [], []
        import random
        median_latency = random.sample(median_latency, 50)
        for e in median_latency:
            asn_list.append(e[0])
            latency_list.append(e[1])
        df['asn'] = asn_list
        df['latency'] = latency_list

        x = df.loc[:, ['latency']]
        # https://www.wyzant.com/resources/answers/606635/what-does-it-mean-to-divide-by-the-standard-deviation#:~:text=When%20you%20divide%20mean%20differences,a%20standard%20deviation%20of%205.
        df['latency_z'] = (x - x.mean()) / x.std()
        df['colors'] = ['red' if x < 0 else 'green' for x in df['latency_z']]
        df.sort_values('latency_z', inplace=True)
        df.reset_index(inplace=True)

        # Draw plot
        plt.figure(figsize=(14, 10), dpi=80)
        plt.hlines(y=df.index, xmin=0, xmax=df.latency_z, color=df.colors, alpha=0.4, linewidth=5)

        # Decorations
        plt.gca().set(ylabel='$ASN$', xlabel='$Latency$')
        plt.yticks(df.index, df.asn, fontsize=12)
        plt.title('Diverging Bars of OCSP Latency from different ASNs', fontdict={'size': 20})
        plt.grid(linestyle='--', alpha=0.5)
        plt.show()
        a = 1
    print("yo")
def make_error_piramid():
    f = open('luminati_stats/all_data.json')
    d = json.load(f)
    import statistics
    data = d
    all_tuples = []
    for key in data:
        total_records = data[key]['total_records']
        error_count = data[key]['error_count']
        proxy_error_count = data[key]['proxy_error_count']
        if total_records - proxy_error_count == 0:
            continue

        all_tuples.append((total_records - proxy_error_count, error_count - proxy_error_count, key, (error_count - proxy_error_count)/(total_records - proxy_error_count)))

    all_tuples.sort(key=lambda x: x[3], reverse=True)
    #top_5 = all_tuples[-5: ]
    bottom_5 = all_tuples[24: 29]

    keys = []
    errors = []
    suc_responses = []
    for element in bottom_5:
        keys.append(element[2][7:])
        errors.append(element[1])
        suc_responses.append(element[0] - element[1])

    panda_frame = pd.DataFrame(
        {'url': keys,
         'Error': errors,
         'Successful': suc_responses})
    urls = keys

    plt.figure(figsize=(40, 10), dpi=80)

    bar_plot = sns.barplot(x='Error', y='url', data=panda_frame, order=urls, color='r', lw=0)
    bar_plot = sns.barplot(x='Successful', y='url', data=panda_frame, order=urls, color='b', lw=0)

    bar_plot.set(xlabel="Response", ylabel="OCSP responders", title="Error Distribution")
    #bar_plot.set_xticklabels(labels)
    bar_plot.legend()
    plt.show()



'''
https://www.hjp.at/doc/rfc/rfc6960.html

The response "unauthorized" is returned in cases where the client is
not authorized to make this query to this server or the server is not
capable of responding authoritatively (cf. [RFC5019], Section 2.2.3).
'''

def make_propagation_line():

    """
           SELECT * from ocsp_host LEFT join
    (
    SELECT serial, akid, fingerprint, ocsp_url_id, count(DISTINCT ocsp_response_status)
    from ocsp_response_wrt_asn
    WHERE ocsp_response_status in ('OCSPResponseStatus.SUCCESSFUL', 'OCSPResponseStatus.UNAUTHORIZED')
    group by serial, akid, fingerprint, ocsp_url_id
    HAVING count(DISTINCT ocsp_response_status) > 1
    ORDER by count(DISTINCT ocsp_response_status) desc, serial asc
    ) B
    on ocsp_host.id = B.ocsp_url_id
    WHERE ocsp_host.url like '%globalsign%';


    SELECT hop, mode, ocsp_cert_status, ocsp_response_status, created_at from ocsp_response_wrt_asn
    WHERE serial = '1583930704036915995879332032619798899' AND akid = '39CC8287FD9DB1FD3E9C707173232C832EEE450D' and fingerprint = '7e7cac92efb8888401ba6ef41435a44cb36227dddabad9410a643823cdd9568d' AND ocsp_url_id = 200 ORDER by created_at;
    """
    f = open('ocsp_prop.json')
    d = json.load(f)

    is_successful_dict = {
        "OCSPResponseStatus.SUCCESSFUL": True,
        "OCSPResponseStatus.UNAUTHORIZED": False
    }

    day = None
    time_tuples = []
    is_sec_visited = {}

    _min, _max = 1000000000, -1

    for element in d:
        (_day, _time) = element['created_at'].split()
        day = day
        sec = _time
        sec = sec[: sec.find(".")]
        ac_sec = int(sec.split(":")[2])
        if sec in is_sec_visited:
            continue
        is_sec_visited[sec] = True

        _min = min(_min, ac_sec)
        _max = max(_max, ac_sec)

        time_tuples.append((ac_sec, element['hop'], is_successful_dict[element['ocsp_response_status']], sec))

    import random
    while(True):
        ans = random.sample(time_tuples, 10)
        pass_ = False
        for e in ans:
            if e[2] == False:
                pass_ =True
        if pass_:
            break

    time_tuples = ans
    time_tuples.sort()

    times = [e[0] for e in time_tuples]
    is_green_list = [e[2] for e in time_tuples]
    color_dict = {
        True: 'green',
        False: 'firebrick'
    }

    color_list = [color_dict[e[2]] for e in time_tuples]
    asn_list = [e[1] for e in time_tuples]
    x_ticks = [e[3] for e in time_tuples]

    df = pd.DataFrame()

    df['time'] = times


    # df_raw = pd.read_csv("https://github.com/selva86/datasets/raw/master/mpg_ggplot2.csv")
    # df = df_raw[['cty', 'manufacturer']].groupby('manufacturer').apply(lambda x: x.mean())
    # df.sort_values('cty', inplace=True)
    # df.reset_index(inplace=True)

    # Draw plot
    fig, ax = plt.subplots(figsize=(20, 10), dpi=80)
    ax.hlines(y=df.index, color='gray', alpha=0.7, xmin=_min, xmax=_max, linewidth=1, linestyles='dashdot')
    ax.scatter(y=df.index, x=df.time, s=75, color=color_list, alpha=0.7)
    #ax.set_xticks(x_ticks)
    x_tick_loc = [e for e in df.time]
    plt.xticks(x_tick_loc, x_ticks, rotation = 60)

    red_patch = mpatches.Patch(color='red', label='Unauthorized')
    green_patch = mpatches.Patch(color='green', label='Successful')

    plt.legend(handles=[green_patch, red_patch])

    # Title, Label, Ticks and Ylim
    ax.set_title('Dot Plot for OCSP response status from various ASNs', fontdict={'size': 22})
    ax.set_xlabel('Time stamp')
    ax.set_ylabel('ASN')
    ax.set_yticks(df.index)
    #plt.xticks(df.index, x_ticks)
    l = ['x' for e in asn_list]
    # ax.set_xticks(df.index)
    # ax.set_xticklabels(l)
    #plt.xticks(ticks=range(len(x_ticks)), labels=x_ticks, rotation=90)
    ax.set_yticklabels(asn_list, fontdict={'horizontalalignment': 'right'})
    #ax.set_xlim(_min - 10, _max + 10)
    plt.show()
    a = 1


def make_error_line():

    """
           SELECT * from ocsp_host WHERE id in
(
SELECT ocsp_url_id
from ocsp_response_wrt_asn
WHERE ocsp_response_status in ('OCSPResponseStatus.SUCCESSFUL', 'OCSPResponseStatus.INTERNAL_ERROR', 'OCSPResponseStatus.TRY_LATER')
group by serial, akid, fingerprint, ocsp_url_id
HAVING count(DISTINCT ocsp_response_status) > 1
);

SELECT ocsp_response_status, count(*) from ocsp_response_wrt_asn
WHERE serial = '1333704466195140224519777259285303847' AND ocsp_url_id = 242 and ocsp_response_status in ('OCSPResponseStatus.SUCCESSFUL', 'OCSPResponseStatus.INTERNAL_ERROR', 'OCSPResponseStatus.TRY_LATER')
GROUP by ocsp_response_status;


    """

    f = open('ocsp_err_succ.json')
    d = json.load(f)

    is_successful_dict = {
        "OCSPResponseStatus.SUCCESSFUL": True,
        "OCSPResponseStatus.INTERNAL_ERROR": False
    }

    day = None
    time_tuples = []
    is_sec_visited = {}

    _min, _max = 1000000000, -1

    for element in d:
        (_day, _time) = element['created_at'].split()
        day = day
        sec = _time
        sec = sec[: sec.find(".")]
        ac_sec = int(sec.split(":")[2])
        # if sec in is_sec_visited:
        #     continue
        # is_sec_visited[sec] = True

        _min = min(_min, ac_sec)
        _max = max(_max, ac_sec)

        time_tuples.append((ac_sec, element['hop'], is_successful_dict[element['ocsp_response_status']], sec))

    # import random
    # while (True):
    #     ans = random.sample(time_tuples, 10)
    #     pass_ = False
    #     for e in ans:
    #         if e[2] == False:
    #             pass_ = True
    #     if pass_:
    #         break

    # time_tuples = ans
    time_tuples = random.sample(time_tuples, 50)
    time_tuples.sort()

    times = [e[0] for e in time_tuples]
    is_green_list = [e[2] for e in time_tuples]
    color_dict = {
        True: 'green',
        False: 'firebrick'
    }

    color_list = [color_dict[e[2]] for e in time_tuples]
    asn_list = [e[1] for e in time_tuples]
    x_ticks = [e[3] for e in time_tuples]

    df = pd.DataFrame()

    df['time'] = times

    # df_raw = pd.read_csv("https://github.com/selva86/datasets/raw/master/mpg_ggplot2.csv")
    # df = df_raw[['cty', 'manufacturer']].groupby('manufacturer').apply(lambda x: x.mean())
    # df.sort_values('cty', inplace=True)
    # df.reset_index(inplace=True)

    # Draw plot
    fig, ax = plt.subplots(figsize=(20, 10), dpi=80)
    ax.hlines(y=df.index, color='gray', alpha=0.7, xmin=0, xmax=len(df.time) + 2, linewidth=1, linestyles='dashdot')
    ax.scatter(y=df.index, x=range(len(df.time)), s=75, color=color_list, alpha=0.7)
    # ax.set_xticks(x_ticks)
    x_tick_loc = [e for e in df.time]
    plt.xticks(range(len(df.time)), x_ticks, rotation=60)

    red_patch = mpatches.Patch(color='red', label='Internal Error')
    green_patch = mpatches.Patch(color='green', label='Successful')

    plt.legend(handles=[green_patch, red_patch])

    # Title, Label, Ticks and Ylim
    ax.set_title('Internal error distribution from various ASNs', fontdict={'size': 22})
    ax.set_xlabel('Time stamp')
    ax.set_ylabel('ASN')
    ax.set_yticks(df.index)
    # plt.xticks(df.index, x_ticks)
    l = ['x' for e in asn_list]
    # ax.set_xticks(df.index)
    # ax.set_xticklabels(l)
    # plt.xticks(ticks=range(len(x_ticks)), labels=x_ticks, rotation=90)
    ax.set_yticklabels(asn_list, fontdict={'horizontalalignment': 'right'})
    # ax.set_xlim(_min - 10, _max + 10)
    plt.show()
    a = 1

make_sd_bars()

'''
Error Distribution
https://matplotlib.org/stable/gallery/lines_bars_and_markers/horizontal_barchart_distribution.html#sphx-glr-gallery-lines-bars-and-markers-horizontal-barchart-distribution-py

'''




