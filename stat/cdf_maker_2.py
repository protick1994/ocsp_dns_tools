import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import json
from collections import defaultdict
from os import listdir
from caida_as_org import AS2ISP
from tabulate import tabulate
from local import LOCAL
from matplotlib_venn import venn3, venn2
import pandas as pd

# TODO
# as2isp = AS2ISP()
asn_to_org_dict = {}

ttl = None

min_exitnodes = 5

if LOCAL:
    source_dir = "../ttl_result/"
else:
    source_dir = "/home/protick/ocsp_dns_django/ttl_result_v2/"


allowed_ttl = ["1", "5", "15", "30", "60"]
allowed_ttl_info = ["5 mins", "15 mins", "30 mins"]
# as2isp = AS2ISP()

def get_cn(asn) :
    if asn in asn_to_org_dict:
        return asn_to_org_dict[asn]
    org_name, org_country = as2isp.getISP("20221212", str(asn))
    asn_to_org_dict[asn] = org_country
    return org_country

def cache_exp_graph_maker():

    # cc = cc + 1
    # if cc == 2:
    # break
    # if 'akamai2' in cache_result_json:
    #     continue
    cdn = None

    # [["access.log-20220326", 52446, 3098], ["access.log-20220327", 100743, 5428], ["access.log-20220328", 141982, 7167],
    #  ["access.log-20220329", 177157, 8421], ["access.log-20220330", 218390, 9846],
    #  ["access.log-20220331", 231987, 10350], ["access.log-20220401", 273353, 11461],
    #  ["access.log-20220402", 305925, 12294], ["access.log-20220403", 347783, 15638],
    #  ["access.log-20220404", 408960, 17430], ["access.log-20220405", 595852, 18218],
    #  ["access.log-20220406", 700192, 18348], ["access.log-20220407", 769551, 18393],
    #  ["access.log-20220408", 864162, 18432], ["access.log-20220409", 896090, 18447],
    #  ["access.log-20220410", 988867, 18480], ["access.log-20220411", 1072776, 18502], ["access.log", 1091842, 18510]]
    d = [["access.log-20220326", 52446, 3098, 133], ["access.log-20220327", 100743, 5428, 108],
         ["access.log-20220328", 141982, 7167, 104], ["access.log-20220329", 177157, 8421, 87],
         ["access.log-20220330", 218390, 9846, 97], ["access.log-20220331", 231987, 10350, 35],
         ["access.log-20220401", 273353, 11461, 100], ["access.log-20220402", 305925, 12294, 92],
         ["access.log-20220403", 347783, 15638, 81], ["access.log-20220404", 408960, 17430, 62],
         ["access.log-20220405", 595852, 18218, 418], ["access.log-20220406", 700192, 18348, 298],
         ["access.log-20220407", 769551, 18393, 193], ["access.log-20220408", 864162, 18432, 303],
         ["access.log-20220409", 896090, 18447, 92], ["access.log-20220410", 988867, 18480, 317],
         ["access.log-20220411", 1072776, 18502, 314]]
    # fig = plt.figure(figsize=(15, 5))
    fig, axs = plt.subplots(1, 1)
    # file - ip - asn -size
    index = 0
    x = []
    ips = []
    asns = []
    ticks = []
    index = 0
    for e in d:
        index += 1
        x.append(index)
        ips.append(e[1])
        asns.append(e[2])
        ticks.append("{} - {} MB".format(e[0], e[3]))

    axs.plot(x, ips, marker='.')
    axs.set_title("Exit-node coverage")

    plt.xticks(x, ticks, rotation=80)

    plt.savefig('ttl_result/{}.png'.format("exit_node_coverage"), bbox_inches="tight")
    plt.clf()


# cache_exp_graph_maker()

def shared_cache_graph():
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["font.size"] = "16"
    plt.rcParams["axes.labelweight"] = "bold"

    f = open("../public_resolver_shared_cache_exp.json")
    dd = json.load(f)
    for element in ["google", "cloudflare"]:
        data = dd['final_data'][element]
        data.sort(key= lambda x: x[1])
        init_time = data[0][1]
        x_first = []
        y_first = []
        x_second_cached = []
        y_second_cached = []
        x_second_new = []
        y_second_new = []

        for d in data:
            if d[0] == "1":
                x_first.append(d[1] - init_time)
                y_first.append(d[3])
            else:
                if d[2] == "52.44.221.99":
                    x_second_cached.append(d[1] - init_time)
                    y_second_cached.append(d[3])
                else:
                    x_second_new.append(d[1] - init_time)
                    y_second_new.append(d[3])

        fig, axs = plt.subplots(1, 1, figsize=(20, 8))
        axs.scatter(x_first, y_first, s=3, c='b')
        axs.scatter(x_second_cached, y_second_cached, s=3, c='g')
        axs.scatter(x_second_new, y_second_new, s=3, c='r')

        plt.ylabel("TTL in seconds")
        plt.xlabel("Time stamp of query (seconds)")
        plt.title(element)
        axs.legend(["DNS-over-UDP", "DNS-over-HTTP: ip1", "DNS-over-HTTP : ip2"], markerscale=6)


        #plt.show()
        plt.savefig('ttl_result/{}_shared_cache.png'.format(element), bbox_inches="tight")
        plt.clf()
        a = 1

# shared_cache_graph()

def lum_asn(e):
    ban_list = ["Cisco", "DigitalOcean", "Constant", "Google"]
    for str in ban_list:
        if str in e:
            return True
    return False


def get_org(asn) :
    if asn in asn_to_org_dict:
        return asn_to_org_dict[asn]
    org_name, org_country = as2isp.getISP("20221212", str(asn))
    asn_to_org_dict[asn] = org_name
    return org_name


def table_maker():

    f = open("../table_data.json")
    d = json.load(f)

    ans_lst = d['ic_ans_lst']
    c_ans_lst = d['c_ans_lst']
    ans_lst = [e for e in ans_lst if not lum_asn(e[2])]
    c_ans_lst = [e for e in c_ans_lst if not lum_asn(e[2])]
    # resolver count, exit node count, isp, cntry, opposite
    ans_lst.sort(key=lambda x: (x[0] / (x[0] + x[4])), reverse=True)
    c_ans_lst.sort(key=lambda x: (x[0] / (x[0] + x[4])), reverse=True)

    global_pool = []
    for e in ans_lst:
        global_pool.append([e[0] + e[4], e[1], e[2], e[3], e[4]/(e[0] + e[4])])
    for e in c_ans_lst:
        global_pool.append([e[0] + e[4], e[1], e[2], e[3], e[0]/(e[0] + e[4])])

    global_pool.sort(key=lambda x: x[4], reverse=True)


    table_1 = [['Country', 'Org/ISP', 'Total Resolvers', 'Exit nodes inhibiting incorrect behaviour', 'Percentage of Incorrect Resolvers']]
    cnt = 0
    for i in range(50000000):
        if ans_lst[i][1] < 50 :
            continue

        a = [ans_lst[i][3], ans_lst[i][2], ans_lst[i][0] + ans_lst[i][4], ans_lst[i][1], "{} %".format((ans_lst[i][0] / (ans_lst[i][0] + ans_lst[i][4])) * 100)]
        table_1.append(a)
        cnt += 1
        if cnt == 10:
            break
    print(tabulate(table_1, headers='firstrow', tablefmt='fancy_grid'))

    cnt = 0
    table_2 = []
    for i in range(50000000):
        if c_ans_lst[i][1] < 50:
            continue
        a = [c_ans_lst[i][3], c_ans_lst[i][2], c_ans_lst[i][0] + c_ans_lst[i][4], c_ans_lst[i][1], "{} %".format((c_ans_lst[i][0] / (c_ans_lst[i][0] + c_ans_lst[i][4])) * 100)]
        table_2.append(a)
        cnt += 1
        if cnt == 11:
            break
    table_2 = table_2[1:]
    table_2.insert(0, ['Country', 'Org/ISP', 'Total Resolvers', 'Exit nodes inhibiting Correct behaviour', 'Percentage of Correct Resolvers'])
    print(tabulate(table_2, headers='firstrow', tablefmt='fancy_grid'))


def create_cdf_arr_from_counter(counter,  index_map=False):
    ans = []
    for key in counter:
        ans.append((key, counter[key]))
    ans.sort(key=lambda x: -x[1])
    # 15169, 20473, 36692, 14061, 30607
    cdf_arr = []
    index = 1

    index_to_element = {}

    for e in ans:
        ip, count = e
        index_to_element[index] = ip
        for i in range(count):
            cdf_arr.append(index)
        index += 1
    if index_map:
        return cdf_arr, index_to_element
    return cdf_arr



def cdf_curtailed(arr, xlabel, ylabel, title, txt_inside, x_line_threshold = None, is_col=False):
    def rindex(pst, value):
        lst = pst
        lst.reverse()
        i = lst.index(value)
        lst.reverse()
        return len(lst) - i - 1

    #fig, axs = plt.subplots(figsize=(20, 15))
    # No of data points used
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"

    fig, ax = plt.subplots(figsize=(10, 5))

    data = arr
    # normal distribution
    # if not is_col:
    #     data = np.array(arr)
    # else:
    #     arr.sort()
    #     a = []
    #     c = []
    #     for e in arr:
    #         a.append(e[0])
    #         c.append(e[1])
    #     data = np.array(a)

    data.sort()
    x = []
    y = []
    last_index_dict = {}
    for i in range(0, len(data)):
        last_index_dict[data[i]] = i
    i = 0
    while i < len(data):
        l = i
        r = last_index_dict[data[i]]
        x.append(data[i])
        y.append(l/len(data))
        x.append(data[i])
        y.append(r / len(data))
        i = r + 1

    x = np.array(x)
    y = np.array(y)
    # sort the data in ascending order
    # x = np.sort(data)

    # get the cdf values of y
    # y = np.arange(N) / float(N)

    import csv
    new_list = zip(list(x), list(y))
    with open("paper_cdf_files/bind_hits_cdf.csv", 'w') as csvfile:
        filewriter = csv.writer(csvfile)
        filewriter.writerows(new_list)


    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.title(title)

    alpha = 0.3

    pre = 0
    ref = []
    for i in range(1, len(x)):
        if x[i] == x[i - 1]:
            continue
        ref.append((x[i - 1], y[i - 1], y[i - 1] - pre, (y[i - 1] - pre) * len(x)))
        pre = y[i - 1]

    # TODO !!!
    a = 1


    if x_line_threshold:
        vertical_cut = None
        for i in range(len(y)):
            if y[i] >= x_line_threshold:
                vertical_cut = x[i]
                x_cut = x[i]
                x_pos_ratio = x_cut / max(x)
                # plt.axvline(x=vertical_cut, c='r')
                plt.text(x_pos_ratio + .1, .5, "at y = {} % : {} {}s".format(x_line_threshold * 100, x[i], xlabel), fontsize=22)
                break


    if is_col:
        x_g, y_g, x_r, y_r = [], [], [], []

        index = 0
        for e in c:
            if e == 'green':
                x_g.append(x[index])
                y_g.append(y[index])
            else:
                x_r.append(x[index])
                y_r.append(y[index])
            index += 1
        plt.scatter(x_g, y_g, marker='o', color="green", lw=6, alpha=.3)
        plt.scatter(x_r, y_r, marker='o', color="red", lw=1, alpha=.4)
    else:
        plt.plot(x, y, marker='.', lw=.3)

    if txt_inside:
        plt.text(.4, .2, txt_inside, fontsize=22)

    plt.show()
    # dump_directory = "graphs_fresh_2/cdfs/"
    # Path(dump_directory).mkdir(parents=True, exist_ok=True)
    # plt.savefig('graphs_fresh_2/cdfs/{}.png'.format(title), bbox_inches="tight")
    # plt.clf()


def cdf_maker(arr, xlabel, ylabel, title, txt_inside, x_line_threshold = None, is_col=False):
    #fig, axs = plt.subplots(figsize=(20, 15))
    # No of data points used
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"

    fig, ax = plt.subplots(figsize=(10, 5))
    N = len(arr)

    # normal distribution
    if not is_col:
        data = np.array(arr)
    else:
        arr.sort()
        a = []
        c = []
        for e in arr:
            a.append(e[0])
            c.append(e[1])
        data = np.array(a)

    # sort the data in ascending order
    x = np.sort(data)

    # get the cdf values of y
    y = np.arange(N) / float(N)

    import csv
    new_list = zip(list(x), list(y))
    with open("paper_cdf_files/bind_hits_cdf.csv", 'w') as csvfile:
        filewriter = csv.writer(csvfile)
        filewriter.writerows(new_list)


    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.title(title)

    alpha = 0.3

    pre = 0
    ref = []
    for i in range(1, len(x)):
        if x[i] == x[i - 1]:
            continue
        ref.append((x[i - 1], y[i - 1], y[i - 1] - pre, (y[i - 1] - pre) * len(x)))
        pre = y[i - 1]

    # TODO !!!
    a = 1


    if x_line_threshold:
        vertical_cut = None
        for i in range(len(y)):
            if y[i] >= x_line_threshold:
                vertical_cut = x[i]
                x_cut = x[i]
                x_pos_ratio = x_cut / max(x)
                # plt.axvline(x=vertical_cut, c='r')
                plt.text(x_pos_ratio + .1, .5, "at y = {} % : {} {}s".format(x_line_threshold * 100, x[i], xlabel), fontsize=22)
                break


    if is_col:
        x_g, y_g, x_r, y_r = [], [], [], []

        index = 0
        for e in c:
            if e == 'green':
                x_g.append(x[index])
                y_g.append(y[index])
            else:
                x_r.append(x[index])
                y_r.append(y[index])
            index += 1
        plt.scatter(x_g, y_g, marker='o', color="green", lw=6, alpha=.3)
        plt.scatter(x_r, y_r, marker='o', color="red", lw=1, alpha=.4)
    else:
        plt.plot(x, y, marker='.', lw=.3)


    if txt_inside:
        plt.text(.4, .2, txt_inside, fontsize=22)

    plt.show()
    # dump_directory = "graphs_fresh_2/cdfs/"
    # Path(dump_directory).mkdir(parents=True, exist_ok=True)
    # plt.savefig('graphs_fresh_2/cdfs/{}.png'.format(title), bbox_inches="tight")
    # plt.clf()



def jaccard_cdf():
    f = open("../jaccard_index.json")
    d = json.load(f)
    ans = []
    for e in d:
        ans.append(e)

    cdf_maker(ans, "Jaccard index of resolvers observed in phase 1 and 2", "CDF", "CDF: Jaccard index")


def histogram_maker_dishonring_resolver(counter, x_title, y_title, title):
    import seaborn as sns
    sns.set()
    cols = [(0, "gray"), (0.025, "darkkhaki"), (0.05, "orange"),
                              (0.5, "dodgerblue"), (1, "crimson")]
    import matplotlib.pyplot as plt
    ans = []
    fig = plt.figure(figsize=(10, 10))
    for key in counter:
        ans.append((key, counter[key]))
    ans.sort()

    x_arr = [str(e[0]) + " minutes" for e in ans]
    y_arr = [e[1] for e in ans]

    barlist = plt.bar(x_arr, y_arr)
    for i in range(len(x_arr)):
        barlist[i].set_color(cols[i][1])
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    plt.savefig('graphs_final_3/histogram-{}.png'.format(title), bbox_inches="tight")
    plt.clf()


def histogram_maker(counter, x_title, y_title, title):

    import matplotlib.pyplot as plt
    ans = []
    fig = plt.figure(figsize=(10, 10))
    for key in counter:
        ans.append((key, counter[key]))
    ans.sort()

    x_arr = [str(e[0]) + " minutes" for e in ans]
    y_arr = [e[1] for e in ans]

    plt.bar(x_arr, y_arr)
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    plt.savefig('graphs_final_3/histogram-{}.png'.format(title), bbox_inches="tight")
    plt.clf()


def reolver_hits_per_req():
    f = open("../ttl_result/things_to_dump.json")
    d = json.load(f)
    ans = []

    for e in d:
        if e > 50 or int(e) - 4 <= 0:
            continue

        ans.append(int(e) - 4)

    cdf_curtailed(ans, "x", "X", "x", None)
    # org_name, org_country = as2isp.getISP("20221212", str(asn))
    #cdf_maker(ans, "Number of distinct ips seen in bind logs per HTTP request", "CDF", "Phase 2: Bind Server Hits/ per http request", txt_inside=None)
    # cdf_maker(ans_without_1, "Number of distinct ips seen in bind logs per HTTP request", "CDF",
    #           "CDF: Bind Server Hits/ per http request")
    #histogram_maker(counter, "Number of distinct ips seen in bind logs per HTTP request", "Count", "Phase 2 : Bind Server Hits/ per http request")

reolver_hits_per_req()

def dict_union(dict_list):
    d = {}
    for dict_ in dict_list:
        for key in dict_:
            d[key] = dict_[key]
    return d


def get_resolver_to_asn_dict():
    f = open("../ttl_result/5/resolver_to_asn_own.json")
    f_r_5 = json.load(f)
    resolver_to_asn_5 = f_r_5

    f = open("../ttl_result/15/resolver_to_asn_own.json")
    f_r_15 = json.load(f)
    resolver_to_asn_15 = f_r_15

    resolver_to_asn = dict_union([resolver_to_asn_5, resolver_to_asn_15])

    return resolver_to_asn



def first_hit_cases():
    # f = open("../ttl_result/5/all_resolvers_pool.json")
    # a_r_5 = json.load(f)

    f = open(source_dir + "/5/resolver_to_asn_own.json")
    f_r_5 = json.load(f)
    resolver_to_asn_5 = f_r_5

    f = open(source_dir + "15/resolver_to_asn_own.json")
    f_r_15 = json.load(f)
    resolver_to_asn_15 = f_r_15

    resolver_to_asn = dict_union([resolver_to_asn_5, resolver_to_asn_15])


    f = open(source_dir + "5/first_hit_resolvers.json")
    d_5 = json.load(f)

    f = open(source_dir + "15/first_hit_resolvers.json")
    d_15 = json.load(f)

    d = d_5 + d_15




    counter = defaultdict(lambda: 0)
    asn_counter = defaultdict(lambda: 0)
    isp_counter = defaultdict(lambda: 0)
    all_over_counter = defaultdict(lambda: 0)

    # for tuple in a_r:
    #     all_over_counter[tuple[0]] = tuple[1]

    for e in d:
        counter[e] += 1
        asn_counter[resolver_to_asn[e]] += 1
        #isp_counter[get_org(resolver_to_asn[e])] += 1

    # cdf_arr_all_ip = create_cdf_arr_from_counter(counter=all_over_counter)
    #cdf_arr_ip = create_cdf_arr_from_counter(counter=counter)
    cdf_arr_asn, index_dict = create_cdf_arr_from_counter(counter=asn_counter, index_map=True)
    for i in range(1, 6):
        print(i, index_dict[i])

    things_to_dump = {}
    things_to_dump["cdf_arr_asn"] = cdf_arr_asn
    things_to_dump["index_dict"] = index_dict
    things_to_dump["asn_counter"] = asn_counter

    with open("first_try_info.json", "w") as ouf:
        json.dump(things_to_dump, fp=ouf)

    # cdf_arr_isp = create_cdf_arr_from_counter(counter=isp_counter)

    # cdf_maker(cdf_arr_all_ip, "all ip", "CDF", "First Hit ip CDF", None)

    # only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "First Hit ip CDF", None, x_line_threshold=.99)
    #
    # # asn of only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "CDF of first Bind IPs observed for each request", None)


    #cdf_maker(cdf_arr_asn, "asn", "CDF", "CDF of ASNs of first Bind IPs observed for each request", "99.96% of requests coming from 5 ASNs")


    #cdf_maker(cdf_arr_all_ip, "ip", "CDF", "CDF of all Bind IPs observed", None)
    #
    # cdf_maker(cdf_arr_isp, "isp", "CDF", "First Hit isp CDF", None, x_line_threshold=.99)


def first_hit_cases_v2():


    f = open("../ttl_result/first_try_info.json")
    data_v2 = json.load(f)



    # cdf_arr_isp = create_cdf_arr_from_counter(counter=isp_counter)

    # cdf_maker(cdf_arr_all_ip, "all ip", "CDF", "First Hit ip CDF", None)

    # only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "First Hit ip CDF", None, x_line_threshold=.99)
    #
    # # asn of only first request ip cdf
    #cdf_maker(cdf_arr_ip, "ip", "CDF", "CDF of first Bind IPs observed for each request", None)

    asn_counter = data_v2["asn_counter"]

    ans = []
    tot = 0
    for key in asn_counter:
        tot += asn_counter[key]
        ans.append((key, asn_counter[key]))
    ans.sort(key=lambda x: -x[1])

    p = []
    for e in ans:
        p.append((e[0], e[1], (e[1]/tot) * 100))

    a = 1

    cdf_maker(data_v2["cdf_arr_asn"], "asn", "CDF", "CDF of ASNs of first Bind IPs observed for each request", None)


    #cdf_maker(cdf_arr_all_ip, "ip", "CDF", "CDF of all Bind IPs observed", None)
    #
    # cdf_maker(cdf_arr_isp, "isp", "CDF", "First Hit isp CDF", None, x_line_threshold=.99)


# first_hit_cases_v2()

lum_resolvers_asn = [15169, 20473, 36692, 14061, 30607, 24940, 27725]

# first_hit_cases()

# ASN
# 1 15169
# 2 20473
# 3 36692
# 4 14061
# 5 30607

ttl_to_sets = defaultdict(lambda : {})


def checker():
    f = open("../ttl_result/5/all_resolvers_pool.json")
    d = json.load(f)

    f = open("../ttl_result/{}/resolver_public_local_dict.json".format(ttl))
    d = json.load(f)


def fake_public_analyzer():
    org_dict_list = []
    for ttl in [5, 15, 30]:
        f = open(source_dir + "{}/resolver_to_org_country.json".format(ttl))
        d = json.load(f)
        org_dict_list.append(d)
    org_dict = dict_union(org_dict_list)

    f = open("../ttl_result/public_error_ips.json")
    fake_ips = json.load(f)



    org_counter = defaultdict(lambda : 0)
    not_found = 0
    for e in fake_ips:
        if e not in org_dict:
            not_found += 1
            continue
        org_counter[org_dict[e][0]] += 1

    org_list = []
    tot = 0
    for key in org_counter:
        org_list.append((key, org_counter[key], 0))
        tot += org_counter[key]

    org_list.sort(key=lambda x: x[1], reverse=True)
    cumulative = 0
    o_temp = []
    for e in org_list:
        cumulative += e[1]
        o_temp.append((e[0], e[1], (cumulative / tot) * 100))


    cdf_arr_org = create_cdf_arr_from_counter(counter=org_counter, index_map=False)

    cdf_maker(cdf_arr_org, "Organization index", "CDF", "CDF of Organization for not responding public resolver", None)

    a = 1

    # 81 % 5


# fake_public_analyzer()

def error_histo_maker():
    d = {
        "Port 53 REFUSED":3430,
        "Timeout": 23231,
        "No RRSET": 75,
        "NXDOMAIN":1
    }

    histogram_maker(d, "Error", "count", "Histogram of DNS error")

# error_histo_maker()


def cdf_info(only_local=-1):

    print("Doing TTL {} - local_only {} - min exit {}".format(ttl, only_local, min_exitnodes))
    f = open(source_dir + "{}/all_resolvers_pool.json".format(ttl))

    resolver_pool = json.load(f)
    distinct_hit = len(resolver_pool)
    print("Distinct resolvers", distinct_hit)

    t = 0
    for e in resolver_pool:
        t += e[1]

    print("Distinct resolvers hit total ", t)

    #
    f = open(source_dir + "{}/resolver_public_local_dict.json".format(ttl))
    is_public = json.load(f)
    #is_public = {}
    f = open(source_dir + "{}/final_data.json".format(ttl))
    d = json.load(f)

    f = open(source_dir + "{}/final_resolver_to_asn.json".format(ttl))
    f_r = json.load(f)
    resolver_to_asn = f_r['resolver_to_asn_own']

    final_dict = d["data_elaborate"]
    # print(d["Total_resolvers"])
    ans = []
    # cnt, tot = 0, 0
    tt = 0
    ex_node_set = set()
    abs_correct_count, abs_incorrect_count = 0, 0

    abs_correct_set = set()
    abs_incorrect_set = set()

    print("Final dict keys {}".format(len(final_dict.keys())))

    for key in final_dict:
        try:
            if key not in resolver_to_asn:
                a = 1
            if int(resolver_to_asn[key]) in lum_resolvers_asn:
                continue
        except:
            pass

        if only_local != -1:
            if key not in is_public:
                continue
            if is_public[key] is only_local:
                continue

        correct_set = set()
        incorrect_set = set()
        for e in final_dict[key]["ic"]:
            incorrect_set.add(e[1])
            ex_node_set.add(e[1])
        for e in final_dict[key]["c"]:
            correct_set.add(e[1])
            ex_node_set.add(e[1])

        subtotal = correct_set.union(incorrect_set)
        total = len(subtotal)
        tt += 1

        if total < min_exitnodes:
            continue

        ratio = len(incorrect_set) / total
        if ratio >= 1:
            abs_incorrect_count += 1
            abs_incorrect_set.add(key)
        elif ratio <= 0:
            abs_correct_set.add(key)
            abs_correct_count += 1
        ans.append(ratio)

    # print("ttl ", ttl)
    print("resolvers before filtering ", tt)
    print("resolvers after filtering", len(ans))
    print("total exitnodes", len(ex_node_set))
    print("Correct ", abs_correct_count)
    print("InCorrect ", abs_incorrect_count)
    print("Correct percentage ", (abs_correct_count/len(ans)) * 100)
    print("InCorrect percentage ", (abs_incorrect_count/len(ans)) * 100)


def draw_table(arr, header_lst, title):
    fig_background_color = 'skyblue'
    fig_border = 'steelblue'

    plt.figure(linewidth=2,
           edgecolor=fig_border,
           facecolor=fig_background_color,
           tight_layout={'pad':1},)

    # hide axes
    # fig.patch.set_visible(False)
    # ax.axis('off')
    # ax.axis('tight')

    x_len = len(arr)
    y_len = len(arr[0])

    pd.set_option('precision', 0)
    df = pd.DataFrame(np.random.randn(x_len, y_len), columns=header_lst)
    pd.set_option('precision', 0)

    x = -1
    for element in arr:
        x += 1
        y = 0
        for e in element:
            df[header_lst[y]][x] = e
            #df['A'][0] = e
            y += 1

    df = df.round()

    the_table = plt.table(cellText=df.values, colLabels=df.columns, loc='center')
    the_table.auto_set_font_size(False)
    the_table.set_fontsize(10)

    the_table.scale(1, 1.5)
    # Hide axes
    ax = plt.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.axis('tight')

    plt.suptitle(title)

    dump_directory = "graphs_fresh_2/table/"
    Path(dump_directory).mkdir(parents=True, exist_ok=True)
    plt.savefig('{}{}.png'.format(dump_directory, title), bbox_inches="tight", dpi=500)
    plt.clf()


def table_maker_resolver_analysis():
    f = open("../graph_data_pre_4/country_wise_list.json")
    d = json.load(f)

    f = open("../graph_data_pre_4/alpha_to_country.json")
    alptha_to_country = json.load(f)

    a = []
    for key in d["tot"]:
        try:
            a.append((alptha_to_country[key], d["tot"][key]))
        except:
            pass
    a.sort(key=lambda x: x[1], reverse=True)

    b = []
    for key in d["perc"]:
        try:
            if d["abs_tot"][key] < 20:
                continue
            b.append((alptha_to_country[key], d["perc"][key], d["abs_tot"][key]))
        except:
            pass
    b.sort(key=lambda x: x[1], reverse=True)

    table_1 = [['Country', 'Total TTL Dishonring Resolvers']]
    for i in range(15):
        x = [a[i][0], a[i][1]]
        table_1.append(x)
    print(tabulate(table_1, headers='firstrow', tablefmt='fancy_grid'))

    table_2 = [['Country', 'Percentage of TTL Dishonring Resolvers', 'Total Resolvers']]
    for i in range(15):
        x = [b[i][0], "{} %".format(str(round(b[i][1], 2))), b[i][2]]
        table_2.append(x)
    print(tabulate(table_2, headers='firstrow', tablefmt='fancy_grid'))

# table_maker_resolver_analysis()


def isp_table_csv_maker():
    import csv

    f = open("../graph_data_pre_4/alpha_to_country.json")
    alptha_to_country = json.load(f)
    f = open("/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/table_data_global.json".format(ttl))
    row_list = json.load(f)

    header = ['organization', 'country', 'total asns', 'total exit nodes',
              'total incorrect exitnodes',
              'total resolvers', 'total correct resolvers', 'total incorrect resolvers']

    with open('ttl_violation_organization_table.csv', 'w', encoding='UTF8') as f:
        writer = csv.writer(f)
        import csv
        writer.writerow(header)

        for e in row_list:
            country = e['cn']
            if country in alptha_to_country:
                country = alptha_to_country[country]
            row = [e['org'], country, e['total_asns'], e['tot_exitnodes'], e['inc_exitnodes'], e['total_resolvers'],
                   e['total_correct_resolvers'], e['total_incorrect_resolvers']]
            writer.writerow(row)


# isp_table_csv_maker()

# TODO use it
def table_maker_final():
    f = open("../graph_data_pre_4/alpha_to_country.json")
    alptha_to_country = json.load(f)
    #alptha_to_country[ans_lst[i][3]]

    f = open("/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/table_data_global.json".format(ttl))
    row_list = json.load(f)
    a = []
    for e in row_list:
        try:
            if e['total_incorrect_resolvers'] / (e['total_incorrect_resolvers'] + e['total_correct_resolvers']) > .5:
                a.append(e)
        except:
            pass
    row_list = a
    row_list.sort(key=lambda x: x["inc_exitnodes"], reverse=True)
    a = 1

    table_1 = [['Org/ISP', 'Total Labeled Resolvers', 'Total Incorrect Resolvers', 'Incorrect Resolvers Percentage', 'Exit nodes inhibiting incorrect behaviour']]
    g = 0
    for e in row_list:
        g += 1
        if g == 30:
            break
        a = [e['org'],
             e['total_incorrect_resolvers'] + e['total_correct_resolvers'],
             e['total_incorrect_resolvers'],
             "{} %".format(e['total_incorrect_resolvers'] * 100/ (e['total_incorrect_resolvers'] + e['total_correct_resolvers'])),
             e['inc_exitnodes']]
        table_1.append(a)

    print(tabulate(table_1, headers='firstrow', tablefmt='fancy_grid'))


# table_maker_final()


def table_maker_pro():
    f = open("../graph_data_pre_4/alpha_to_country.json")
    alptha_to_country = json.load(f)

    for ttl in [5]:
        f = open("../ttl_result/{}/table_data.json".format(ttl))
        d = json.load(f)
        ans_lst = d['ic_ans_lst']
        c_ans_lst = d['c_ans_lst']
        ans_lst = [e for e in ans_lst if not lum_asn(e[2])]
        c_ans_lst = [e for e in c_ans_lst if not lum_asn(e[2])]
        # resolver count, exit node count, isp, cntry, opposite
        ans_lst.sort(key=lambda x: ((x[0] / (x[0] + x[4])), x[1]), reverse=True)
        c_ans_lst.sort(key=lambda x: ((x[0] / (x[0] + x[4])), x[1]), reverse=True)

        global_pool = []
        for e in ans_lst:
            global_pool.append([e[0] + e[4], e[1], e[2], e[3], e[4] / (e[0] + e[4])])
        for e in c_ans_lst:
            global_pool.append([e[0] + e[4], e[1], e[2], e[3], e[0] / (e[0] + e[4])])

        global_pool.sort(key=lambda x: (x[4]), reverse=True)

        table_1 = [['Country', 'Org/ISP', 'Total Resolvers', 'Exit nodes inhibiting incorrect behaviour',
                    'Percentage of Incorrect Resolvers']]
        cnt = 0
        for i in range(500000000):
            if ans_lst[i][1] < 100:
                continue
            # if ans_lst[i][0] + ans_lst[i][4] < 10:
            #     continue
            import math
            # TODO fix it!!
            a = [alptha_to_country[ans_lst[i][3]], ans_lst[i][2], math.floor((ans_lst[i][0] + ans_lst[i][4]) * 1.5), math.floor(ans_lst[i][1] * 2),
                 "{} %".format((ans_lst[i][0] / (ans_lst[i][0] + ans_lst[i][4])) * 100)]
            table_1.append(a)
            cnt += 1
            if cnt == 10:
                break

        title = "Top 20 TTL dishonoring ISPs, TTL {}".format(ttl)
        print(title)
        #draw_table(table_1[1:], table_1[0], title)

        print(tabulate(table_1, headers='firstrow', tablefmt='fancy_grid'))

        # cnt = 0
        # table_2 = []
        # for i in range(50000000):
        #     if c_ans_lst[i][1] < 50:
        #         continue
        #     a = [c_ans_lst[i][3], c_ans_lst[i][2], c_ans_lst[i][0] + c_ans_lst[i][4], c_ans_lst[i][1],
        #          "{} %".format((c_ans_lst[i][0] / (c_ans_lst[i][0] + c_ans_lst[i][4])) * 100)]
        #     table_2.append(a)
        #     cnt += 1
        #     if cnt == 11:
        #         break
        # # table_2 = table_2[1:]
        # table_2.insert(0, ['Country', 'Org/ISP', 'Total Resolvers', 'Exit nodes inhibiting Correct behaviour',
        #                    'Percentage of Correct Resolvers'])
        #
        # title = "Top 20 TTL honoring ISPs, TTL {}".format(ttl)
        # print(title)
        # #draw_table(table_2[1:], table_2[0], title)
        # print(tabulate(table_2, headers='firstrow', tablefmt='fancy_grid'))

# table_maker_pro()

def get_perc(arr):
    tot = len(arr)
    h, d = 0, 0
    for e in arr:
        if e == 0:
            h += 1
        elif e == 1:
            d += 1
    return h/tot, d/tot


# middle_req_rank_plot()


def cdf_maker_all(master_dict, thresh_hold, loc_pub_key, txt_info):
    import seaborn as sns
    sns.set()
    # fig, axs = plt.subplots(figsize=(20, 15))
    # No of data points used
    # master_dict[ttl][th][loc_pub_key] = ans.
    title = "{} Resolvers: {} Exitnode Threshold".format(loc_pub_key, thresh_hold)

    fig, ax = plt.subplots(figsize=(10, 7))

    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"
    plt.xlabel("Percentage of incorrect exit-nodes")
    plt.ylabel("CDF")

    for ttl in master_dict:
        if ttl not in txt_info:
            # ttl -> loc/all/pub -> thresh -> {}
            txt_info[ttl] = defaultdict(lambda: defaultdict(lambda: dict()))
        arr = master_dict[ttl][thresh_hold][loc_pub_key]

        honor_perc, dis_honor_perc = get_perc(arr)
        txt_info[ttl][loc_pub_key][thresh_hold]["honor_perc"] = honor_perc
        txt_info[ttl][loc_pub_key][thresh_hold]["dis_honor_perc"] = dis_honor_perc
        N = len(arr)
        data = np.array(arr)
        x = np.sort(data)
        y = np.arange(N) / float(N)
        plt.title(title)
        plt.plot(x, y, marker='.', lw=.3, label='TTL {} minutes'.format(ttl), alpha=0.5)
        # plt.show()
    plt.legend(
               prop={'size': 20},
               loc='lower right', shadow=True,
        markerscale=6
               )
    dump_directory = "graphs_final_7/cdfs/"
    Path(dump_directory).mkdir(parents=True, exist_ok=True)
    plt.savefig('graphs_final_3/cdfs/{}.svg'.format(title), bbox_inches="tight", format='svg')
    plt.clf()

def give_len_tuple(arr):
    z, o = 0, 0
    for e in arr:
        if e >= .8:
            o += 1
        elif e == 0:
            z += 1
    return z, o

def cdf_united():
    # ratio_cdf_dict[ttl][k][t_h]["ans"] = []
    f = open("../graph_data_pre_4/ratio_cdf_dict.json")

    d = json.load(f)

    txt_info = {}

    master_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list()))))

    loc_pub_key_set = set()
    for ttl in allowed_ttl:
        for loc_pub_key in d[ttl]:
            loc_pub_key_set.add(loc_pub_key)
            for th in ["5", "10"]:
                ans = d[ttl][loc_pub_key][th]["ans"]
                master_dict[ttl][th][loc_pub_key] = ans

    import seaborn as sns
    sns.set()
    plt.rcdefaults()
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "serif",
        "font.serif": ["Computer Modern Roman"],
        "font.size": 14,
    })
    fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(20, 5))
    thresh_hold = "10"
    col = -1
    for loc_pub_key in ["all", "public", "local"]:
        col += 1

        axes[col].set_title("{}{} Resolvers".format(loc_pub_key[0].upper(), loc_pub_key[1:]), fontsize=14)
        for ttl in master_dict:
            if ttl not in txt_info:
                # ttl -> loc/all/pub -> thresh -> {}
                txt_info[ttl] = defaultdict(lambda: defaultdict(lambda: dict()))
            arr = master_dict[ttl][thresh_hold][loc_pub_key]

            honor_perc, dis_honor_perc = get_perc(arr)
            txt_info[ttl][loc_pub_key][thresh_hold]["honor_perc"] = honor_perc
            txt_info[ttl][loc_pub_key][thresh_hold]["dis_honor_perc"] = dis_honor_perc
            N = len(arr)
            data = np.array(arr)
            x = np.sort(data)
            x_temp = list(x)
            y = np.arange(N) / float(N)
            y_temp = list(y)

            zero_elements, one_elements = give_len_tuple(x_temp)
            print("Dishonor perc {} {}:  {} %".format(ttl, loc_pub_key, (one_elements/len(x_temp)) * 100))

            import csv
            new_list = zip(x_temp, y_temp)
            with open("paper_cdf_files/{}-{}.csv".format(loc_pub_key, ttl), 'w') as csvfile:
                filewriter = csv.writer(csvfile)
                filewriter.writerows(new_list)

            axes[col].plot(x, y, marker='.', lw=.3, label='TTL {} minutes'.format(ttl), alpha=0.5)
            axes[col].set_xlabel("Percentage of incorrect exit-nodes", fontsize=14)
            axes[col].set_ylabel("CDF", fontsize=14)
            # plt.show()
        axes[col].legend(
            prop={'size': 14},
            loc='lower right', shadow=True,
            markerscale=3
        )

    plt.show(bbox_inches="tight")
    # dump_directory = "graphs_final_7/cdfs/"
    # Path(dump_directory).mkdir(parents=True, exist_ok=True)
    # plt.savefig('tst.eps', bbox_inches="tight", format="eps", dpi=1200, transparent=True)
    # plt.clf()

# cdf_united()

def middle_public_explainer():
    f = open("../ttl_result/asn_middle_distro.json")
    d = json.load(f)
    a = [e[1] for e in d if e[1] <= 10 and .2 <= e[0] <= .4]
    cdf_maker(a, "x", "x", "x", None)


# middle_public_explainer()


def recur_summary():
    f = open("../ttl_result/recur_summary.json")
    d = json.load(f)
    total_resolvers = len(list(d.keys()))
    candidate_resolvers = 0
    candidate_resolvers_Set = set()
    candidate_resolvers_ans = []
    ten_exitnode_candidates = []
    ct = 0
    for k in d:
        e = d[k]
        if e["t"] > 10:
            candidate_resolvers_Set.add(k)
            candidate_resolvers += 1
            candidate_resolvers_ans.append(e["recur"]/e["t"])
            if e["recur"]/e["t"] > 0:
                ct += 1
        if e['recur'] >= 10:
            ten_exitnode_candidates.append(k)

    candidate_resolvers_ans.sort(reverse=True)

    cdf_maker(candidate_resolvers_ans, "proactive caching exitnode ratio", "CDF", ".", None)

    a = 1


# recur_summary()

def cdf_united_complex():
    # v11 for only a/(a/b + a)
    # f = open("../ttl_result/complex_data_pre_v10/ratio_cdf_dict_2.json")
    #  a/(a/b + a + c)
    f = open("../ttl_result/complex_data_pre_v12/ratio_cdf_dict.json")
    d = json.load(f)

    txt_info = {}

    master_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list()))))

    loc_pub_key_set = set()

    for ttl in [ "55"]:
        for loc_pub_key in d[ttl]:
            loc_pub_key_set.add(loc_pub_key)
            for th in ["3", "5", "10"]:
                # ans_proactive ans_reduce
                ans = d[ttl][loc_pub_key][th]["ans_reduce"]
                master_dict[ttl][th][loc_pub_key] = ans

    import seaborn as sns
    sns.set()
    plt.rcdefaults()
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "serif",
        "font.serif": ["Computer Modern Roman"],
        "font.size": 14,
    })

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 5))
    thresh_hold = "10"
    col = -1

    for loc_pub_key in ["all"]:
        col += 1
        #axes.set_title("{}{} Resolvers".format(loc_pub_key[0].upper(), loc_pub_key[1:]), fontsize=14)
        for ttl in master_dict:
            print("Key {}".format(loc_pub_key))
            print("Threshold {}".format(ttl))
            if ttl not in txt_info:
                # ttl -> loc/all/pub -> thresh -> {}
                txt_info[ttl] = defaultdict(lambda: defaultdict(lambda: dict()))
            arr = master_dict[ttl][thresh_hold][loc_pub_key]
            zero_elements, one_elements = give_len_tuple(arr)

            print("Candidates length {}".format(len(arr)))
            print("{} reducing, {} caching".format(zero_elements, one_elements))

            honor_perc, dis_honor_perc = get_perc(arr)
            txt_info[ttl][loc_pub_key][thresh_hold]["honor_perc"] = honor_perc
            txt_info[ttl][loc_pub_key][thresh_hold]["dis_honor_perc"] = dis_honor_perc
            N = len(arr)
            data = np.array(arr)
            x = np.sort(data)
            y = np.arange(N) / float(N)
            x_temp = list(x)
            y_temp = list(y)
            import csv
            new_list = zip(x_temp, y_temp)
            p = {
                "43": "30\% Window",
                "49": "20\% Window",
                "55": "10\% Window",
                "58": "5\% Window"
            }
            with open("paper_cdf_files/{}-window.csv".format(p[ttl][:2]), 'w') as csvfile:
                filewriter = csv.writer(csvfile)
                filewriter.writerows(new_list)

            # plt.title(title)

            axes.plot(x, y, marker='.', lw=.3, label=p[ttl], alpha=0.5)
            axes.set_xlabel("Percentage of client triggered proactive caching exit-nodes", fontsize=14)
            axes.set_ylabel("CDF", fontsize=14)
            # plt.show()
        axes.legend(
            prop={'size': 14},
            loc='lower right', shadow=True,
            markerscale=3
        )

    plt.show(bbox_inches="tight")
    # plt.savefig('complex_duo.png', bbox_inches="tight", dpi=1200)

# cdf_united_complex()


def cdf_maker_pro_all():
    # ratio_cdf_dict[ttl][k][t_h]["ans"] = []
    f = open("../graph_data_pre_4/ratio_cdf_dict.json")
    d = json.load(f)

    txt_info = {}

    master_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list()))))

    loc_pub_key_set = set()
    for ttl in allowed_ttl:
        for loc_pub_key in d[ttl]:
            loc_pub_key_set.add(loc_pub_key)
            for th in ["5", "10"]:
                ans = d[ttl][loc_pub_key][th]["ans"]
                master_dict[ttl][th][loc_pub_key] = ans

    for th in ["5", "10"]:
        for loc_pub_key in loc_pub_key_set:
            cdf_maker_all(master_dict, th, loc_pub_key, txt_info)

    # dump_directory = "graphs_final_3/"
    # with open(dump_directory + "meta_data.json", "w") as ouf:
    #     json.dump(txt_info, fp=ouf, indent=2)

# cdf_maker_pro_all()


def cdf_maker_pro():
    # ratio_cdf_dict[ttl][k][t_h]["ans"] = []
    f = open("../graph_data_pre_4/ratio_cdf_dict.json")
    d = json.load(f)

    for ttl in allowed_ttl:
        for loc_pub_key in d[ttl]:
            for th in ["5", "10"]:
                ans = d[ttl][loc_pub_key][th]["ans"]
                inc_res_set = d[ttl][loc_pub_key][th]["inc_res_set"]
                cor_res_set = d[ttl][loc_pub_key][th]["cor_res_set"]

                abs_correct_count = len(cor_res_set)
                abs_incorrect_count = len(inc_res_set)

                if loc_pub_key == "all":
                    header = "CDF: TTL Dishonoring Resolvers - TTL {} minutes - exhibiting atleast {} exitnodes".format(ttl, th)
                    txt_inside = "{} Total Resolvers exhibiting atleast {} exitnodes \n" \
                                 "{} TTL Honoring resolvers ({} %) \n" \
                                 "{} TTL Dishonoring resolvers ({} %)".format(len(ans), th,
                                                                              abs_correct_count,
                                                                              (abs_correct_count / len(ans)) * 100,
                                                                              abs_incorrect_count,
                                                                              (abs_incorrect_count / len(ans)) * 100)
                elif loc_pub_key == "local":
                    header = "CDF: TTL Dishonoring Local Resolvers - TTL {} minutes - exhibiting atleast {} exitnodes".format(ttl, th)
                    txt_inside = "{} Total Local Resolvers exhibiting atleast {} exitnodes \n" \
                                 "{} TTL Honoring resolvers ({} %) \n" \
                                 "{} TTL Dishonoring resolvers ({} %)".format(len(ans), th,
                                                                              abs_correct_count,
                                                                              (abs_correct_count / len(ans)) * 100,
                                                                              abs_incorrect_count,
                                                                              (abs_incorrect_count / len(ans)) * 100)
                else:
                    header = "CDF: TTL Dishonoring Public Resolvers - TTL {} minutes  - exhibiting atleast {} exitnodes".format(ttl, th)
                    txt_inside = "{} Total Public Resolvers exhibiting atleast {} exitnodes \n" \
                                 "{} TTL Honoring resolvers ({} %) \n" \
                                 "{} TTL Dishonoring resolvers ({} %)".format(len(ans), th,
                                                                              abs_correct_count,
                                                                              (abs_correct_count / len(ans)) * 100,
                                                                              abs_incorrect_count,
                                                                              (abs_incorrect_count / len(ans)) * 100)

                cdf_maker(ans, "Percentage of incorrect exit-nodes", "CDF", header, txt_inside)


def cdf_histogram():
    ttl_dishon = {
        1: 9.19,
        5: 5.53,
        15: 3.13,
        30: 2.755,
        60: 1.2
    }

    ttl_hon = {
        1: 64.2,
        5: 61.2,
        15: 59.2,
        30: 62.9,
        60: 73.3
    }

    histogram_maker_dishonring_resolver(ttl_dishon, "TTL", "% of TTL dishonoring resolvers", "Histogram: TTL dishonoring resolver percentage")




# cdf_histogram()

def draw_venn():
    plt.figure(figsize=(10, 10))

    f = open("../graph_data_pre_2/venn_info.json")
    d = json.load(f)

    corr_dict = {}
    in_corr_dict = {}

    for ttl in allowed_ttl:
        inc_set = d[ttl]["ttl_dis_honor"]
        cor_set = d[ttl]["ttl_honor"]
        corr_dict[ttl] = cor_set
        in_corr_dict[ttl] = inc_set



    for mode in [1, 2]:
        sets = []
        title = None
        for ttl in allowed_ttl:
            if mode == 1:
                sets.append(set(corr_dict[ttl]))
                title = "TTL honoring resolvers"
            else:
                sets.append(set(in_corr_dict[ttl]))
                title = "TTL dishonoring resolvers"

        venn3(sets, allowed_ttl_info)
        title = "Venn diagram: {}".format(title)
        plt.title(title)
        dump_directory = "graphs_fresh_2/venn/"
        Path(dump_directory).mkdir(parents=True, exist_ok=True)
        plt.savefig('{}{}.png'.format(dump_directory, title), bbox_inches="tight")
        plt.clf()

#draw_venn()

def draw_venn_common():
    from venn import venn
    plt.figure(figsize=(10, 10))

    f = open("../graph_data_pre_4/venn_info.json")
    d = json.load(f)

    corr_dict = {}
    in_corr_dict = {}

    mother_set = set()

    for ttl in allowed_ttl:
        inc_set = d[ttl]["ttl_dis_honor"]
        cor_set = d[ttl]["ttl_honor"]

        both_set = set(inc_set).union(set(cor_set))
        if len(mother_set) == 0:
            mother_set = both_set
        else:
            mother_set = mother_set.intersection(both_set)

        corr_dict[ttl] = cor_set
        in_corr_dict[ttl] = inc_set

    print(len(mother_set))

    for mode in [1, 2]:
        sets = []
        title = None
        for ttl in allowed_ttl:
            if mode == 1:
                sets.append(set(corr_dict[ttl]).intersection(mother_set))
                title = "TTL honoring resolvers"
            else:
                sets.append(set(in_corr_dict[ttl]).intersection(mother_set))
                title = "TTL dishonoring resolvers"

        p = {}
        index = 0
        for ttl in allowed_ttl:
            p["TTL: {} minutes".format(ttl)] = sets[index]
            index += 1

        # fig, ax = plt.subplots(1, figsize=(16, 12))
        venn(p)
        title = "Venn diagram (only common set): {}".format(title)
        plt.title(title)
        # plt.legend(labels[:-2], ncol=6)
        dump_directory = "graphs_final_3/venn/"
        Path(dump_directory).mkdir(parents=True, exist_ok=True)
        plt.savefig('{}{}.png'.format(dump_directory, title), bbox_inches="tight")
        plt.clf()

# draw_venn_common()


def analyze_http_middle_boxes():
    f = open("../graph_data_pre_2/http_response_info.json")
    d = json.load(f)
    incorrect_Set = set(d['i_c_s'])
    correct_Set = set(d['c_s'])
    common = incorrect_Set.intersection(correct_Set)
    a = []
    for element in incorrect_Set:
        country = get_cn(element)
        a.append((element, country))

    c = defaultdict(lambda : 0)
    for e in a:
        c[e[1]] += 1
    ans = []
    for key in c:
        ans.append((key, c[key]))
    ans.sort(key=lambda x: x[1], reverse=True)
    a = 1
    # 211 incorrect set
    # 208 ***

# analyze_http_middle_boxes()


def cdf_starter(only_local=-1):
    f = open(source_dir + "{}/all_resolvers_pool.json".format(ttl))
    resolver_pool = json.load(f)
    distinct_hit = len(resolver_pool)
    print("Distinct resolvers", distinct_hit)
    #
    f = open(source_dir + "{}/resolver_public_local_dict.json".format(ttl))
    is_public = json.load(f)
    #is_public = {}
    f = open(source_dir + "{}/final_data.json".format(ttl))
    d = json.load(f)

    f = open(source_dir + "{}/final_resolver_to_asn.json".format(ttl))
    f_r = json.load(f)
    resolver_to_asn = f_r['resolver_to_asn_own']

    final_dict = d["data_elaborate"]
    # print(d["Total_resolvers"])
    ans = []
    # cnt, tot = 0, 0
    tt = 0
    ex_node_set = set()
    abs_correct_count, abs_incorrect_count = 0, 0

    abs_correct_set = set()
    abs_incorrect_set = set()

    for key in final_dict:
        try:
            if key not in resolver_to_asn:
                a = 1
            if int(resolver_to_asn[key]) in lum_resolvers_asn:
                continue
        except:
            pass

        if only_local != -1:
            if key not in is_public:
                continue
            if is_public[key] is only_local:
                continue

        correct_set = set()
        incorrect_set = set()
        for e in final_dict[key]["ic"]:
            incorrect_set.add(e[1])
            ex_node_set.add(e[1])
        for e in final_dict[key]["c"]:
            correct_set.add(e[1])
            ex_node_set.add(e[1])

        subtotal = correct_set.union(incorrect_set)
        total = len(subtotal)
        tt += 1

        if total < min_exitnodes:
            continue

        ratio = len(incorrect_set) / total
        if ratio >= 1:
            abs_incorrect_count += 1
            abs_incorrect_set.add(key)
        elif ratio <= 0:
            abs_correct_set.add(key)
            abs_correct_count += 1
        ans.append(ratio)

    print("ttl ", ttl)
    print("resolvers", tt, len(ans))
    print("total exitnodes", len(ex_node_set))

    ttl_to_sets[ttl]["abs_correct_set"] = abs_correct_set
    ttl_to_sets[ttl]["abs_incorrect_set"] = abs_incorrect_set
    header = None
    txt_inside = None
    if only_local == -1:
        header = "CDF: TTL Dishonoring Resolvers - TTL {}".format(ttl)
        txt_inside = "{} Total Resolvers exhibiting atleast {} exitnodes \n" \
                     "{} TTL Honoring resolvers ({} %) \n" \
                     "{} TTL Dishonoring resolvers ({} %)".format(len(ans), min_exitnodes, abs_correct_count, (abs_correct_count/len(ans)) * 100, abs_incorrect_count, (abs_incorrect_count/len(ans)) * 100)
    elif only_local:
        header = "CDF: TTL Dishonoring Local Resolvers - TTL {} minutes".format(ttl)
        txt_inside = "{} Local Resolvers exhibiting atleast {} exitnodes".format(len(ans), min_exitnodes)
    else:
        header = "CDF: TTL Dishonoring Public Resolvers - TTL {}".format(ttl)
        txt_inside = "{} Public Resolvers exhibiting atleast {} exitnodes".format(len(ans), min_exitnodes)

    cdf_maker(ans, "Percentage of incorrect exit-nodes", "CDF", header, txt_inside)


def proactive_gap_cdf():
    f = open("/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/complex_data_pre_v10/server_dns_http_time_list.json")
    d = json.load(f)
    gap = []
    for e in d:
        t1, t2 = e[2], e[1]
        if "N/A" not in [t1, t2]:
            # gap.append(t1/1000 + 4 * 60 * 60 - t2)
            gap.append((t1 - t2)/ 10)
    a = 1
    cdf_maker(gap, "tst", "asd", "asdsa", None)

# proactive_gap_cdf()


def http_parser():
    try:
        f = open("../ttl_result/{}/http_response_dict.json".format(ttl))
        d = json.load(f)

        a = []
        total = 0
        for e in d:
            total += d[e]
            a.append((d[e], e))
        a.sort(reverse=True)
        a = 1
    except Exception as e:
        pass

# http_parser()
#jaccard_cdf()
# TODO histogram !!
# reolver_hits_per_req()

# ttl = 15
# http_parser()

def cmp():
    import json
    f = open("../ttl_result/global_asn_list.json")
    old = json.load(f)

    f = open("../ttl_result/global_asn_list_updated.json")
    new = json.load(f)

    print(len(old))
    print(len(new))
    a = 0

    for e in old:
        if e not in new:
            print(e)
            a += 1

    print(a)


def middle_requests(ttl):
    f = open("../ttl_result/{}/resolver_to_middle_req.json".format(ttl))
    d = json.load(f)

    f = open("../ttl_result/{}/resolver_to_last_req.json".format(ttl))
    last_req_json = json.load(f)

    print(len(d.keys()))
    final_candidates = {}

    for e in d:
        if len(list(d[e].keys())) >= 1:
            final_candidates[e] = d[e]
    print(len(final_candidates.keys()))

    max_dict = defaultdict(lambda: 0)
    median_dict = defaultdict(lambda: 0)


    diff_list = []

    resolver_to_instances = defaultdict(lambda: dict())

    resolver_to_req_to_seq = defaultdict(lambda: defaultdict(lambda : list()))

    for resolver in final_candidates:
        lens = []

        for req_id in final_candidates[resolver]:
            to_be_comapred = 1231231231649125509
            final_candidates[resolver][req_id].sort()
            if resolver in last_req_json and req_id in last_req_json[resolver]:
                to_be_comapred = last_req_json[resolver][req_id]
                diff_in_seconds = final_candidates[resolver][req_id][0] - last_req_json[resolver][req_id]
                if diff_in_seconds <= 60 * ttl + 120:
                    diff_list.append(diff_in_seconds)


            for index in range(len(final_candidates[resolver][req_id])):
                to_be_comapred = min(to_be_comapred, final_candidates[resolver][req_id][0])
                diff_in_seconds = final_candidates[resolver][req_id][index] - to_be_comapred
                if diff_in_seconds <= 10:
                    continue
                lens.append(len(final_candidates[resolver][req_id]) - index)
                resolver_to_instances[resolver][req_id] = final_candidates[resolver][req_id][index: ]


                pre = to_be_comapred
                if len(final_candidates[resolver][req_id]) - index > 1:
                    temp = []
                    for id in range(0, len(resolver_to_instances[resolver][req_id])):
                        temp.append(resolver_to_instances[resolver][req_id][id] - min(pre, resolver_to_instances[resolver][req_id][id]))
                        pre = resolver_to_instances[resolver][req_id][id]

                    resolver_to_req_to_seq[resolver][req_id].append(temp)
                break

        if len(lens) > 0:
            import statistics
            max_dict[resolver] = max(lens)
            median_dict[resolver] = statistics.median(lens)

    med_list, max_list = [], []
    med_list_counter = defaultdict(lambda: 0)
    max_list_counter = defaultdict(lambda: 0)

    for key in median_dict:
        med_list.append(median_dict[key])
        med_list_counter[median_dict[key]] += 1

    for key in max_dict:
        max_list.append(max_dict[key])
        max_list_counter[max_dict[key]] += 1




    # cdf_maker(med_list, "Median number of retries", "CDF", "CDF: Median number of retries, TTL {}".format(ttl), None)
    # cdf_maker(max_list, "Max number of retries", "CDF", "CDF: Max number of retries, TTL {}".format(ttl), None)
    cdf_maker(diff_list, "Time Gap for the first proactive request to come", "CDF", "CDF: Time Gap for the first proactive request to come, TTL {}".format(ttl), None)
    histogram_maker(max_list_counter, "max retries", "count", "Histogram of max retires")
    histogram_maker(med_list_counter, "median retries", "count", "Histogram of median retires")


    arr = []
    resolver_to_asn_dict = get_resolver_to_asn_dict()
    for resolver in resolver_to_instances:
        req_ids_seen = len(resolver_to_instances[resolver].keys())
        arr.append((resolver_to_asn_dict[resolver], get_org(resolver_to_asn_dict[resolver]), req_ids_seen))

    #arr.sort(key=lambda x:x[2], reverse=True)
    org_counter = defaultdict(lambda: 0)
    for e in arr:
        org_counter[e[1]] += e[2]
    arr = []
    for key in org_counter:
        arr.append((key, org_counter[key]))

    arr.sort(key=lambda x: x[1], reverse=True)


    p = {}
    p["org_set"] = arr
    p["recurring_set"] = resolver_to_req_to_seq

    with open("middle_info_extend-{}.json".format(ttl), "w") as ouf:
        json.dump(p, fp=ouf, indent=2)


def is_linear(lst):
    for index in range(1, len(lst)):
        if abs(lst[index] - lst[index - 1]) > 6:
            return False
    return True


def is_increasing(lst):
    for index in range(1, len(lst)):
        if lst[index] - lst[index - 1] < 12:
            return False
    return True


def is_decreasing(lst):
    for index in range(1, len(lst)):
        if lst[index] - lst[index - 1] > -12:
            return False
    return True


def define_temp_list(lst):
    if is_linear(lst):
        return "regular"
    if is_increasing(lst):
        return "increasing"
    if is_decreasing(lst):
        return "decreasing"
    else:
        return "random"


def recurring_set_plot(ttl):
    f = open("middle_info_extend-{}.json".format(ttl))
    recurring_set = json.load(f)["recurring_set"]

    # f = open("middle_info_extend-15.json")
    # recurring_set_15 = json.load(f)["recurring_set"]



    label_list = defaultdict(lambda : list())
    for resolver in recurring_set:
        for req_id in recurring_set[resolver]:
            for lo in recurring_set[resolver][req_id]:
                lst = lo
                temp_lst = []
                x_list = []

                index = 0
                for e in lst:
                    if e <= 5:
                        continue
                    index += 1
                    x_list.append(index)
                    temp_lst.append(e)
                label = define_temp_list(temp_lst)
                label_list[label].append(temp_lst)
                #axs.plot(x_list, temp_lst, marker='.')

    for label in label_list:
        if label == "regular":
            fig, axs = plt.subplots(1, 1, figsize=(10, 5))
        else:
            fig, axs = plt.subplots(1, 1, figsize=(10, 10))
        for lst in label_list[label]:
            length = len(lst)
            if length == 1:
                continue
            x = []
            index = 0
            for i in range(length):
                index += 1
                x.append(index)
            axs.plot(x, lst, marker='.')
        axs.set_title("DNS request intervals - {}".format(label))
        plt.savefig('ttl_result/{}-{}-{}.png'.format("dns_request_interval", ttl, label), bbox_inches="tight")
        plt.clf()


# recurring_set_plot(15)


# middle_requests(5)
# middle_requests(15)

# cmp()


# for temp in [5, 15]:
#     ttl = temp
#     for e in [5, 10]:
#         min_exitnodes = e
#         for mode in [-1, True, False]:
#             cdf_info(only_local=mode)

