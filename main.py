import json
import ujson

import plotly.express as px
import numpy as np

alpha2_to_3_map = {}
alpha3_to_2_map = {}
f3 = open("alpha3.json")
f2 = open("alpha2.json")

d2 = json.load(f2)
d3 = json.load(f3)

# "alpha-2":"BD","country-code":"050"

for e in d2:
    c_code = e['country-code']
    a_2_code = e['alpha-2']
    for nested_e in d3:
        if nested_e['country-code'] == c_code:
            alpha2_to_3_map[a_2_code] = nested_e['alpha-3']
            alpha3_to_2_map[nested_e['alpha-3']] = a_2_code


def draw_map(country_code_to_count_map, mono=False) :
    np.random.seed(12)
    gapminder = px.data.gapminder().query("year==2007")


    #count_list = np.random.uniform(low=0, high=200, size=len(gapminder)).tolist()
    count_list = []
    country_code_list = list(gapminder.iso_alpha)
    col_lst = []
    for i in range(len(country_code_list)):
        a2_code = alpha3_to_2_map[country_code_list[i]]
        # if a2_code == 'US':
        #     count_list.append(100)
        # else:
        # if country_code_to_count_map[a2_code] > 30:
        #     count_list.append(0)
        #     continue
        if not mono:
            val = country_code_to_count_map[a2_code]
        else:
            val = min(country_code_to_count_map[a2_code], 1)
            if val > 0:
                col_lst.append('r')
            else:
                col_lst.append('b')
        count_list.append(val)

    gapminder['counts'] = count_list

    if not mono:
        fig = px.choropleth(gapminder, locations="iso_alpha",
                            color="counts",
                            hover_name="country",  # column to add to hover information
                            color_continuous_scale=px.colors.diverging.balance, title="TTL dishonoring resolver distribution")
    else:
        fig = px.choropleth(gapminder, locations="iso_alpha",
                            color=col_lst,
                            hover_name="country",  # column to add to hover information
                            title="TTL dishonoring resolver distribution")

    fig.show()
    fig.write_image("yourfile.png")


def draw_all_over():
    from collections import defaultdict
    f4 = open("resolver_to_org_country.json")
    resolver_to_org_country = json.load(f4)
    country_code_to_count_map = defaultdict(lambda: 0)
    for resolver in resolver_to_org_country:
        country_code_to_count_map[resolver_to_org_country[resolver][1]] += 1
    draw_map(country_code_to_count_map)


def draw_misconfigured_resolvers():
    from collections import defaultdict
    f4 = open("resolver_to_org_country.json")
    resolver_to_org_country = json.load(f4)
    country_code_to_count_map = defaultdict(lambda: 0)

    f = open("resolver_public_local_dict.json")
    is_public = json.load(f)
    f = open("final_data.json")
    d = json.load(f)

    final_dict = d["data_elaborate"]
    ans = []
    # cnt, tot = 0, 0
    for key in final_dict:
        correct_set = set()
        incorrect_set = set()
        for e in final_dict[key]["ic"]:
            incorrect_set.add(e[1])
        for e in final_dict[key]["c"]:
            correct_set.add(e[1])

        subtotal = correct_set.union(incorrect_set)
        # total = len(correct_set) + len(incorrect_set)
        total = len(subtotal)

        if total < 5:
            continue

        ratio = len(incorrect_set) / total
        if ratio >= .8:
            country_code_to_count_map[resolver_to_org_country[key][1]] += 1

    draw_map(country_code_to_count_map, mono=True)


draw_misconfigured_resolvers()