
import json, random

f = open("AS_INFO.json")
all_asns = json.load(f)
all_asns = list(all_asns.keys())
all_asns = [e[2:] for e in all_asns]

random.shuffle(all_asns)

f = open("successful_asns.json")
successful_asns = json.load(f)

random.shuffle(successful_asns)

final = successful_asns[0: 16000] + all_asns[0: 2000]
final.sort(key=lambda x: int(x))

with open("available_asns_jul_21.json", "w") as ouf:
    json.dump(final, fp=ouf)



