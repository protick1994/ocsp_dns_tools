import json


# main_file_base_directory = "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/new_ttl/results_new_exp_1/"
#
# bind_file_directory = "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/new_ttl/dns_ttl_new/bind"

main_file_base_directory = "/home/protick/node_code/results_new_exp_xxx/"

bind_file_directory = "/net/data/dns-ttl/dns_ttl_new_xxx/bind"

allowed_ttl = [1, 5, 15, 30, 60]

'''
Total unique resolvers, exitnodes and ASNs:
Without lum resolvers: koyta

How many used by exitnodes??

How many of them are honoring, how many of them are dishonoring??

Honoring er exitnode
Dishonoring er exitnode

Basically: resolver to exitnode (H, D)
'''

def get_leaf_files(path):
    import os
    list_of_files = []

    for root, dirs, files in os.walk(path):
        for file in files:
            list_of_files.append(os.path.join(root, file))
    return list_of_files


ip_to_index = {
    "3.223.194.233": 7,
    "34.226.99.56": 5,
    "52.44.221.99": 8,
    "52.71.44.50": 6,
    "18.207.47.246": 2,
    "3.208.196.0": 3,
    "44.195.175.12": 4,
    "50.16.6.90": 1,
}

from collections import defaultdict
dishonor_lst = []
honor_lst = []
req_dict = {}
response_to_ip = {}
req_uid_to_phase_1_resolver_tuple = {}
req_uid_to_phase_2_resolver_lst = defaultdict(lambda : list())
req_uid_to_asn = {}
req_uid_to_hash = {}

all_resolver_set = set()
all_exitnode_set = set()
all_asn_set = set()
all_lum_resolver_set = set()

found_set = set()
not_found_set = set()


def reset():
    global dishonor_lst
    global honor_lst
    global req_dict
    global response_to_ip
    global req_uid_to_phase_1_resolver_tuple
    global req_uid_to_phase_2_resolver_lst
    global req_uid_to_asn
    global req_uid_to_hash
    global all_resolver_set
    global all_exitnode_set
    global all_asn_set
    global all_lum_resolver_set
    global found_set
    global not_found_set

    dishonor_lst = []
    honor_lst = []
    req_dict = {}
    response_to_ip = {}
    req_uid_to_phase_1_resolver_tuple = {}
    req_uid_to_phase_2_resolver_lst = defaultdict(lambda: list())
    req_uid_to_asn = {}
    req_uid_to_hash = {}

    all_resolver_set = set()
    all_exitnode_set = set()
    all_asn_set = set()
    all_lum_resolver_set = set()

    found_set = set()
    not_found_set = set()


global_resolver_set = set()

def analyze_bind(ttl):
    for key in ip_to_index:
        response_to_ip["phase{}".format(ip_to_index[key])] = key
    global global_resolver_set
    resolver_set = set()

    leaf_files_unfiltered = get_leaf_files(bind_file_directory.replace("xxx", ttl))
    # leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    print(len(leaf_files_unfiltered))
    for file in leaf_files_unfiltered:
        with open(file) as FileObj:
            for line in FileObj:
                try:
                    non_decidings_ips = ['52.4.120.223']

                    if "zeus_reload" not in line:
                        continue

                    segments = line.strip().split()

                    resolver_ip, timestamp, allotted_ip, req_url = segments[1], segments[2], segments[3], segments[4]
                    resolver_set.add(resolver_ip)
                    global_resolver_set.add(resolver_ip)
                    if "good" not in line:
                        continue
                    # good 198.82.247.98 1660802862.1054115 50.16.6.90 f08ed8ae-dfb6-4289-930a-b2bbe75362561660802861993.zeus_reload_1_1_1.60.1.1.securekey.app. A


                    req_uid = req_url.split(".")[0]

                    try:
                        all_resolver_set.add(resolver_ip)
                        all_asn_set.add(req_uid_to_asn[req_uid])
                        all_exitnode_set.add(req_uid_to_hash[req_uid])
                    except:
                        pass

                    if allotted_ip in non_decidings_ips:
                        all_lum_resolver_set.add(resolver_ip)
                        continue

                    response = req_dict[req_uid]
                    real_allotted_ip = response_to_ip[response]

                    if allotted_ip == '3.220.52.113':
                        # phase 2
                        req_uid_to_phase_2_resolver_lst[req_uid].append(resolver_ip)
                    else:
                        # phase 1
                        if allotted_ip == real_allotted_ip:
                            if req_uid not in req_uid_to_phase_1_resolver_tuple:
                                req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))
                            else:
                                _, t = req_uid_to_phase_1_resolver_tuple[req_uid]
                                if float(timestamp) < t:
                                    req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))
                    found_set.add(req_uid)
                except Exception as e:
                    # TODO severe inspect
                    if "KeyError" in str(type(e)):
                        not_found_set.add(req_uid)
                    #print('parse bind apache logs ', e)

    return len(resolver_set)

global_exitnode_set = set()

def analyze_main_files(ttl):
    global global_exitnode_set
    exitnode_set = set()

    leaf_files_unfiltered = get_leaf_files(main_file_base_directory.replace("xxx", ttl))
    leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    leaf_files_filtered = [e for e in leaf_files_filtered if ".json" in e]

    exp_id_list = []
    pp = []
    for element in leaf_files_filtered:
        exp_id_list.append(element[: - len("-out.json")])

    err_set = set()

    print(len(exp_id_list))

    for exp_id in exp_id_list:
        level = exp_id.split("_")[-2]
        full_file = "{}{}/{}-out.json".format(main_file_base_directory.replace("xxx", ttl), level, exp_id)
        f = open(full_file)
        d = json.load(f)
        a = 1
        # d['dict_of_phases']['12973']['1-response'].strip()[4: -5]
        weird = 0
        for num in d['dict_of_phases']:

            try:
                response_1 = d['dict_of_phases'][num]['1-response'].strip()[4: -5]
                if "lum" not in response_1:
                    exitnode_set.add(d['dict_of_phases'][num]['ip_hash'])
                    global_exitnode_set.add(d['dict_of_phases'][num]['ip_hash'])

                response_2 = d['dict_of_phases'][num]['2-response'].strip()[4: -5]

                req_url_uid = d['dict_of_phases'][num]['req_url'][7:].split(".")[0]
                req_uid_to_asn[req_url_uid] = d['dict_of_phases'][num]['asn']
                req_uid_to_hash[req_url_uid] = d['dict_of_phases'][num]['ip_hash']



                save_tuple = (req_url_uid, d['dict_of_phases'][num]['asn'],
                              d['dict_of_phases'][num]['ip_hash'], response_1, response_2)

                req_dict[req_url_uid] = response_1



                if "phasex" not in response_2:
                    if response_1 != response_2:
                        weird += 1
                    else:
                        # url, asn, ip_hash, r1, r2
                        dishonor_lst.append(save_tuple)
                else:
                    honor_lst.append(save_tuple)

            except Exception as e:
                err_set.add(str(e))

    return len(exitnode_set)


def init(ttl):
    ttl = str(ttl)
    reset()
    tot_exitnode = analyze_main_files(ttl=ttl)
    tot_resolvers = analyze_bind(ttl=ttl)

    print("{} {} {}".format(ttl, tot_exitnode, tot_resolvers))


    # dishonor_lst = []
    # honor_lst = []
    # req_id, asn, ip_hash, r1, r2
    # req_dict = {}
    # response_to_ip = {}
    # req_uid_to_phase_1_resolver_tuple = {}
    # req_uid_to_phase_2_resolver_lst = defaultdict(lambda: list())

    resolver_ip_to_verdict_list = defaultdict(lambda: {"g": set(), "b": set()})

    for e in dishonor_lst:
        try:
            req_uid, asn, ip_hash, r1, r2 = e
            culprit_resolver = req_uid_to_phase_1_resolver_tuple[req_uid][0]
            if culprit_resolver not in req_uid_to_phase_2_resolver_lst[req_uid]:
                resolver_ip_to_verdict_list[culprit_resolver]["b"].add(ip_hash)
        except:
            pass

    for e in honor_lst:
        try:
            req_uid, asn, ip_hash, r1, r2 = e
            good_resolver = req_uid_to_phase_1_resolver_tuple[req_uid][0]
            if ip_hash not in resolver_ip_to_verdict_list[good_resolver]["b"] and \
                    good_resolver in req_uid_to_phase_2_resolver_lst[req_uid]:
                resolver_ip_to_verdict_list[good_resolver]["g"].add(ip_hash)
            elif ip_hash in resolver_ip_to_verdict_list[good_resolver]["b"]:
                resolver_ip_to_verdict_list[good_resolver]["b"].remove(ip_hash)
        except:
            pass

    arr = []
    arr_global = []

    bad_resolvers = []

    for resolver_ip in resolver_ip_to_verdict_list:
        good_len = len(resolver_ip_to_verdict_list[resolver_ip]["g"])
        bad_len = len(resolver_ip_to_verdict_list[resolver_ip]["b"])
        if good_len + bad_len == 0:
            continue
        arr_global.append((resolver_ip, (bad_len/(good_len + bad_len))))
        if bad_len/(good_len + bad_len) == 1:
            bad_resolvers.append(resolver_ip)
        if good_len + bad_len >= 5:
            arr.append((resolver_ip, bad_len/(good_len + bad_len)))

    from pathlib import Path
    dump_directory = "ttl_new_data_{}/".format(ttl)
    Path(dump_directory).mkdir(parents=True, exist_ok=True)

    meta = {
        "all_resolver_set": len(all_resolver_set),
        "all_exitnode_set": len(all_exitnode_set),
        "all_asn_set": len(all_asn_set),
        "all_lum_resolver_set": len(all_lum_resolver_set),
    }

    with open(dump_directory + "meta.json", "w") as ouf:
        json.dump(meta, fp=ouf)
    with open(dump_directory + "yo.json", "w") as ouf:
        json.dump(arr, fp=ouf)
    with open(dump_directory + "bad.json", "w") as ouf:
        json.dump(bad_resolvers, fp=ouf)
    with open(dump_directory + "arr_global.json", "w") as ouf:
        json.dump(arr_global, fp=ouf)


for ttl in allowed_ttl:
    init(ttl=ttl)

print("{} {} {}".format(-1, len(global_exitnode_set), len(global_resolver_set)))
