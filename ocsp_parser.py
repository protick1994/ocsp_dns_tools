
from cryptography.x509 import ocsp
import binascii
import hashlib
import time


from pyasn1_modules import rfc2560
from pyasn1_modules import rfc2459
from pyasn1_modules import pem
from pyasn1.codec.der import decoder
from pyasn1.codec.der import encoder
from pyasn1.type import univ


def return_ocsp_result(ocsp_response, is_bytes=False):
    """ Extract the OCSP result from the provided ocsp_response """
    try:
        if is_bytes:
            ocsp_response = ocsp.load_der_ocsp_response(ocsp_response)
        else:
            ocsp_response = ocsp.load_der_ocsp_response(ocsp_response.content)

        return ocsp_response

    except ValueError as err:
        # print(f"{str(err)}")
        return f"{str(err)}"


with open("/Users/protick.bhowmick/Desktop/untitled1", mode='rb') as file: # b is important -> binary
    fileContent = file.read()
decoded_response = return_ocsp_result(fileContent, is_bytes=True)
a = 1