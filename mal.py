import json
from multiprocessing.dummy import Pool as ThreadPool

# main_file_base_directory = "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/new_ttl/results_new_exp_1/"
#
# bind_file_directory = "/Users/protick.bhowmick/PriyoRepos/dns_test_ground/ttl_result/new_ttl/dns_ttl_new/bind"

# TODO change here
main_file_base_directory = "/home/protick/node_code/new_prefetch_v2_60/55/"

bind_file_directory = "/net/data/dns-ttl/dns_ttl_new/bind"

'''
Total unique resolvers, exitnodes and ASNs:
Without lum resolvers: koyta

How many used by exitnodes??

How many of them are honoring, how many of them are dishonoring??

Honoring er exitnode
Dishonoring er exitnode

Basically: resolver to exitnode (H, D)
'''


def get_leaf_files(path):
    import os
    list_of_files = []

    for root, dirs, files in os.walk(path):
        for file in files:
            list_of_files.append(os.path.join(root, file))
    return list_of_files


ip_to_index = {
    "3.223.194.233": 7,
    "34.226.99.56": 5,
    "52.44.221.99": 8,
    "52.71.44.50": 6,
    "18.207.47.246": 2,
    "3.208.196.0": 3,
    "44.195.175.12": 4,
    "50.16.6.90": 1,
}

from collections import defaultdict


tot_dict = {}

dishonor_lst = []
honor_lst = []
req_dict = {}
response_to_ip = {}
req_uid_to_phase_1_resolver_tuple = {}
req_uid_to_phase_2_resolver_lst = defaultdict(lambda: list())
req_uid_to_asn = {}
req_uid_to_hash = {}
req_uid_to_phase_one_time = {}
req_uid_to_phase_two_time = {}

all_resolver_set = set()
all_exitnode_set = set()
all_asn_set = set()
all_lum_resolver_set = set()

found_set = set()
not_found_set = set()

for key in ip_to_index:
    response_to_ip["phase{}".format(ip_to_index[key])] = key


def store_or_update_event(req_uid, event_str, timestamp, req_uid_to_event_str_to_time):
    # req_uid_to_event_str_to_time = defaultdict(lambda : dict())
    if event_str not in req_uid_to_event_str_to_time[req_uid]:
        req_uid_to_event_str_to_time[req_uid][event_str] = float(timestamp)
    else:
        prev_allotted_time = req_uid_to_event_str_to_time[req_uid][event_str]
        if float(timestamp) < prev_allotted_time:
            req_uid_to_event_str_to_time[req_uid][event_str] = float(timestamp)


req_uid_to_event_str_to_time = defaultdict(lambda: dict())
resolver_to_req_uid_to_event_list = defaultdict(lambda: defaultdict(lambda: list()))


def analyze_bind_leaf(file):
    global req_uid_to_event_str_to_time
    global resolver_to_req_uid_to_event_list
    global response_to_ip
    cmp = 1665000000
    ts = int(file.split(".")[-1])
    ts = ts // 1000
    if ts < cmp:
        return

    with open(file) as FileObj:
        for line in FileObj:
            try:
                non_decidings_ips = ['52.4.120.223']

                if "zeus_reload_pro" not in line:
                    continue
                # good 198.82.247.98 1660802862.1054115 50.16.6.90 f08ed8ae-dfb6-4289-930a-b2bbe75362561660802861993.zeus_reload_1_1_1.60.1.1.securekey.app. A

                segments = line.strip().split()

                if "goodevent" in line:
                    resolver_ip, timestamp, allotted_ip, req_url = segments[1], segments[2], segments[3], segments[
                        4]
                    req_uid = req_url.split(".")[0]
                    event_str = req_url.split(".")[2]
                    store_or_update_event(req_uid, event_str, timestamp, req_uid_to_event_str_to_time)
                elif "good" in line:
                    resolver_ip, timestamp, allotted_ip, req_url = segments[1], segments[2], segments[3], segments[
                        4]
                    req_uid = req_url.split(".")[0]
                    resolver_to_req_uid_to_event_list[resolver_ip][req_uid].append(
                        (float(timestamp), 1, allotted_ip))
                    response = req_dict[req_uid]
                    real_allotted_ip = response_to_ip[response]

                    if allotted_ip in non_decidings_ips:
                        all_lum_resolver_set.add(resolver_ip)
                        continue

                    if allotted_ip == '3.220.52.113':
                        req_uid_to_phase_2_resolver_lst[req_uid].append(resolver_ip)
                    else:
                        if allotted_ip == real_allotted_ip:
                            if req_uid not in req_uid_to_phase_1_resolver_tuple:
                                req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))
                            else:
                                _, t = req_uid_to_phase_1_resolver_tuple[req_uid]
                                if float(timestamp) < t:
                                    req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))
                    found_set.add(req_uid)
                elif "nxmode" in line:
                    resolver_ip, timestamp, req_url = segments[1], segments[2], segments[3]
                    resolver_to_req_uid_to_event_list[resolver_ip][req_uid].append(
                        (float(timestamp), 2, "xxx"))
            except Exception as e:
                # TODO severe inspect
                if "KeyError" in str(type(e)):
                    not_found_set.add(req_uid)
                # print('parse bind apache logs ', e)
    print("Done with {}".format(file))

def analyze_bind(ttl):
    leaf_files_unfiltered = get_leaf_files(bind_file_directory)
    pool = ThreadPool(40)
    results = pool.map(analyze_bind_leaf, leaf_files_unfiltered)
    pool.close()
    pool.join()



def analyze_main_file_leaf(exp_id):
    level = exp_id.split("_")[-2]
    full_file = "{}{}/{}-out.json".format(main_file_base_directory, level, exp_id)
    f = open(full_file)
    d = json.load(f)

    weird = 0
    for num in d['dict_of_phases']:

        try:
            response_1 = d['dict_of_phases'][num]['1-response'].strip()[4: -5]
            if "phase" not in response_1 and len(response_1) > 15:
                continue

            response_2 = d['dict_of_phases'][num]['2-response'].strip()[4: -5]

            req_url_uid = d['dict_of_phases'][num]['req_url'][7:].split(".")[0]
            req_uid_to_asn[req_url_uid] = d['dict_of_phases'][num]['asn']
            req_uid_to_hash[req_url_uid] = d['dict_of_phases'][num]['ip_hash']
            two_time = float(d['dict_of_phases'][num]['2-time']) / 1000
            one_time = float(d['dict_of_phases'][num]['1-time']) / 1000

            req_uid_to_phase_one_time[req_url_uid] = one_time
            req_uid_to_phase_two_time[req_url_uid] = two_time

            save_tuple = (req_url_uid, d['dict_of_phases'][num]['asn'],
                          d['dict_of_phases'][num]['ip_hash'], response_1, response_2,
                          one_time, two_time)

            tot_dict[req_url_uid] = save_tuple
            req_dict[req_url_uid] = response_1

        except Exception as e:
            pass

def analyze_main_files():
    global tot_dict
    leaf_files_unfiltered = get_leaf_files(main_file_base_directory)
    leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    leaf_files_filtered = [e for e in leaf_files_filtered if ".json" in e]

    exp_id_list = []
    for element in leaf_files_filtered:
        exp_id_list.append(element[: - len("-out.json")])

    pool = ThreadPool(40)
    results = pool.map(analyze_main_file_leaf, exp_id_list)
    pool.close()
    pool.join()

    return tot_dict






def get_event_times(req_uid):
    strs = ['event-phase1-start', 'event-phase1-end', 'event-sleep-end', 'event-phase2-end']
    ans = []
    for st in strs:
        ans.append(req_uid_to_event_str_to_time[req_uid][st])
    return ans


def curate_lst_phase_1(lst, allowed_flag):
    # resolver_to_req_uid_to_event_list
    # ((float(timestamp), 1, allotted_ip))
    ans = []
    for e in lst:
        if e[1] == allowed_flag and e[2] != '3.220.52.113':
            ans.append(e)
    return ans

def curate_lst_phase_2(lst, allowed_flag):
    # ((float(timestamp), 1, allotted_ip))
    ans = []
    for e in lst:
        if e[1] == allowed_flag and e[2] == '3.220.52.113':
            ans.append(e)
    return ans


def curate_lst_mid(lst, allowed_flag, t1, t2):
    # ((float(timestamp), 1, allotted_ip))
    ans = []
    for e in lst:
        if t1 <= e[0] <= t2 and e[1] == allowed_flag:
            ans.append(e)
    return ans


def segment(lst, req_uid, one_time, two_time):
    # times = get_event_times(req_uid)
    first_phase = curate_lst_phase_1(lst, 1)
    mid_phase = curate_lst_mid(lst, 2, one_time, two_time)
    second_phase = curate_lst_phase_2(lst, 1)

    curated_second_phase_lenient = []
    curated_second_phase_harsh = []
    curated_second_phase_short = []

    for e in second_phase:
        if two_time - 3 <= e[0] <= two_time + 3:
            curated_second_phase_lenient.append(e)
        if two_time <= e[0] <= two_time + 3:
            curated_second_phase_harsh.append(e)
        if two_time - 5 <= e[0] <= two_time:
            curated_second_phase_short.append(e)

    return first_phase, mid_phase, second_phase, curated_second_phase_lenient, curated_second_phase_harsh, curated_second_phase_short


def init(ttl):
    ttl = str(ttl)

    tot_dict = analyze_main_files()
    print("TOT dict {}".format(len(list(tot_dict.keys()))))


    from pathlib import Path
    # TODO change
    dump_directory = "ttl_new_prefetch_55_v3/"
    Path(dump_directory).mkdir(parents=True, exist_ok=True)

    # things_to_save_1 = {
    #     "tot_dict": tot_dict,
    #     "req_uid_to_asn": req_uid_to_asn,
    #     "req_uid_to_hash": req_uid_to_hash,
    #     "req_uid_to_phase_two_time": req_uid_to_phase_two_time,
    #     "req_dict": req_dict
    # }

    # f = open(dump_directory + "things_to_save_1.json")
    # d = json.load(f)
    # tot_dict = d['tot_dict']
    # req_uid_to_asn = d['req_uid_to_asn']
    # req_uid_to_hash = d['req_uid_to_hash']
    # req_uid_to_phase_two_time = d['req_uid_to_phase_two_time']
    # req_dict = d['req_dict']


    # with open(dump_directory + "things_to_save_1.json", "w") as ouf:
    #     json.dump(things_to_save_1, fp=ouf)

    '''
    Logistics:
    phase 1 e asche -> 1 bari hit
    majhkhane ashe nai -> absent
    phase 2 te asche -> ekbari hit
    '''

    # req_uid_er jonno -> phase 1 er resolver ip ta, phase 2 te o ki asche kina? ar phase 2 te abar hit korse kina ??

    analyze_bind(ttl=ttl)

    # things_to_save_2 = {
    #     "resolver_to_req_uid_to_event_list": resolver_to_req_uid_to_event_list,
    #     "req_uid_to_phase_1_resolver_tuple": req_uid_to_phase_1_resolver_tuple,
    #     "req_uid_to_event_str_to_time": req_uid_to_event_str_to_time
    #
    # }

    # f = open(dump_directory + "things_to_save_2.json")
    # d = json.load(f)
    # resolver_to_req_uid_to_event_list = d['resolver_to_req_uid_to_event_list']
    # req_uid_to_phase_1_resolver_tuple = d['req_uid_to_phase_1_resolver_tuple']


    # with open(dump_directory + "things_to_save_2.json", "w") as ouf:
    #     json.dump(things_to_save_2, fp=ouf)

    for req_uid in resolver_to_req_uid_to_event_list:
        for resolver in resolver_to_req_uid_to_event_list[req_uid]:
            resolver_to_req_uid_to_event_list[req_uid][resolver].sort()

    resolver_ip_to_verdict_list = defaultdict(lambda: {"n": set(), "p": set(), "s": set(), "pl": set(), "ph": set()})

    mom = 0
    for req_uid in tot_dict:
        if req_uid not in req_uid_to_phase_1_resolver_tuple:
            print("chachi {}".format(req_dict[req_uid]))
            continue

        # save_tuple = (req_url_uid, d['dict_of_phases'][num]['asn'], d['dict_of_phases'][num]['ip_hash'], response_1, response_2)
        req_url, asn, ip_hash, response_1, response_2, one_time, two_time = tot_dict[req_uid]

        diff = ((two_time - one_time)/60)
        # TODO change here!!
        if not (diff > 54.1 and diff < 60):
            continue


        '''
            case: normal -> same, but no visit again
            case: short ttl -> diff, but no visit
            case: prefetch  -> same, but visit again
        '''
        # TODO concern check if chosen ip is same in phase 2 for ttl dishonor

        # resolver_to_req_uid_to_event_list[resolver_ip][req_uid].append(
        #     (float(timestamp), 2, "xxx"))

        # req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))

        # req_uid_to_phase_2_resolver_lst[req_uid].append(resolver_ip)

        chosen_resolver = req_uid_to_phase_1_resolver_tuple[req_uid][0]

        two_time = req_uid_to_phase_two_time[req_uid]
        one_time = req_uid_to_phase_one_time[req_uid]

        try:
            phase_1_lst, mid_lst, phase_2_lst, curated_second_phase_lenient, \
            curated_second_phase_harsh, curated_second_phase_short = segment(
                resolver_to_req_uid_to_event_list[chosen_resolver][req_uid], req_uid, one_time, two_time)
        except Exception as e:
            print("mami {}".format(e))
            continue
        print("mama")
        mom += 1

        # TODO timing
        if len(mid_lst) > 0 or len(phase_1_lst) > 1 or len(phase_2_lst) > 1:
            continue

        if response_1 == response_2:
            if len(phase_2_lst) > 0:
                # prefetch
                resolver_ip_to_verdict_list[chosen_resolver]['p'].add(ip_hash)
                if len(curated_second_phase_lenient) == 1:
                    resolver_ip_to_verdict_list[chosen_resolver]['pl'].add(ip_hash)
                if len(curated_second_phase_harsh) == 1:
                    resolver_ip_to_verdict_list[chosen_resolver]['ph'].add(ip_hash)
            else:
                resolver_ip_to_verdict_list[chosen_resolver]['n'].add(ip_hash)

        elif "phasex" in response_2 and len(curated_second_phase_short) == 1:
            resolver_ip_to_verdict_list[chosen_resolver]['s'].add(ip_hash)

    resolver_ip_to_verdict_list_v2 = defaultdict(lambda: {"n": list(), "p": list(), "s": list(), "pl": list(), "ph": list()})
    for resolver in resolver_ip_to_verdict_list:
        for key in resolver_ip_to_verdict_list[resolver]:
            resolver_ip_to_verdict_list_v2[resolver][key] = list(resolver_ip_to_verdict_list[resolver][key])

    print("Mom {}".format(mom))

    meta = {
        "all_resolver_set": len(all_resolver_set),
        "all_exitnode_set": len(all_exitnode_set),
        "all_asn_set": len(all_asn_set),
        "all_lum_resolver_set": len(all_lum_resolver_set),
    }

    with open(dump_directory + "meta.json", "w") as ouf:
        json.dump(meta, fp=ouf)
    with open(dump_directory + "resolver_ip_to_verdict_list.json", "w") as ouf:
        json.dump(resolver_ip_to_verdict_list_v2, fp=ouf)

import time
start_time = time.time()
init(ttl=60)
tot_end = time.time()
print("Total time {}".format((tot_end - start_time) / 60))
