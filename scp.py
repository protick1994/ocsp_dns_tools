import json
import time
from collections import defaultdict
from multiprocessing.dummy import Pool as ThreadPool

main_file_base_directory = "/home/protick/node_code/results_new_exp_v2_xxx/"

bind_file_directory = "/net/data/dns-ttl/dns_ttl_new/bind"

allowed_ttl = [1, 5, 15, 30, 60]

req_uid_to_response = {}
req_uid_to_response_time = {}
req_uid_to_phase_1_resolvers_all = defaultdict(lambda: set())
req_uid_to_phase_1_resolvers_without_lum = defaultdict(lambda: set())


def get_leaf_files(path):
    import os
    list_of_files = []
    for root, dirs, files in os.walk(path):
        for file in files:
            list_of_files.append(os.path.join(root, file))
    return list_of_files


ip_to_index = {
    "3.223.194.233": 7,
    "34.226.99.56": 5,
    "52.44.221.99": 8,
    "52.71.44.50": 6,
    "18.207.47.246": 2,
    "3.208.196.0": 3,
    "44.195.175.12": 4,
    "50.16.6.90": 1,
}

chosen_meta = None


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


class Meta:
    def __init__(self):
        self.resolver_ip_to_verdict_list_dump = None

    pass


ttl_to_meta_dict = {}

'''
These are seen in global bind logs, anytime, anyhow
'''

all_resolver_set = list()
all_exitnode_set = set()
all_asn_set = set()
all_lum_resolver_set = set()
all_lum_exitnode_set = set()
all_lum_asn_set = set()

all_detected_resolvers = set()
all_detected_asns = set()
all_detected_exitnodes = set()

response_to_ip = {}

for ttl in allowed_ttl:
    meta = Meta()

    # (uid, asn, ip_hash, response_1, response_2)
    meta.dishonor_lst = []
    meta.honor_lst = []

    # both response
    meta.detected_resolvers = set()
    meta.detected_asns = set()
    meta.detected_exitnodes = set()

    # (resolver_ip, float(timestamp))
    meta.req_uid_to_phase_1_resolver_tuple = {}

    # ip er list just TODO think !!
    meta.req_uid_to_phase_2_resolver_lst = defaultdict(lambda: list())

    # only one phase successful holei
    meta.req_uid_to_asn = {}
    meta.req_uid_to_hash = {}
    meta.req_dict = {}  # contains response 1

    # severely think !!!
    meta.found_set = set()
    meta.not_found_set = set()

    ttl_to_meta_dict[ttl] = meta

for key in ip_to_index:
    response_to_ip["phase{}".format(ip_to_index[key])] = key


def analyze_bind_leaf(file):
    global req_uid_to_phase_1_resolvers_all
    global req_uid_to_phase_1_resolvers_without_lum
    with open(file) as FileObj:
        for line in FileObj:
            try:
                non_decidings_ips = ['52.4.120.223']
                if "good" not in line or "zeus_reload" not in line:
                    continue

                segments = line.strip().split()
                resolver_ip, timestamp, allotted_ip, req_url = segments[1], segments[2], segments[3], segments[4]
                req_uid = req_url.split(".")[0]
                ttl = int(req_url.split(".")[2])

                meta = ttl_to_meta_dict[ttl]

                all_resolver_set.append(resolver_ip)

                if allotted_ip != '3.220.52.113':
                    req_uid_to_phase_1_resolvers_all[req_uid].add((resolver_ip, timestamp))

                try:
                    all_asn_set.add(meta.req_uid_to_asn[req_uid])
                    all_exitnode_set.add(meta.req_uid_to_hash[req_uid])
                except:
                    pass

                if allotted_ip in non_decidings_ips:
                    all_lum_resolver_set.add(resolver_ip)
                    continue

                response = meta.req_dict[req_uid]
                real_allotted_ip = response_to_ip[response]

                if allotted_ip == '3.220.52.113':
                    meta.req_uid_to_phase_2_resolver_lst[req_uid].append(resolver_ip)
                else:
                    req_uid_to_phase_1_resolvers_without_lum[req_uid].add((resolver_ip, timestamp))
                    if allotted_ip == real_allotted_ip:
                        if req_uid not in meta.req_uid_to_phase_1_resolver_tuple:
                            meta.req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))
                        else:
                            _, t = meta.req_uid_to_phase_1_resolver_tuple[req_uid]
                            if float(timestamp) < t:
                                meta.req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(timestamp))
                # meta.found_set.add(req_uid)
            except Exception as e:
                # TODO severe inspect
                pass


def analyze_bind():
    global ttl_to_meta_dict

    leaf_files_unfiltered = get_leaf_files(bind_file_directory)

    pool = ThreadPool(10)
    results = pool.map(analyze_bind_leaf, leaf_files_unfiltered)
    pool.close()
    pool.join()


bad_time_diff = []
good_time_diff = []


def analyze_main_file_leaf(file):
    f = open(file)
    d = json.load(f)
    for num in d['dict_of_phases']:
        try:

            if '1-response' in d['dict_of_phases'][num]:
                good_time_diff.append(d['dict_of_phases'][num]['1-time'] - d['dict_of_phases'][num]['1-time-start'])

            # if '2-response' in d['dict_of_phases'][num]:
            #     good_time_diff.append(d['dict_of_phases'][num]['2-time'] - d['dict_of_phases'][num]['2-time-start'])
            #
            # if 'host-phase-1' in d['dict_of_phases'][num] and "Could not resolve host" in d['dict_of_phases'][num]['errmsg-1']:
            #     bad_time_diff.append(d['dict_of_phases'][num]['1-time'] - d['dict_of_phases'][num]['1-time-start'])
            #
            # if 'host-phase-2' in d['dict_of_phases'][num] and "Could not resolve host" in d['dict_of_phases'][num]['errmsg']:
            #     bad_time_diff.append(d['dict_of_phases'][num]['2-time'] - d['dict_of_phases'][num]['2-time-start'])

        except Exception as e:
            # TODO
            pass

    return 1


def analyze_main_files(ttl):
    global chosen_meta
    global ttl_to_meta_dict
    chosen_meta = ttl_to_meta_dict[ttl]
    # /home/protick/node_code/results_new_exp_v2_1/2
    # leaf_files_unfiltered = get_leaf_files("/home/protick/node_code/results_new_exp_dnssec_v11/")
    leaf_files_unfiltered = get_leaf_files("/home/protick/node_code/results_new_exp_v2_1/2/")
    # leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    # leaf_files_filtered = [e for e in leaf_files_filtered if ".json" in e]
    #
    # exp_id_list = []
    # for element in leaf_files_filtered:
    #     exp_id_list.append(element[: - len("-out.json")])

    pool = ThreadPool(10)
    results = pool.map(analyze_main_file_leaf, leaf_files_unfiltered)
    pool.close()
    pool.join()


# TTL str na onno kichu

def dump_files():
    from pathlib import Path
    dump_directory = "ttl_new_results/"
    Path(dump_directory).mkdir(parents=True, exist_ok=True)

    time_meta = {
        "good_time_diff": good_time_diff,
        "bad_time_diff": bad_time_diff
    }
    with open(dump_directory + "time_normal.json", "w") as ouf:
        json.dump(time_meta, fp=ouf)


'''
 uid -> ' a '       ' a '
'''


def init(ttl):
    global ttl_to_meta_dict
    meta = ttl_to_meta_dict[ttl]

    resolver_ip_to_verdict_list = defaultdict(lambda: {"g": set(), "b": set()})
    resolver_ip_to_verdict_list_dump = defaultdict(lambda: {"g": list(), "b": list()})

    for e in meta.dishonor_lst:
        try:
            req_uid, asn, ip_hash, r1, r2 = e
            culprit_resolver = meta.req_uid_to_phase_1_resolver_tuple[req_uid][0]
            if culprit_resolver not in meta.req_uid_to_phase_2_resolver_lst[req_uid]:
                resolver_ip_to_verdict_list[culprit_resolver]["b"].add(ip_hash)
                meta.detected_resolvers.add(culprit_resolver)
                meta.detected_asns.add(asn)
                meta.detected_exitnodes.add(ip_hash)
        except:
            pass

    for e in meta.honor_lst:
        try:
            req_uid, asn, ip_hash, r1, r2 = e
            good_resolver = meta.req_uid_to_phase_1_resolver_tuple[req_uid][0]
            if ip_hash not in resolver_ip_to_verdict_list[good_resolver]["b"] and \
                    good_resolver in meta.req_uid_to_phase_2_resolver_lst[req_uid]:
                resolver_ip_to_verdict_list[good_resolver]["g"].add(ip_hash)
                meta.detected_resolvers.add(good_resolver)
                meta.detected_asns.add(asn)
                meta.detected_exitnodes.add(ip_hash)
            elif ip_hash in resolver_ip_to_verdict_list[good_resolver]["b"]:
                resolver_ip_to_verdict_list[good_resolver]["b"].remove(ip_hash)
        except:
            pass

    for resolver in resolver_ip_to_verdict_list:
        resolver_ip_to_verdict_list_dump[resolver]["g"] = list(resolver_ip_to_verdict_list[resolver]["g"])
        resolver_ip_to_verdict_list_dump[resolver]["b"] = list(resolver_ip_to_verdict_list[resolver]["b"])

    meta.resolver_ip_to_verdict_list_dump = resolver_ip_to_verdict_list_dump

    # arr = []
    # arr_new = []
    # arr_global = []
    # bad_resolvers = []
    #
    # for resolver_ip in resolver_ip_to_verdict_list:
    #     good_len = len(resolver_ip_to_verdict_list[resolver_ip]["g"])
    #     bad_len = len(resolver_ip_to_verdict_list[resolver_ip]["b"])
    #     if good_len + bad_len == 0:
    #         continue
    #     arr_global.append((resolver_ip, (bad_len/(good_len + bad_len))))
    #     if bad_len/(good_len + bad_len) == 1:
    #         bad_resolvers.append(resolver_ip)
    #     if good_len + bad_len >= 5:
    #         arr.append((resolver_ip, bad_len/(good_len + bad_len)))
    #         arr_new.append((resolver_ip, bad_len/(good_len + bad_len), bad_len))


start_time = time.time()

for ttl in allowed_ttl:
    analyze_main_files(ttl=ttl)



dump_files()
print("Dumped files")