import json
import time
from collections import defaultdict
from multiprocessing.dummy import Pool as ThreadPool
import traceback
main_file_base_directory = "/home/protick/node_code/dnssec_probing_v8/"

bind_file_directory = ['/home/protick/new_probe/log_1/', '/home/protick/new_probe/log/']

allowed_ttl = [60]


def get_leaf_files(path):
    import os
    list_of_files = []
    for root, dirs, files in os.walk(path):
        for file in files:
            list_of_files.append(os.path.join(root, file))
    return list_of_files


ip_to_index = {
    "3.223.194.233": 7,
    "34.226.99.56": 5,
    "52.44.221.99": 8,
    "52.71.44.50": 6,
    "18.207.47.246": 2,
    "3.208.196.0": 3,
    "44.195.175.12": 4,
    "50.16.6.90": 1,
}


chosen_meta = None


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


response_to_ip = {}
req_dict = {}
req_uid_to_phase_1_resolver_tuple = {}

for key in ip_to_index:
    response_to_ip["phase{}".format(ip_to_index[key])] = key

req_uid_to_resolvers = defaultdict(lambda : set())

req_uid_to_resolver_to_edns = defaultdict(lambda : defaultdict(lambda : False))


# good proper 104.156.251.15 1665429669.1053329 1 52.4.120.223 172.17.0.6 832a1c83-242b-4ed9-bbe4-37c83e3b2cc61665429665514.live_signsecprobing_60.correct.cashslide.app. A tc: 0 do: 32768 UDP
def analyze_bind_leaf(file):
    with open(file) as FileObj:
        for line in FileObj:
            try:
                non_decidings_ips = ['52.4.120.223']
                if "good" not in line or "live_signsecprobing_60" not in line:
                    continue

                segments = line.strip().split()

                resolver_ip, ts, mode, allotted_ip, dns_server_inside_ip, qn, qt, tc_bit, \
                ednsflag, protocol = segments[2], segments[3], segments[4], segments[5], segments[6], \
                                     segments[7], segments[8], segments[10], segments[12], segments[13]


                req_url = qn
                req_uid = req_url.split(".")[0]

                if int(ednsflag) == 32768:
                    req_uid_to_resolver_to_edns[req_uid][resolver_ip] = True

                # if allotted_ip in non_decidings_ips:
                #     continue

                response = req_dict[req_uid]
                real_allotted_ip = response_to_ip[response]
                req_uid_to_resolvers[req_uid].add(resolver_ip)

                if allotted_ip == real_allotted_ip:
                    if req_uid not in req_uid_to_phase_1_resolver_tuple:
                        req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(ts))
                    else:
                        _, t = req_uid_to_phase_1_resolver_tuple[req_uid]
                        if float(ts) < t:
                            req_uid_to_phase_1_resolver_tuple[req_uid] = (resolver_ip, float(ts))

            except Exception as e:
                # TODO severe inspect
                pass


def analyze_bind():
    global ttl_to_meta_dict

    leaf_files_unfiltered_1 = get_leaf_files(bind_file_directory[0])
    leaf_files_unfiltered_2 = get_leaf_files(bind_file_directory[1])
    leaf_files_unfiltered = leaf_files_unfiltered_1 + leaf_files_unfiltered_2
    pool = ThreadPool(10)
    results = pool.map(analyze_bind_leaf, leaf_files_unfiltered)
    pool.close()
    pool.join()

'''
{'ip_hash_2': 'rebaf791c0deef5f9f5f55f5cd547db5f', 
'asn_2': '11522', 
'host-phase-2': '<p>\nphaselum\n</p>\n', 
'2-time': 1665337843701, 
'req_url_2': 'http://c8897056-ba6e-4adb-9045-3f7b687c5c621665337838452.live_signsecprobing_60.incorrect.cashslide.app'}, 

'544715': {'ip_hash': 're43c3972f31cac919988f8dfe658aee4', 
'asn': '52827', 
'host-phase-1': '<p>\nphaselum\n</p>\n', 
'1-time': 1665337521550, 
'req_url_1': 'http://4e480940-e8b2-454e-aa8b-74ddd3581e511665337518514.live_signsecprobing_60.correct.cashslide.app', 
'host-phase-2': 'err', 
'errmsg': '<!doctype html><h1>Webpage not available</h1><p>The webpage could not be loaded because:</p><p>Could not resolve host e6686eaa-0fad-4b0b-9056-4041ce97d5ae1665337521550.live_signsecprobing_60.incorrect.cashslide.app</p>'}

'''

allowed_resp = ['phase1', 'phase2', 'phase3', 'phase4']


def modate_response_type(data):
    try:
        for e in allowed_resp:
            if e in data:
                return 1
        if "phaselum" in data:
            return 2
        if "Could not resolve host" in data:
            return 3
        return 4
    except Exception as e:
        print("yo ", e)

meta_list = []

def analyze_main_file_leaf(full_file):
    # zeus reload 1 1663827072 2
    # /home/protick/node_code/results_new_exp_v2_1/2/1663827072
    # level_timestamp = exp_id.split("_")[-2]
    # full_file = "{}{}/{}/{}-out.json".format(main_file_base_directory.replace("xxx", str(ttl)), 2,
    #                                          level_timestamp, exp_id)
    f = open(full_file)
    e = json.load(f)

    #print(full_file)

    for num in e['dict_of_phases']['live_signsecprobing_60']:
        try:
            d = e['dict_of_phases']['live_signsecprobing_60']
            # print("padding")
            response_1 = modate_response_type(d[num]['host-phase-1'].strip())
            req_url_uid_1 = d[num]['req_url_1'][7:].split(".")[0]
            req_dict[req_url_uid_1] = d[num]['host-phase-1'].strip()[4: -5]
            req_1_time = int(d[num]['1-time']) / 1000
            # req_dict[req_url_uid_1] = response_1

            if "lum" in req_dict[req_url_uid_1]:
                continue

            two_res = 'arrived'

            if 'errmsg' in d[num]:
                two_res = "blocked"

            # req_url_uid_2 = d[num]['req_url_2'][7:].split(".")[0]
            # req_dict[req_url_uid_2] = d[num]['host-phase-2'].strip()[4: -5]
            # response_2 = d[num]['host-phase-2'].strip()
            # req_2_time = int(d[num]['2-time']) / 1000
            #
            # if 'err' in response_2:
            #     response_2 = modate_response_type(d[num]['errmsg'].strip())
            # else:
            #     response_2 = modate_response_type(response_2)



            meta_list.append((req_url_uid_1, response_1, req_1_time, two_res))
            #print("adding")
        except Exception as ee:
            #print(traceback.format_exc())
            #print("hello", ee)
            pass

    return 1


def get_main_files():
    leaf_files_unfiltered = get_leaf_files(main_file_base_directory)
    # leaf_files_filtered = [e.split("/")[-1] for e in leaf_files_unfiltered]
    # leaf_files_filtered = [e for e in leaf_files_filtered if ".json" in e]
    return leaf_files_unfiltered


def analyze_main_files():
    files_filtered = get_main_files()

    exp_id_list = []
    for element in files_filtered:
        leaf_file = element.split("/")[-1]
        exp_id_list.append(leaf_file[: - len("-out.json")])

    pool = ThreadPool(10)
    results = pool.map(analyze_main_file_leaf, files_filtered)
    pool.close()
    pool.join()

'''
 uid -> ' a '       ' a '
'''

def init(ttl):

    # for e in allowed_resp:
    #     if e in data:
    #         return 1
    # if "phaselum" in data:
    #     return 2
    # if "Could not resolve host" in data:
    #     return 3
    # return 4

    # meta_list.append((req_url_uid_1, response_1, req_1_time, req_url_uid_2, response_2, req_2_time))

    validating_resolver = set()
    non_validating_resolvers = set()

    # arrived

    for e in meta_list:
        try:
            req_url_uid_1, response_1, req_1_time, res_2 = e

            if response_1 == 1 and res_2 == 'blocked':
                resolver = req_uid_to_phase_1_resolver_tuple[req_url_uid_1][0]
                if req_uid_to_resolver_to_edns[req_url_uid_1][resolver]:
                    validating_resolver.add(resolver)

            if response_1 == 1 and res_2 == 'arrived':
                resolver = req_uid_to_phase_1_resolver_tuple[req_url_uid_1][0]
                if req_uid_to_resolver_to_edns[req_url_uid_1][resolver]:
                    non_validating_resolvers.add(resolver)

        except:
            pass


    validating_resolver = validating_resolver.difference(non_validating_resolvers)

    # f = open("validating_resolvers.json")
    # old_validating_v2 = set(json.load(f))

    f = open("validating_resolvers_v2.json")
    old_validating = set(json.load(f))
    old_validating = old_validating.difference(non_validating_resolvers)
    validating_resolver = validating_resolver.union(old_validating)

    print(len(validating_resolver))
    with open("validating_resolvers_v4.json", "w") as ouf:
        json.dump(list(validating_resolver), fp=ouf)




start_time = time.time()

for ttl in allowed_ttl:
    analyze_main_files()
s = set()
analyzed_node_time = time.time()
# print("Analyze time {}".format((analyzed_node_time - start_time) / 60))
# for e in meta_list:
#     s.add(e[3])
# print("Yo ", s)

analyze_bind()

analyzed_bind_time = time.time()
print("Bind time {}".format((analyzed_bind_time - start_time) / 60))

for ttl in allowed_ttl:
    init(ttl)

analyzed_init_time = time.time()
print("Total time {}".format((analyzed_init_time - start_time) / 60))
